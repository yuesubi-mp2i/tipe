type 'a option =
  | None
  | Some of 'a;;


let or = fun (opt_a) -> fun (opt_b) ->
  match opt_a with
  | Some a -> Some (a)
  | None -> opt_b;;

let unwrap_or = fun (opt_a) -> fun (b) ->
  match opt_a with
  | Some a -> a
  | None -> b;;
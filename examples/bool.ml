type bool = False | True;;

let and =
  fun a ->
    match a with
    | False ->
        fun (_) -> False
    | True ->
        fun (b) -> b;;

let better_and = fun a -> fun b ->
  match a with
  | False -> False
  | True -> b;;

let or = fun a -> fun b ->
  match a with
  | True -> True
  | False -> b;;

let not = fun a ->
  match a with
  | True -> False
  | False -> True;;

let implies = fun a -> fun b ->
  or (not a) b;;
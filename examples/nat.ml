type nat =
  | Zero
  | Succ of nat;;

let add = fun x -> fun y ->
  match x with
  | Zero -> y
  | Succ x' -> Succ (add x' y);;

let res = add (Succ (Zero)) (Succ (Succ (Zero)));;
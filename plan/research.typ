= Idées

+ Faire un programme qui parse des fichiers à
  l'aide d'une description de la syntaxe, type
  Barkus-Naur.

  Problème : ça existe déjà, et c'est pas si compliqué.

2. Générer des contres-exemples pour une syntaxe
  qui serait ambigüe.

  Problème : ça à l'air trop simple.

3. Faire un mini langage fonctionnel et prouver
  des post-conditions que l'utilisateur renseigne.

  Problème : quel serait la post-condition d'une
  fontion qui fait office de définition ?

  Par exemple l'addition des entiers de Peano :
  ```ocaml
  type peano = Zero | Succ of peano;;

  let rec add (a: peano) (b: peano) : peano =
    match a with
    | Zero -> b
    | Succ of a' -> Succ (add a' b);;
  ```

  En fait il suffit de donner des propriétés sur les fonctions.

4. Faire la même chose mais avec des variant cette
  fois ci, pour prouver la terminaison des
  fonctions récursives.

  Problème : si on ajoute des variants on se
  ramène à un #link("https://en.wikipedia.org/wiki/Total_functional_programming")[langage total].
  Donc c'est pas assez ambitieux.


= Liens

- Systèmes de logique
  - Logique propositionnelle #link("https://en.wikipedia.org/wiki/Propositional_calculus")
  - Logique du premier ordre #link("https://en.wikipedia.org/wiki/First-order_logic")

- Faire un interpréteur
  - infos générales #link("https://en.wikipedia.org/wiki/Compiler#Compiler_construction")
  - livre #link("https://craftinginterpreters.com")

- Systèmes de preuves #link("https://en.wikipedia.org/wiki/Automated_theorem_proving#")
  - Induction structurelle #link("https://en.wikipedia.org/wiki/Structural_induction")
  - Hoare logic #link("https://en.wikipedia.org/wiki/Hoare_logic")
  - Language total #link("https://en.wikipedia.org/wiki/Total_functional_programming")

- Inférence de type
  - Hindley–Milner type system #link("https://en.wikipedia.org/wiki/Hindley%E2%80%93Milner_type_system")


Idée, essayer de créer des règles similaire à celle de Hoare
pour les langages fonctionnnels.

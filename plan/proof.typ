#align(center)[
  = Démonstration de l'indécidabilité de mon TIPE
]

== Théorème de Rice
(Exercice 9, du TD 5.)

=== Énoncé
#let verifQ = [#smallcaps[Vérifie]$""_Q$]

Si $Q$ est un prédicat non trivial
sur les langages des machines,
alors le problème
$ verifQ
    cases(
      "Entrée" &: "Une machine" cal(M),
      "Sortie" &: cal(M) "vérifie-t-elle" Q "?"
    ) $
est indécidable.

=== Preuve
#let arret = smallcaps[Arrêt]

- Soit $Q$ un prédicat non trivial sur les
  langages des machines.

  Montrons que le problème
  de l'#arret se réduit
  au problème #verifQ.

  - Soit $(cal(M), omega)$ une instance du problème
    #arret.

    On pose $cal(N)_cal(M)$ la machine
    ...

  On note $f$ la fonction définie par
  $cal(M) |-> cal(N)_cal(M)$.
  Cette fonction est calculable.

  Alors
  $ () in arret^+
    &<=> ... \
    &<=> f() in verifQ^+ $

== Mon TIPE
#let verif = smallcaps[Vérifie]

Dans mon TIPE, je m'intéresse aux
propriétés de la forme
$ (Q) quad forall e_1 : tau_1, ..., forall e_n : tau_n,
  E_f = E'_f $
i.e. on définit alors le prédicat $Q$
comme étant l'ensemble des fonctions
OCaml vérifiant cette propriété.

On note $cal(T)$ l'ensemble des prédicats
de cette forme.

L'objectif de mon TIPE est de résoudre
le problème
// note : Q n'est pas dénombrable
$ verif cases(
      "Entrée" : Q "un prédicat de" cal(T) "et"
        cal(M) "une machine",
      "Sortie" : cal(M) "vérifie-t-elle" Q ?
    ) $

== Indécidabilité de #verif
Soit $Q$ un prédicat non trivial
de $cal(T)$ (oui il en existe#super[TM]).

On pose $f$ la fonction définie
par $cal(M) |-> (Q, cal(M))$.
Cette fonction est calculable.

Soit $cal(M)$ une instance du problème
#verifQ, alors
$ cal(M) in verifQ^+
  <=> (Q, cal(M)) in verif^+
  <=> f(cal(M)) in verif^+ $

Ainsi #verif est indécidable.
$square$
#set page(
  width: auto,
  height: auto
)

= Hello

== Code
```ml
type bool = False | True;;
```

== Propriété
// gruvbox
#let light_orange = rgb("d65d0e");
#let orange = rgb("af3a03")

#let code(body) = box(
  body,
  inset: 4pt,
  stroke: (
    paint: light_orange,
    dash: "dashed",
  ),
)

#let seq(hyp, left_body, right_body) = {
  align(
    horizon,
    grid(
      columns: 5,
      gutter: 4pt,
      [#hyp],
      [$tack$],
      [#left_body],
      [$equiv$],
      [#right_body]
    )
  )
}

#let prog = ("let",
  ("var",
    ("id",
      "x",
      42
    )
  ),
  ("lit_constr",
    ("id",
      "True",
      8
    )
  ),
  ("lit_var",
    ("id",
      "x",
      42
    )
  )
)

#seq(
  [],
  code[
    *let* x#sub[42] = #smallcaps[True]#sub[8] *in* \
    x#sub[42]
  ],
  code[
    #smallcaps[True]#sub[8]
  ]
)



hi #code[hello] hi


== Terminaison
_nothing to do_

== Preuve / contre exemple
#import "@preview/curryst:0.4.0": rule, proof-tree

#proof-tree(
  rule(
    name: [trans#sub[L] [1]],
    seq(
      [$Gamma$],
      code[
        let x = True in \
        x
      ],
      code[
        True
      ]
    ),
    rule(
      name: [equiv],
      $ tack #[True] equiv #[True] $
    )
  )
)

Utiliser des ids relatifs, comme ça, \
même pas besoin de link.
+ $ tack.double
    #[let] x = #[True in] x $
  $ ~~> $
  $ x := #[True]
    tack.double x equiv #[True] $
  $ ~~> $
  $ tack.double #[True] equiv #[True] $
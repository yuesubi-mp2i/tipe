import os


def list_files(path, name):
    folder = os.path.join(path, name)
    content = [name]

    for elem in os.listdir(folder):
        elem_path = os.path.join(folder, elem)

        if os.path.isfile(elem_path):
            content.append(elem)
        elif os.path.isdir(elem_path):
            content.append(list_files(folder, elem))

    return content

files = str(list_files(".", "src"))
files = files.replace("[", "(")
files = files.replace("]", ")")
files = files.replace("'", '"')
print(files)
#{
  set raw(tab-size: 4)
  set text(
    lang: "fr",
    size: 8pt
  )

  set page(
    margin: (inside: 2.5cm, outside: 2cm, y: 1.75cm),
    header: context {
      let matches = query(
        selector(heading).before(here())
      ).map(
        h => h.body
      );
      matches.insert(0, []);

      matches.last()
    },
    numbering: "1 / 1"
  )

  align(center, text(size: 2em)[
    Gros bouquin de code
  ])
  align(right)[-- RIP la planète]

  let tree_outline(dir, tree) = {
    let dirname = tree.at(0)
    let dir = dir + dirname + "/"

    let dir_elems = tree.slice(1)
    let folders = ()
    let files = ()

    for elem in dir_elems {
      if type(elem) == array {
        folders.push(elem)
      } else {
        files.push(elem)
      }
    }

    box(stroke: black, inset: 1em)[
      #smallcaps(underline[#dirname])
      #context[
        (page #locate(label(dir)).page())
      ]

      #files.map(
        file => [
          #file
          #context[
            (#locate(label(dir + file)).page())
          ]
        ]
      ).join("")

      #for elem in folders {
        tree_outline(dir, elem)
        h(1em)
      }
    ]
   
  }

  let print_files(dir, tree) = {
    let dir = dir + tree.at(0) + "/";
    [ #[= Dossier #dir] #label(dir)]

    let dir_elems = tree.slice(1)
    let folders = ()
    let files = ()

    for elem in dir_elems {
      if type(elem) == array {
        folders.push(elem)
      } else {
        files.push(elem)
      }
    }

    for elem in files {
      [ #[== Fichier #dir#elem] #label(dir + elem) ]
      raw(read(dir + elem), lang: "rust")
    }

    for elem in folders {
      print_files(dir, elem)
    }
  }

  // Use `list_files.py` to generate this file tree
  let tree = ("src", "typ.rs", ("solv", "matcher.rs", "dependency.rs", "free_id.rs", "pat.rs", ("normal", ("sort", "mod.rs"), ("devel", "alias.rs", "factor.rs", "mod.rs"), ("utils", "extract.rs", "misc.rs", "find.rs", "mod.rs"), "sub.rs", "mod.rs", ("simpl", "rename.rs", ("eval", "matcher.rs", "call.rs", "assoc.rs", "tail.rs", "pat.rs", "choice.rs", "mod.rs"), ("clean", "unalias.rs", "mod.rs", "used.rs"), "mod.rs")), "prove.rs", "proof.rs", "prop.rs", "mod.rs", "eval.rs"), ("link", "id_gen.rs", ("link", "typ.rs", "expr.rs", "pat.rs", "stmt.rs", "ast.rs", "prop.rs", "mod.rs"), "scope_stack.rs", "vars.rs", "mod.rs"), "main.rs", ("lang", ("ast", "kind.rs", "typ.rs", "expr.rs", "pat.rs", "stmt.rs", "ast.rs", "prop.rs", "mod.rs"), "read.rs", "trace.rs", ("lex", "token.rs", "lexer.rs", "mod.rs"), ("parser", "typ.rs", "expr.rs", "pat.rs", "stmt.rs", "prop.rs", "mod.rs"), "mod.rs"))

  [ = Arborescence du projet ]
  align(
    center,
    tree_outline("../../", tree)
  )

  set page(columns: 2)
  print_files("../../", tree)
}
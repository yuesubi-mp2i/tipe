#import "@preview/polylux:0.3.1": *
#import themes.clean: *


#import "@preview/lovelace:0.2.0": *

#show: setup-lovelace
#let algorithm = algorithm.with(supplement: "Code")
#show: setup-lovelace.with(line-number-supplement: "Ligne")


// https://ethanschoonover.com/solarized/
#let yellow = rgb("#b58900")
#let orange = rgb("#cb4b16")
#let red    = rgb("#dc322f")
#let magenta= rgb("#d33682")
#let violet = rgb("#6c71c4")
#let blue   = rgb("#268bd2")
#let cyan   = rgb("#2aa198")
#let green  = rgb("#859900")


#let highlight_syntax(doc) = {

  let keywords = ("type", "let", "match", "with", "rec", "of", "\\|",
    "=", "->", "#", "post", ":");

  // show regex(keywords.join("|")): strong 
  show regex(keywords.join("|")): elem => text(red, elem)
  show regex("eq"): elem => smallcaps(text(yellow, elem))

  let types = ("nat", "bool")
  show regex(types.join("|")): elem => text(magenta, elem)

  let constructors = ("Zero", "Succ", "True", "False")
  show regex(constructors.join("|")): elem => text(violet, elem)

  doc
}


#show: clean-theme.with(
  footer: [Author],
  short-title: [Preuves automatiques de validité des post-conditions],
  logo: [ #sym.floral ],
  aspect-ratio: "4-3",
  color: rgb("#ee964b")
)

#title-slide(
  title: [Preuves automatiques de validité  \ des post-conditions],
  subtitle: [],
  authors: ([Author]),
  date: [Version du #datetime.today().display("[day]/[month]/[year]")],
  // watermark: [42]
)


// #slide(title: [Sommaire])[
//   #outline(title: none, indent: 1em)
// ]


#new-section-slide(
  [Exemple : égalité sur les entiers de Peano]
)


#let peano_code = algorithm(
    caption: [Égalité sur les entiers de Peano],
    highlight_syntax[
      #pseudocode-list[
        + type bool = True | False
        + type nat = Zero | Succ of nat
        + $space$
        + let rec eq ($a$: nat) ($b$: nat) : bool =
          + \#post $x$: nat. $y$: nat. eq $x$ $y$ = eq $y$ $x$
          + match $a$, $b$ with
          + | Zero, Zero $->$ True
          + | Zero, Succ $b'$ $->$ False
          + | Succ $a'$, Zero $->$ False
          + | Succ $a'$, Succ $b'$ $->$ eq $a'$ $b'$
      ]
    ]
  )


#slide(title: [Le programme])[
  #align(center + horizon, peano_code)
]



#slide(title: [Symétrie de l'égalité])[
  #table(
    columns: 2,
    stroke: none,
    [
      Quantificateurs universels
      - #highlight_syntax[$x$: nat.]
      - $forall x in NN$

      Post-condition
      - #highlight_syntax[$x$: nat. $y$: nat. eq $x$ $y$ = eq $y$ $x$]
      - $forall x in NN, forall y in x, x=y <=> y = x$
    ],
    [
      #set text(size: 0.7em)
      #peano_code
    ]
  )
]


#slide(title: [Stratégie])[
  #table(
    columns: 2,
    stroke: none,
    [
      #set text(size: 0.85em)

      === Correction partielle
      #align(
        center + horizon,
        highlight_syntax(table(
          columns: 2,
          inset: 0.5em,
          [*Élément*], [*Raisonnement*],
          [$x$: nat.], [Soit $x$: nat.],

          [match $x$ with],
          [Disjonction de cas #footnote[Réalisable car les matchs imbriqués
            seront pas trop nombreux en pratique]],

          [Appel récursif], [Induction structurelle]
        ))
      )

      === Terminaison
      Langage total : appel sur des arguments plus petits selon
      l'ordre produit couplé avec l'ordre structurel.
    ],
    [
      #set text(size: 0.7em)
      #peano_code
    ]
  )
]


#slide(title: [Preuve])[
  #table(
    columns: 2,
    stroke: none,
    highlight_syntax[
      #set text(size: 0.72em)
      #pseudocode-list[
      + Soit $x$: nat.
      - $y$: nat. nat_eq $x$ $y$ = nat_eq $y$ $x$
      + Soit $y$: nat.
      - nat_eq $x$ $y$ = nat_eq $y$ $x$
      - match $x,y$ with ... = nat_eq $y$ $x$
      - match $x,y$ with ... = match $y,x$ with ...
      + Si ($x$,$y$) = (Zero, Zero)
        - True = match $y,x$ with ...
        - True = True
      + Si ($x$,$y$) = (Zero, Succ $y'$)
        - False = match $y,x$ with ...
        - False = False
      + Si ($x$,$y$) = (Succ $x'$, Zero)
        - False = match $y,x$ with ...
        - False = False
      + Si ($x$,$y$) = (Succ $x'$, Succ $y'$)
        - eq $x'$ $y'$ = match $y,x$ with ...
        - eq $x'$ $y'$ = eq $y'$ $x'$ $square$
        // + Vrai par hypothèse d'induction structurelle. $qed$
    ]],
    [
      #set text(size: 0.7em)
      #peano_code
    ]
  )
]


#slide(title: [Une propriété fausse])[
  #align(center, highlight_syntax[\#post $x$: nat. eq (Succ $x$) $x$ = True])

  #table(
    columns: 2,
    stroke: none,
    [
      #set text(size: 0.9em)

      Vérification
      #highlight_syntax(pseudocode-list[
        + Soit $x$: nat.
        - eq (Succ $x$) $x$ = True
        - match Succ $x$, $x$ with ... = True
        + Si (Succ $x$, $x$) = (Succ $x$, Zero)
          - False
      ])

      Contre-exemple : \
      #highlight_syntax[eq (Succ Zero) Zero #text(red)[$!=$] True]
    ],
    [
      #set text(size: 0.7em)
      #peano_code
    ]
  )
]


#slide(title: [Elements (minimals) du langage])[
  - Définition de types
    - Type somme
    - Type produit
    - Type fonction

  - Disjonctions de cas
    - Pattern matching

  - Affectations

  - Fonctions anonymes

  - Post-conditions
    - Égalité
]


#slide(title: [TODO])[
  - Coder
    - Choisir une syntaxe (probablement une sous-syntaxe du Caml)
    - Analyse lexicale et syntaxique
    - Construction d'un AST
    - Inférence de type : Hindley-Milner
    - Vérification des propriétés (et écriture de la preuve)
    - (Interpréteur)
]
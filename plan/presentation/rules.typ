#import "@preview/curryst:0.4.0": rule, proof-tree

$ Gamma = forall alpha_1 ... forall alpha_n. phi = psi, v :=e $

#proof-tree(
  rule(
    name: [match#sub[G]],
    $ Gamma tack
      #rect(
        $ &"match" e "with"\
          &| p_1 -> e_1 \
          &quad dots.v quad quad dots.v \
          &| p_n -> e_n $
      )_E
      equiv E' $,
    
    $ Gamma, "FillVars"(p_1, e) tack e_1 equiv E' $,
    $ ... $,
    $ Gamma, "FillVars"(p_n, e) tack e_n equiv E' $,
  )
)
où

Utiliser une propriété
#proof-tree(
  rule(
    name: [use],
    $ Gamma tack e = e'$,
    $ Gamma,
      forall alpha_1 ... forall alpha_n. phi = psi
      tack phi = e'$
  )
)

#proof-tree(
  rule(
    name: [ax],
    $ Gamma, phi tack phi$
  )
)

Élimination du $bot$ :
sert à dire que quand FillVars
à le droit de ne pas marcher
#proof-tree(
  rule(
    name: [$bot$#sub[e]],
    $ Gamma tack phi $,
    $ Gamma tack bot $
  )
)

#proof-tree(
  rule(
    name: [i.s.],
    $ $
  )
)
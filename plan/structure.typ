= Idées
- Distinguer des cas particuliers pour les types inductifs
  - Pour les types similaires à `nat` il suffit d'encoder la longueur
  - Pour les bools il suffit d'avoir un identifiant pour chaque cas de base.


= Architecture
- Représentation de la syntaxe
  - Transformer la source en lexèmes
  - Générer l'arbre de syntaxe aussi sucré que le code
- Représentation du code
  - Transformer la syntaxe en une représentation
    la plus minimale possible
    - Enlever tout le sucre
    - Identifier les différents symboles et leur
      attribuer une représentation plus minimale.
    - Trouver une façon de rattacher les variables,
      les fonctions et les types à leur définition.
- Interpréteur
  - ???

== Phases du compilateur
- Transformation en lexèmes
- Arbre de syntaxe abtrait
- Associer les fonctions et variables à leurs définition

- Inférence de type
- Vérification des propriétés

- Tansformation en byte code
- Exécution du code

== Problèmes à résoudre
- où est-ce que l'on déclare les propriétés ?
- en dessous de la définition
- pour les fonctions anonymes ?
- soit on met des propriétés soit, la définition est utilisée directement
  pour transformer les préconditions

== Syntaxe

=== Élémentaires
- Définition de types
  - Alias d'un autre type
    ```ml
    type <name> = <type_inline>
    ```

  - Création d'un type somme avec
    ```ml
    type <name> = <constructeur_1> | ... | <constructeur_n> of <type_inline>
    ```

- Équalité avec `=`

- Instructions conditionnelles
  - Match with
    ```ml
    match <expression> with
    | <pattern_1> -> <expr_1>
    ...
    | <pattern_n> -> <expr_n>
    ```

- Assignations
  - Variables
    ```ml
    let <pattern_variable> = <expr> in <expr>
    ```

- Preconditions
  ```ml
  #pre <prédicat>;
  # <prédicat>;
  <expr>
  ```


= Système de logique
Piqué sur #link("https://en.wikipedia.org/wiki/Hoare_logic#Assignment_axiom_schema")

Il faudrait peut être créer des règles pour simplifier les
propriétés.

Regarder la logique du premier ordre.

== Assignment axiom schema
$ {P[E/x]} #raw("let") x = E {P} $

== Match rule
Sort of the if rule but with a match statement

== Consequence rule
Try to understand what it means

== Structural induction rule smth
maybe consider properties true on smaller structures


== Test
```ml
type bool = True | False
type nat = Zero | Succ of nat

let rec nat_eq (x: nat) (y: nat) : bool =
  #post x: nat. y: nat. nat_eq x y = nat_eq y x
  match x, y with
  | Zero, Zero -> True
  | Zero, Succ _ -> False
  | Succ _, Zero -> False
  | Succ x', Succ y' -> nat_eq x' y'
```

Mq $x: "nat". y: "nat". "nat_eq" x y = "nat_eq" y x$

- On pose $x = "Zero"$

  - On pose $y = "Zero"$
    + $"nat_eq" x y = "nat_eq" y x$
    + $"True" = "nat_eq" y x$
    + $"True" = "True"$
  
  - Soit $y': "nat"$.

    On suppose $"nat_eq" x y' = "nat_eq" y' x$.

    On pose $y = "Succ" y'$.

    + $"nat_eq" x y = "nat_eq" y x$
    + $"False" = "nat_eq" y x$
    + $"False" = "False"$

- Soit $x': "nat"$.

  On suppose $ y: "nat". "nat_eq" x' y = "nat_eq" y x'$.

  - On pose $y = "Zero"$
    + $"nat_eq" x y = "nat_eq" y x$
    + $"False" = "nat_eq" y x$
    + $"False" = "False"$
  
  - Soit $y': "nat"$.

    On suppose $"nat_eq" x y' = "nat_eq" y' x$.

    On pose $y = "Succ" y'$.

    + $"nat_eq" x y = "nat_eq" y x$
    + $"nat_eq" x' y' = "nat_eq" y x$
    + $"nat_eq" x' y' = "nat_eq" y' x'$
    $qed$


= Apparence

```ocaml
type nat = Zero | Succ of nat

// Equal
let rec nat_eq (x: nat) (y: nat) : bool =
  match x, y with
  | Zero, Zero -> True
  | Zero, Succ _ -> False
  | Succ _, Zero -> False
  | Succ x', Succ y' -> nat_eq x' y'

// Smaller of equal
let rec nat_se (x: nat) (y: nat) : bool =
  # x: nat |- nat_se x x
  # x: nat, y: nat |- nat_se x y && nat_se y x => nat_eq x y
  # x: nat, y: nat, z: nat |- nat_se x y && nat_se y z => nat_se x z
  match x, y with
  | Zero, _ -> True
  | _, Zero -> False
  | Succ x', Succ y' -> nat_is_smaller x' y'

// Addition
let add (x: nat) (y: nat) : nat =
  # x: nat, y: nat, z: nat |- nat_se x y => nat_se (add x z) (add y z)
  match x with
  | Zero -> y
  | Succ x' -> Succ (add x' y)

let and (x: bool) (y: bool) : bool =
  match (x, y) with
  | False, False -> False
  | ...

type bool = True | False
```

= Langage

```EBNF
expr = type def
  | let in
  | ;


(* TYPES *)

type def = "type", space, type name, space opt, "=", type ;

type = type product
  | type sum ;

type product = "(", [ type ], ")"
  | "(", type, { ",", type } ")" ;

type sum = ident constructor


(* WHITE SPACES *)

space = space characters, space opt ;
space opt = { space characters } ;
space characters = " " | "\n" | "\t" | "\r" | "\f" | "\b"  ;
```

```ocaml
namespace nat {

} in
type 'a option = None | Some 'a in
let f x = x * x in
```

== Types

Aucun type n'est définit par défaut, ils doivent
tous être déclarés.
```ocaml
type vec = (int, int);

type nat =
  | Zero
  | Succ nat;
```

```
<type-def> ::= "type" <spaces> <type-name>
```

== Variables et fonctions
```ocaml
let x = Succ Zero;
let succ t = Succ t;
```

== Sortie

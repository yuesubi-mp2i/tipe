#set text(lang: "fr")
#set page(
  header: [MCOT #h(1fr) #smallcaps[Lux] Matthieu],
  numbering: "1 / 1"
)

#let todo = text(fill: gray)[À faire]
#let todo-later = text(fill: gray)[À faire plus tard]


#align(center)[
  = Preuves de correction automatiques des programmes
]


== Professeur encadrant du candidat
Matthieu Journault

== Motivation
Pour tenter d'éliminer les bogues de leurs programmes, les développeurs testent leur code sur des exemples.
Mais pour être assuré qu'un programme est correct, il faut écrire une preuve qui assure son bon fonctionnement.
C'est un processus long et fastidieux que l'on aimerait pouvoir déléguer à un ordinateur.

== Ancrage au thème de l'année (transition, transformation, conversion)
Dans ce TIPE on essaye de transformer des propriétés sur des programmes en preuves de correction.

== Positionnement thématique
- Informatique théorique
- Mathématiques (Autres)

== Mots-clés
#table(
  columns: 2,
  stroke: none,
  [
    === Mots-clés (en français)
    - Démonstration automatique de théorème
    - Induction structurelle
    - Terminaison
    - Variant
    - Programmation fonctionnelle
  ],
  [
    === Mots-clés (en anglais)
    - Automated theorem proving
    - Structural induction
    - Halting
    - Variant
    - Functional programming
  ]
)

== Bibliographie commentée (max 650)
Une partie de l'étude de la correction d'un programme consiste à déterminer si ce programme termine sur toutes ses entrées et s'il vérifie des propriétés appelées post-conditions décrivant les effets attendus à l'issue de son exécution.

L'analyse de la correction des programmes rencontre un problème fondamental : il n'existe pas de programme vérifiant la terminaison d'un autre programme.
Ce problème, appelé le problème de l'arrêt, est alors dit indécidable @halting.
Ce résultat permet de déduire que d'autres problèmes sont indécidables grâce aux réductions.
Un résultat important est le théorème de Rice qui énonce que pour toute propriété dite non triviale de la sémantique d'un programme, le problème de tester cette propriété est indécidable @rice.

On peut, pour contourner ce problème, restreindre notre langage de programmation pour ne contenir que des fonctions pour lesquelles il est possible de prouver la terminaison.
Un tel langage est dit total @total.
Pour prouver qu'un algorithme termine, il suffit de trouver un objet, appelé variant, qui décroît strictement au cours de la progression de l'algorithme et qui est à valeurs dans un ensemble dit bien ordonné.

Quant à la vérification des post-conditions.
Il faut se munir de règles formelles de déduction pour que l'ordinateur puisse raisonner.
Ces règles sont appelés règles d'inférence.
Et un ensemble de telles règles est appelé un système de preuve.

Le typage est un exemple de propriété sur les variables d'un programme.
C'est une propriété importante car souvent nécessaire pour la compilation du code.
Dans de nombreux langages l'utilisateur doit donc explicitement donner le type de chaque variable.
Il est alors nécessaire, à minima, de vérifier que l'utilisation des variables est cohérente avec leur type.
Le système de types de Hindley-Milner donne un système de preuves qui permet de vérifier cette cohérence.
Et même mieux que cela, il existe un algorithme pouvant déduire les types automatiquement @hm.
Ainsi, dans certains langages de programmation, les types sont déterminés automatiquement.

Les triplets de Hoare, eux, formalisent les effets des instructions d'un algorithme sur les préconditions @hoare.
Ce système de preuves décrit, pour chaque opération élémentaire d'un langage de programmation impératif, comment démontrer que sous certaines hypothèses une opération donnée produit certains effets.
Dans un langage de programmation fonctionnel, les boucles sont remplacées par des appels récursifs.
Il faut donc d'autres règles d'inférence, dont une qui décrit les effets d'un appel récursif.

En mathématiques, on utilise le théorème de récurrence qui permet de prouver une propriété en examinant deux façons qui suffisent pour construire tous les entiers : le constructeur zéro et le constructeur successeur.
Et ce théorème marche même si cette propriété fait intervenir un objet défini par récurrence.
Ce type de raisonnement s'étend aux objets dits inductifs, qui sont construits avec des objets de base et des règles de construction.
On possède alors un théorème qui généralise le principe de récurrence, appelé théorème d'induction structurelle @better-tipe.
De plus les objets inductifs produisent naturellement un ordre bien fondé, on peut donc aussi les utiliser comme variants pour prouver la terminaison.

Une fois le système de preuves défini, on a les outils permettant de construire des preuves.
Mais leur recherche automatique est souvent très difficile.
Sans compter qu'il se pourrait même qu'il existe une post-condition vraie, mais ne pouvant pas être prouvée dans le système de preuves choisi.

== Problématique retenue (max 50)
Comment générer des preuves de correction automatiquement dans un langage de programmation total et purement fonctionnel ?

== Objectifs du TIPE du candidat (max 100)
- Définir un langage de programmation jouet.

- Construire un parseur du langage pour produire la structure en mémoire des programmes à manipuler.

- Réussir à prouver des égalités sémantiques par réécritures successives.

- Réussir à trouver et prouver un variant pour assurer la terminaison.

- Réussir à prouver des propriétés, portant sur des fonctions récursives, par induction structurelle.

== Références bibliographiques (2-10 références)
#bibliography(title: none, "biblio.yml")

== DOT
#todo-later
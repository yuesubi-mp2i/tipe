mod analysis;
mod lang;
mod process;

use std::io;
use colored::Colorize;
use clap::Parser;
use inquire::Text;

use process::read;


#[derive(clap::Parser)]
struct Cli {
    file_path: std::path::PathBuf
}

fn main() -> io::Result<()> {
    let args = Cli::parse();
    println!("Content of {:?}", args.file_path);
    let content = std::fs::read_to_string(args.file_path)
        .unwrap();

    let ast = read::ast(content.chars());

    // use process::Linker;
    // let mut linker = Linker::new();
    // let ast = linker.ast(ast).unwrap();

    // use process::Typer;
    // let mut typer = Typer::new();
    // let ast = typer.type_ast(ast).unwrap();
    // println!("{:?}", ast.statements.iter().map(|stmt| stmt.val.meta.typ.clone()).collect::<Vec<_>>());

    loop {
        let input = Text::new("> ")
            .with_placeholder("x y. x = y")
            .with_help_message("Entrez une propriété")
            .prompt()
            .unwrap();

        let prop = read::property(input.chars());
        println!("{} {:?}", "=>".blue().bold(), prop);

        match analysis::prove(&prop, &ast) {
            Ok(proof) =>
                println!("{} :\n{}", "True".green(), proof),
            Err(counter_ex) =>
                println!("{} :\n{}", "Could not deduce".red(), counter_ex)
        }
    }
}

mod lang;
mod link;
mod solv;

mod typ;


use std::io;
use colored::Colorize;
use clap::Parser;
use inquire::Text;

use lang::read;


#[derive(clap::Parser)]
struct Cli {
    file_path: std::path::PathBuf
}

fn main() -> io::Result<()> {
    let args = Cli::parse();
    println!("Content of {:?}", args.file_path);
    let content = std::fs::read_to_string(args.file_path)
        .unwrap();

    let ast = lang::read::ast(content.chars());

    loop {
        //println!("{}", "Enter property :".purple().bold());

        let input = Text::new("> ")
            .with_placeholder("x y. x = y")
            .with_help_message("Entrez une propriété")
            .prompt()
            .unwrap();

        let prop = read::property(input.chars());
        //println!("{:#?}", prop);
        println!("{} {:?}", "=>".blue().bold(), prop);

        match solv::prove(&prop, &ast) {
            Ok(proof) =>
                println!("{} :\n{}", "True".green(), proof),
            Err(counter_ex) =>
                println!("{} :\n{}", "Could not deduce".red(), counter_ex)
        }
    }
}

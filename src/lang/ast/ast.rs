use super::{ Statement, TypeDef };
use super::kind::Kind;


#[derive(Debug, Clone)]
pub struct AST<K: Kind> {
    pub type_defs: Vec<TypeDef<K>>,
    pub statements: Vec<Statement<K>>,
}
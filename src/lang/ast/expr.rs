use std::fmt::Debug;

use super::{ Pattern, Var };
use super::kind::Kind;


#[derive(Clone, PartialEq, Eq)]
pub struct Expr<K: Kind> {
    pub data: ExprData<K>,
    pub meta: K::ExprMeta
}

#[derive(Clone, PartialEq, Eq)]
pub enum ExprData<K: Kind> {
    Binding {
        var: Var<K>,
        val: Box<Expr<K>>,
        body: Box<Expr<K>>,
    },
    Function {
        input: Var<K>,
        out_type: K::TypeAnnotation,
        body: Box<Expr<K>>,
    },
    Call {
        caller: Box<Expr<K>>,
        arg: Box<Expr<K>>,
    },
    Match {
        expr: Box<Expr<K>>,
        cases: Vec<MatchBranch<K>>,
    },
    LitVar {
        id: K::Id,
    },
    LitConstructor {
        id: K::Id,
        args: Vec<Expr<K>>,
    },
}


#[derive(Clone, PartialEq, Eq)]
pub struct MatchBranch<K: Kind> {
    pub pattern: Pattern<K>,
    pub body: Expr<K>,
}


impl<K: Kind> Debug for Expr<K> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?} <{:?}>", self.data, self.meta)
    }
}

impl<K: Kind> Debug for ExprData<K> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use ExprData::*;

        // TODO: write types
        match self {
            Binding { var, val, body } =>
                write!(f, "let {:?} = {:?} in {:?}", var, val, body),
            Function { input, body, .. } =>
                write!(f, "fun {:?} -> {:?}", input, body),
            Call { caller, arg } =>
                match caller.data {
                    LitVar { .. } | Call { .. } =>
                        write!(f, "{:?} {:?}", caller, arg),
                    _ =>
                        write!(f, "({:?}) {:?}", caller, arg),
                }
            Match { expr, cases } => {
                write!(f, "match {:?} with", expr)?;
                for case in cases {
                    write!(f, " | {:?}", case)?;
                }
                Ok(())
            },
            LitVar { id } =>
                write!(f, "{:?}", id),
            LitConstructor { id, args } => {
                write!(f, "{:?}", id)?;
                let mut tpl = f.debug_tuple("");
                for arg in args {
                    tpl.field(arg);
                }
                tpl.finish()
            }
        }
    }
}


impl<K: Kind> Debug for MatchBranch<K> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?} -> {:?}", self.pattern.data, self.body)
    }
}
use std::fmt::Debug;

use super::{Var, Expr};
use super::kind::Kind;


#[derive(Clone)]
pub struct Statement<K: Kind> {
    pub var: Var<K>,
    pub val: Expr<K>,
}


impl<K: Kind> Debug for Statement<K> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "let {:?} = {:?};;", self.var, self.val)
    }
}
use crate::lang::ast::{Expr, ExprData, Pattern, PatternData};

use super::*;


#[derive(Debug, Clone, PartialEq, Eq)]
/// Linked
pub struct Linked {}

impl Kind for Linked {
    type Id = LId;
    type TypeAnnotation = Option<Type<Linked>>;
    type ExprMeta = ();
    type PatternMeta = ();

    fn link_annotation(annotation: Self::TypeAnnotation, linker: &mut Linker<Self>)
        -> Result<<Linked as Kind>::TypeAnnotation, LinkingErr>
    {
        annotation.map(|typ| linker.typ(typ))
            .map_or(
                Ok(None),
                |res|
                    res.map(|typ| Some(typ))
            )
    }
}


impl From<ExprData<Linked>> for Expr<Linked> {
    fn from(data: ExprData<Linked>) -> Self {
        Expr { data, meta: () }
    }
}


impl From<PatternData<Linked>> for Pattern<Linked> {
    fn from(data: PatternData<Linked>) -> Self {
        Pattern { data, meta: () }
    }
}
use super::*;


#[derive(Debug, Clone, PartialEq, Eq, Hash)]
/// Linked & typed
pub struct LT {}

impl Kind for LT {
    type Id = LId;
    type TypeAnnotation = ();
    type ExprMeta = LTNodeMeta;
    type PatternMeta = LTNodeMeta;

    fn link_annotation(annotation: Self::TypeAnnotation, linker: &mut Linker<Self>)
        -> Result<<Linked as Kind>::TypeAnnotation, LinkingErr>
    {
        todo!()
    }
}


#[derive(Clone, PartialEq, Eq)]
pub struct LTNodeMeta {
    pub typ: Type<LT>
}


impl From<Type<LT>> for LTNodeMeta {
    fn from(typ: Type<LT>) -> Self {
        LTNodeMeta { typ }
    }
}


impl Debug for LTNodeMeta {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.typ)
    }
}
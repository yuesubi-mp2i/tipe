use crate::lang::ast::{Expr, ExprData, Pattern, PatternData};

use super::*;


#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Raw {}
impl Kind for Raw {
    type Id = String;
    type TypeAnnotation = Option<Type<Raw>>;
    type ExprMeta = ();
    type PatternMeta = ();

    fn link_annotation(annotation: Self::TypeAnnotation, linker: &mut Linker<Self>)
        -> Result<<Linked as Kind>::TypeAnnotation, LinkingErr>
    {
        annotation.map(|typ| linker.typ(typ))
            .map_or(
                Ok(None),
                |res|
                    res.map(|typ| Some(typ))
            )
    }
}


impl From<ExprData<Raw>> for Expr<Raw> {
    fn from(data: ExprData<Raw>) -> Self {
        Expr { data, meta: () }
    }
}


impl From<PatternData<Raw>> for Pattern<Raw> {
    fn from(data: PatternData<Raw>) -> Self {
        Pattern { data, meta: () }
    }
}
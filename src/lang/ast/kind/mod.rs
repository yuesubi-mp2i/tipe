mod linked;
mod raw;
mod lt;

pub use linked::Linked;
pub use raw::Raw;
pub use lt::LT;

use std::{fmt::Debug, hash::Hash};
use crate::process::{Linker, LinkingErr};

use super::Type;


pub trait Kind {
    type Id: Debug + Clone + Eq + Hash + ToString;
    type TypeAnnotation: Debug + Clone + Eq;
    type ExprMeta: Debug + Clone + Eq;
    type PatternMeta: Debug + Clone + Eq;

    fn link_annotation(annotation: Self::TypeAnnotation, linker: &mut Linker<Self>)
        -> Result<<Linked as Kind>::TypeAnnotation, LinkingErr>
        where Self: Sized;
}


#[derive(Clone, Eq)]
pub struct LId {
    pub id: usize,
    pub name: String,
}

impl LId {
    pub fn unnamed(id: usize) -> LId {
        LId { id, name: id.to_string() }
    }
}

impl From<(usize, &str)> for LId {
    fn from((id, str_name): (usize, &str)) -> Self {
        LId { id, name: String::from(str_name) }
    }
}

impl Debug for LId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.name)?;
        subscript(f, self.id)
    }
}

fn subscript(f: &mut std::fmt::Formatter<'_>, id: usize) -> std::fmt::Result {
    for digit in id.to_string().chars() {
        write!(f, "{}", match digit {
            '0' => "₀",
            '1' => "₁",
            '2' => "₂",
            '3' => "₃",
            '4' => "₄",
            '5' => "₅",
            '6' => "₆",
            '7' => "₇",
            '8' => "₈",
            '9' => "₉",
            _ => "₋"
        })?
    }
    Ok(())
}

impl ToString for LId {
    fn to_string(&self) -> String {
        self.name.clone()
    }
}

impl PartialEq for LId {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl std::hash::Hash for LId {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.id.hash(state)
    }
}
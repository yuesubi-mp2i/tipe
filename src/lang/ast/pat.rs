use std::fmt::Debug;

use super::kind::Kind;


#[derive(Clone, PartialEq, Eq)]
pub struct Pattern<K: Kind> {
    pub data: PatternData<K>,
    pub meta: K::PatternMeta
}

#[derive(Clone, PartialEq, Eq)]
pub enum PatternData<K: Kind> {
    Var(Var<K>),
    Constructor {
        id: K::Id,
        args: Vec<Pattern<K>>,
    }
}


#[derive(Clone, PartialEq, Eq)]
pub struct Var<K: Kind> {
    pub id: K::Id,
    pub typ: K::TypeAnnotation,
    pub meta: K::PatternMeta
}


impl<K: Kind> Debug for Pattern<K> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.data)
    }
}


impl<K: Kind> Debug for PatternData<K> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            PatternData::Var(var) =>
                write!(f, "{:?}", var),
            PatternData::Constructor { id, args } => {
                write!(f, "{:?}", id)?;
                let mut tpl = f.debug_tuple("");
                for arg in args {
                    tpl.field(&arg.data);
                }
                tpl.finish()
            }
        }
    }
}

impl<K: Kind> Debug for Var<K> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.id)
    }
}
use std::{fmt::{write, Debug}, hash::Hash};
use crate::link::IdGenerator;

use super::Type;


pub trait Kind {
    type Id: Debug + Clone + Eq + Hash + ToString;
    type Type: Debug + Clone + Eq;
    type Meta: Debug + Clone;
}


#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Raw {}
impl Kind for Raw {
    type Id = String;
    type Type = Option<Type<Raw>>;
    type Meta = ();
}


#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Typed {}
impl Kind for Typed {
    type Id = String;
    type Type = String;
    type Meta = ();
}


#[derive(Debug, Clone, PartialEq, Eq)]
/// Linked & Untyped (LU)
pub struct LU {}

impl Kind for LU {
    type Id = LUId;
    type Type = ();
    type Meta = LUMeta;
}


#[derive(Debug, Clone)]
pub struct LUMeta {
    pub id_generator: IdGenerator,
}


#[derive(Clone, Eq)]
pub struct LUId {
    pub id: usize,
    pub name: String,
}

impl LUId {
    pub fn unnamed(id: usize) -> LUId {
        LUId { id, name: id.to_string() }
    }
}

impl From<(usize, &str)> for LUId {
    fn from((id, str_name): (usize, &str)) -> Self {
        LUId { id, name: String::from(str_name) }
    }
}

impl Debug for LUId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.name)?;
        subscript(f, self.id)
    }
}

fn subscript(f: &mut std::fmt::Formatter<'_>, id: usize) -> std::fmt::Result {
    for digit in id.to_string().chars() {
        write!(f, "{}", match digit {
            '0' => "₀",
            '1' => "₁",
            '2' => "₂",
            '3' => "₃",
            '4' => "₄",
            '5' => "₅",
            '6' => "₆",
            '7' => "₇",
            '8' => "₈",
            '9' => "₉",
            _ => "₋"
        })?
    }
    Ok(())
}

impl ToString for LUId {
    fn to_string(&self) -> String {
        self.name.clone()
    }
}

impl PartialEq for LUId {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl std::hash::Hash for LUId {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.id.hash(state)
    }
}
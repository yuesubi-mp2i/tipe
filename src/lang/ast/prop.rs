use std::fmt::Debug;

use super::{Expr, Var};
use super::kind::Kind;


#[derive(Clone)]
pub struct Property<K: Kind> {
    pub vars: Vec<Var<K>>,
    pub left: Expr<K>,
    pub right: Expr<K>
}

impl<K: Kind> Debug for Property<K> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for var in &self.vars {
            write!(f, "{:?} ", var)?;
        }
        write!(f, ". {:?} = {:?}", self.left, self.right)
    }
}
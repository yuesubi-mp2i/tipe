use std::fmt::{write, Debug};

use super::kind::Kind;


#[derive(Clone, Debug, PartialEq, Eq)]
pub struct TypeDef<K: Kind> {
    pub id: K::Id,
    pub arg_ids: Vec<K::Id>,
    pub typ: TypeDefType<K>,
}


#[derive(Clone, Debug, PartialEq, Eq)]
pub enum TypeDefType<K: Kind> {
    Type(Type<K>),
    TypeSum(Vec<TypeSumBranch<K>>),
}


#[derive(Clone, PartialEq, Eq, Hash)]
pub enum Type<K: Kind> {
    Generic {
        id: K::Id
    },
    Specialization{
        args: Vec<Type<K>>,
        typ: K::Id,
    },
    Function {
        input: Box<Type<K>>,
        output: Box<Type<K>>,
    },
}


#[derive(Clone, Debug, PartialEq, Eq)]
pub struct TypeSumBranch<K: Kind> {
    pub constructor_id: K::Id,
    pub args: Vec<Type<K>>,
}


impl<K: Kind> Debug for Type<K> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use Type::*;

        match self {
            Generic { id } =>
                write!(f, "{:?}", id),

            Function { input, output } =>
                write!(f, "({:?}) -> ({:?})", *input, *output),

            Specialization { args, typ } => {
                args.iter()
                    .map(|arg| write!(f, "({:?}) ", arg))
                    .collect::<std::fmt::Result>()?;
                write!(f, "{:?}", typ)
            }
        }
    }
}
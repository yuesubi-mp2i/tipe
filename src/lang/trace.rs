#[derive(Clone, Debug)]
pub enum TraceNode {
    BindingVar,
    BindingVal,
    BindingBody,
    FunctionInput,
    FunctionOutType,
    FunctionBody,
    CallCaller,
    CallArg,
    MatchExpr,
    MatchCases,
    LitVarId,
    LitConstructorId,
    LitConstructorArgs,

    MatchBranchPattern,
    MatchBranchBody,

    PatternVar,
    ConstructorId,
    ConstructorArgs,

    VarId,
    VarTyp,

    PropertyVars,
    PropertyLeft,
    PropertyRight,

    StatementVar,
    StatementVal,

    TypeDefId,
    TypeDefArgIds,
    TypeDefType,

    GenericId,
    SpecializationArgs,
    SpecializationType,
    TypeFunctionInput,
    TypeFunctionOutput,

    TypeSumBranchConstructorId,
    TypeSumBranchArgs,
}


#[derive(Clone, Debug)]
pub struct Trace {
    trace: Vec<TraceNode>
}

impl Trace {
    pub fn new() -> Trace {
        Trace { trace: Vec::new() }
    }

    pub fn enter(&mut self, node: TraceNode) {
        self.trace.push(node);
    }

    pub fn exit(&mut self) {
        self.trace.pop();
    }
}
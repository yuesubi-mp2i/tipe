#[derive(Debug, PartialEq, Clone)]
pub enum TokenData {
    AMP, DOT,
    ARROW, COMMA, STAR,
    COLUMN, DBLSEMICOL, EQUAL,
    PIPE, HASHTAG,
    LPAREN, RPAREN,

    LET, IN, FUN,
    MATCH, WITH,
    TYPE, OF,

    EOF,

    SnakeIdent(String),
    PascalIdent(String),
    GenericIdent(String),
}


#[derive(Debug, Clone)]
pub struct Token {
    pub line: usize,
    pub column: usize,
    pub data: TokenData
}


impl std::fmt::Display for TokenData {

    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use TokenData::*;

        write!(f, "{}",
            match self {
                DOT     => ".",
                AMP     => "&",
                ARROW   => "->",
                COMMA   => ",",
                STAR    => "*",
                COLUMN  => ":",
                DBLSEMICOL => ";;",
                EQUAL   => "=",
                PIPE    => "|",
                HASHTAG => "#",
                LPAREN  => "(",
                RPAREN  => ")",

                LET   => "let",
                IN    => "in",
                FUN   => "fun",
                MATCH => "match",
                WITH  => "with",
                TYPE  => "type",
                OF    => "of",

                EOF => "",
                
                SnakeIdent(ident)  => ident,
                PascalIdent(ident)   => ident,
                GenericIdent(ident) => ident,
            }
        )
    }
    
}
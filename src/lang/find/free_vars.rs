use std::collections::HashSet;

use crate::lang::*;


pub trait FindFreeVars<K: Kind> {
    fn free_vars(&self) -> HashSet<K::Id>;
}


impl<K: Kind> FindFreeVars<K> for AST<K> {
    fn free_vars(&self) -> HashSet<K::Id> {
        self.statements
            .iter()
            .flat_map(|stmt| stmt.free_vars().into_iter())
            .collect()
    }
}


impl<K: Kind> FindFreeVars<K> for Expr<K> {
    fn free_vars(&self) -> HashSet<K::Id> {
        self.data.free_vars()
    }
}


impl<K: Kind> FindFreeVars<K> for ExprData<K> {
    fn free_vars(&self) -> HashSet<K::Id> {
        use ExprData::*;

        match self {
            Binding { var, val, body } =>
                body.free_vars()
                    .difference(&var.free_vars())
                    .cloned()
                    .collect::<HashSet<_>>()
                    .union(&val.free_vars())
                    .cloned()
                    .collect(),

            Function { input, body, .. } =>
                body.free_vars()
                    .difference(&input.free_vars())
                    .cloned()
                    .collect(),

            Call { caller, arg } =>
                (*caller)
                    .free_vars()
                    .union(&(*arg).free_vars())
                    .cloned()
                    .collect(),

            Match { expr, cases } =>
                (*expr)
                    .free_vars()
                    .into_iter()
                    .chain(
                        cases.into_iter()
                            .flat_map(|case|
                                case.free_vars().into_iter()
                            )
                    )
                    .collect(),

            LitVar { id } =>
                HashSet::from([id.clone()]),

            LitConstructor { args, .. } =>
                args.into_iter()
                    .flat_map(|arg| arg.free_vars().into_iter())
                    .collect()
        }
    }
}


impl<K: Kind> FindFreeVars<K> for MatchBranch<K> {
    fn free_vars(&self) -> HashSet<K::Id> {
        self.body.free_vars()
            .difference(&self.pattern.free_vars())
            .cloned()
            .collect()
    }
}


impl<K: Kind> FindFreeVars<K> for Pattern<K> {
    fn free_vars(&self) -> HashSet<K::Id> {
        match &self.data {
            PatternData::Var(var) =>
                var.free_vars(),
            PatternData::Constructor { id, args } =>
                args.into_iter()
                    .flat_map(|arg|
                        arg.free_vars().into_iter()
                    )
                    .chain([id.clone()].into_iter())
                    .collect()
        }
    }
}


impl<K: Kind> FindFreeVars<K> for Var<K> {
    fn free_vars(&self) -> HashSet<K::Id> {
        HashSet::from([self.id.clone()])
    }
}


impl<K: Kind> FindFreeVars<K> for Statement<K> {
    fn free_vars(&self) -> HashSet<K::Id> {
        self.val.free_vars()
            .into_iter()
            .chain(self.var.free_vars().into_iter())
            .collect()
    }
}
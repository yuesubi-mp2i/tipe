use std::collections::HashSet;
use crate::lang::*;


pub trait FindTypes<K: Kind> {
    fn types(&self) -> HashSet<K::Id>;
}


impl<K: Kind> FindTypes<K> for AST<K> {
    fn types(&self) -> HashSet<K::Id> {
        self.type_defs.iter()
            .flat_map(|type_def| type_def.types())
            .collect()
    }
}


impl<K: Kind> FindTypes<K> for TypeDef<K> {
    fn types(&self) -> HashSet<K::Id> {
        HashSet::from([self.id.clone()])
    }
}
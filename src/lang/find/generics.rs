use std::collections::HashSet;
use crate::lang::*;


pub trait FindGenerics<K: Kind> {
    fn generics(&self) -> HashSet<K::Id>;
}


impl FindGenerics<Linked> for AST<Linked> {
    fn generics(&self) -> HashSet<<Linked as Kind>::Id> {
        self.type_defs.iter()
            .flat_map(|type_def| type_def.generics())
            .chain(
                self.statements.iter()
                    .flat_map(|statement| statement.generics())
            )
            .collect()
    }
}

impl FindGenerics<Linked> for TypeDef<Linked> {
    fn generics(&self) -> HashSet<<Linked as Kind>::Id> {
        self.arg_ids.clone()
            .into_iter()
            .collect()
    }
}

impl<K: Kind> FindGenerics<K> for Type<K> {
    fn generics(&self) -> HashSet<K::Id> {
        use Type::*;
        match self {
            Generic { id } => HashSet::from([id.clone()]),
            Specialization { args, .. } =>
                args.into_iter()
                    .flat_map(|arg|
                        arg.generics().into_iter()
                    )
                    .collect(),
            Function { input, output } =>
                input.generics()
                    .into_iter()
                    .chain(output.generics().into_iter())
                    .collect()
        }
    }
}

impl FindGenerics<Linked> for Statement<Linked> {
    fn generics(&self) -> HashSet<<Linked as Kind>::Id> {
        self.var.generics()
            .into_iter()
            .chain(self.val.generics().into_iter())
            .collect()
    }
}

impl FindGenerics<Linked> for Expr<Linked> {
    fn generics(&self) -> HashSet<<Linked as Kind>::Id> {
        self.data.generics()
    }
}

impl FindGenerics<Linked> for ExprData<Linked> {
    fn generics(&self) -> HashSet<<Linked as Kind>::Id> {
        use ExprData::*;
        match self {
            Binding { var, val, body } =>
                var.generics()
                    .into_iter()
                    .chain(val.generics().into_iter())
                    .chain(body.generics().into_iter())
                    .collect(),

            Function { input, out_type, body } =>
                input.generics()
                    .into_iter()
                    .chain(out_type.generics().into_iter())
                    .chain(body.generics().into_iter())
                    .collect(),
            
            Call { caller, arg } =>
                caller.generics()
                    .into_iter()
                    .chain(arg.generics().into_iter())
                    .collect(),
            
            Match { expr, cases } =>
                cases.into_iter()
                    .flat_map(|case| case.generics())
                    .chain(expr.generics().into_iter())
                    .collect(),
                    
            LitVar { .. } => HashSet::new(),

            LitConstructor { args, .. } =>
                args.into_iter()
                    .flat_map(|arg| arg.generics())
                    .collect(),
        }
    }
}

impl FindGenerics<Linked> for MatchBranch<Linked> {
    fn generics(&self) -> HashSet<<Linked as Kind>::Id> {
        self.pattern.generics()
            .into_iter()
            .chain(self.body.generics().into_iter())
            .collect()
    }
}

impl FindGenerics<Linked> for Pattern<Linked> {
    fn generics(&self) -> HashSet<<Linked as Kind>::Id> {
        self.data.generics()
    }
}

impl FindGenerics<Linked> for PatternData<Linked> {
    fn generics(&self) -> HashSet<<Linked as Kind>::Id> {
        match self {
            PatternData::Var(var) =>
                var.generics(),
            PatternData::Constructor { args, .. } =>
                args.into_iter()
                    .flat_map(|arg| arg.generics())
                    .collect()
        }
    }
}

impl FindGenerics<Linked> for Var<Linked> {
    fn generics(&self) -> HashSet<<Linked as Kind>::Id> {
        self.typ.generics()
    }
}

impl FindGenerics<Linked> for Option<Type<Linked>> {
    fn generics(&self) -> HashSet<<Linked as Kind>::Id> {
        match self {
            None => HashSet::new(),
            Some(typ) => typ.generics()
        }  
    }
}
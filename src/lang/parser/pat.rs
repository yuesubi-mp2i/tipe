use super::*;


impl<I: Iterator<Item = Token>> Parser<I> {
    pub fn pattern(&mut self) -> ParseResult<Pattern<Raw>> {
        Ok(match &self.peek()?.data {
            SnakeIdent(_) =>
                Pattern::Var(self.variable()?),
            PascalIdent(_) =>
                self.pattern_constructor()?,
            _ => return  Err(UnexpectedToken {
                expected: String::from("pattern"),
                got: self.next()?
            })
        })
    }

    pub fn pattern_constructor(&mut self) -> ParseResult<Pattern<Raw>> {
        let id = match &self.peek()?.data {
            PascalIdent(id) => id.clone(),
            _ => return Err(UnexpectedToken {
                expected: String::from("constructor identifier"),
                got: self.next()?
            })
        };
        self.next()?;

        let mut arguments = Vec::new();

        match &self.peek()?.data {
            LPAREN => {
                self.consume(LPAREN)?;
                arguments.push(self.pattern()?);

                while self.peek()?.data == COMMA {
                    self.consume(COMMA)?;
                    arguments.push(self.pattern()?);
                }

                self.consume(RPAREN)?;
            },

            SnakeIdent(_) | PascalIdent(_) =>
                arguments.push(self.pattern()?),

            _ => ()
        }

        Ok(Pattern::Constructor { id, args: arguments })
    }

    pub fn variable(&mut self) -> ParseResult<Var<Raw>> {
        let id = match &self.peek()?.data {
            SnakeIdent(id) => id.clone(),
            _ => return Err(UnexpectedToken {
                expected: String::from("variable identifier"),
                got: self.next()?
            })
        };
        self.next()?;

        let typ = match &self.peek()?.data {
            COLUMN => {
                self.consume(COLUMN)?;
                Some(self.typ()?)
            },
            _ => None
        };

        Ok(Var { id, typ })
    }
}


#[cfg(test)]
mod tests {
    use super::super::tests::str_to_parser;
    use super::*;

    use Pattern::{Constructor, Var as PVar};
    use Type::*;


    #[test]
    fn test_pattern_constructor() -> ParseResult<()> {
        assert_eq!(
            str_to_parser("Zero").pattern_constructor()?,
            Constructor { id: String::from("Zero"), args: vec![] }
        );

        assert_eq!(
            str_to_parser("Tuple (Succ n, n')").pattern_constructor()?,
            Constructor {
                id: String::from("Tuple"),
                args: vec![
                    Constructor {
                        id: String::from("Succ"),
                        args: vec![
                            PVar(Var { id: String::from("n"), typ: None })
                        ]
                    },
                    PVar(Var { id: String::from("n'"), typ: None })
                ]
            }
        );

        Ok(())
    }


    #[test]
    fn test_variable() -> ParseResult<()> {
        assert_eq!(
            str_to_parser("_").variable()?,
            Var { id: String::from("_"), typ: None }
        );

        assert_eq!(
            str_to_parser("n: nat").variable()?,
            Var {
                id: String::from("n"),
                typ: Some(Specialization {
                    args: vec![],
                    typ: String::from("nat")
                })
            }
        );

        // TODO: correct the variable parsing / or the match parsing
        // assert_eq!(
        //     str_to_parser("n: nat -> n").variable()?,
        //     Var {
        //         id: String::from("n"),
        //         typ: Some(Specialization {
        //             args: vec![],
        //             typ: String::from("nat")
        //         })
        //     }
        // );

        Ok(())
    }
}
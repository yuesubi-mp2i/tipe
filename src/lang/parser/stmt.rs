use super::*;


impl<I: Iterator<Item = Token>> Parser<I> {
    pub fn statements(&mut self) -> ParseResult<Vec<Statement<Raw>>> {
        let mut res = Vec::new();

        loop {
            match self.peek()? {
                Token { data: TokenData::LET, .. } =>
                    res.push(self.statement()?),
                _ => break
            }
        }

        Ok(res)
    }

    pub fn statement(&mut self) -> ParseResult<Statement<Raw>> {
        self.consume(TokenData::LET)?;
        let variable = self.variable()?;
        self.consume(TokenData::EQUAL)?;
        let val = self.expr()?;
        self.consume(TokenData::DBLSEMICOL)?;

        Ok(Statement { var: variable, val })
    }
}
use crate::lang::*;
use super::lex::*;
use super::parse::*;


pub fn lex_and_unwrap<I: IntoIterator<Item = char>>(input: I) -> impl Iterator<Item = Token> {
    lex(input)
        .map(|res_tok|
            match res_tok {
                Ok(tok) => tok,
                Err(UnrecognizedToken { line, column, text }) =>
                    panic!(
                        "Lexing error at {}:{} : unrecognized token '{}'.",
                        line, column,
                        text
                    )
            }
        )
}


fn ast_node_unwrap<N>(node: Result<N, ParsingErr>) -> N {
    use ParsingErr::*;

    match node {
        Ok(node) => node,

        Err(UnexpectedToken { expected, got }) => {
            panic!(
                "Parsing error at {}:{} : expected '{}' got '{}'.",
                got.line, got.column, expected, got.data
            );
        }
    }
}


pub fn ast<I: IntoIterator<Item = char>>(input: I) -> AST<Raw> {
    ast_node_unwrap(Parser::new(lex_and_unwrap(input)).ast())
}

pub fn type_def<I: IntoIterator<Item = char>>(input: I) -> TypeDef<Raw> {
    ast_node_unwrap(Parser::new(lex_and_unwrap(input)).type_def())
}

pub fn expr<I: IntoIterator<Item = char>>(input: I) -> Expr<Raw> {
    ast_node_unwrap(Parser::new(lex_and_unwrap(input)).expr())
}

pub fn property<I: IntoIterator<Item = char>>(input: I) -> Property<Raw> {
    ast_node_unwrap(Parser::new(lex_and_unwrap(input)).property())
}

pub fn pattern<I: IntoIterator<Item = char>>(input: I) -> Pattern<Raw> {
    ast_node_unwrap(Parser::new(lex_and_unwrap(input)).pattern())
}

pub fn var<I: IntoIterator<Item = char>>(input: I) -> Var<Raw> {
    ast_node_unwrap(Parser::new(lex_and_unwrap(input)).variable())
}
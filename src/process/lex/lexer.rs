use std::iter::{Iterator, Peekable};
use super::token::*;


// TODO: fix broken line & column error messages
pub fn lex<I: IntoIterator<Item = char>>(input: I)
    -> impl Iterator<Item = Result<Token, UnrecognizedToken>>
{
    Lexer::new(input.into_iter())
}


#[derive(Debug)]
pub struct  UnrecognizedToken {
    pub line: usize,
    pub column: usize,
    pub text: String
}


struct Lexer<I: Iterator<Item = char>> {
    line: usize,
    column: usize,
    input: Peekable<I>,
}


impl<I: Iterator<Item = char>> Iterator for Lexer<I> {
    type Item = Result<Token, UnrecognizedToken>;

    fn next(&mut self) -> Option<Self::Item> {
        self.consume_eventual_whitespaces();
        self.consume_token()
            .or(Some(Ok(Token { line: 0, column: 0, data: TokenData::EOF })))
    }
}


impl<I: Iterator<Item = char>> Lexer<I> {

    fn new(iter: I) -> Lexer<I> {
        Lexer {
            line: 1,
            column: 1,
            input: iter.into_iter().peekable()
        }
    }

    fn peek(&mut self) -> Option<char> {
        self.input.peek().map(|&c| c)
    }

    fn next_alt(&mut self) -> Option<char> {
        let c_opt = self.input.next();

        if let Some(c) = c_opt {
            if c == '\n' {
                self.line += 1;
                self.column = 1;
            } else {
                self.column += 1;
            }
        }

        c_opt
    }

    fn consume_eventual_whitespaces(&mut self) {
        while self.peek().is_some_and(|c| c.is_ascii_whitespace()) {
            self.next_alt();
        }
    }

    fn consume_token(&mut self) -> Option<Result<Token, UnrecognizedToken>> {
        use TokenData::*;

        let data = match self.peek()? {
            '&' => self.single_char(AMP),
            '.' => self.single_char(DOT),
            ',' => self.single_char(COMMA),
            '*' => self.single_char(STAR),
            ':' => self.single_char(COLUMN),
            '=' => self.single_char(EQUAL),
            '|' => self.single_char(PIPE),
            '#' => self.single_char(HASHTAG),
            '(' => self.single_char(LPAREN),
            ')' => self.single_char(RPAREN),
            ';' => self.consume(";;").and(Ok(DBLSEMICOL)),
            '-' => self.consume("->").and(Ok(ARROW)),
            _ => self.word()
        };

        Some(data.map(|d| Token {
            line: self.line,
            column: self.column,
            data: d
        }))
    }

    fn single_char(&mut self, data: TokenData) -> Result<TokenData, UnrecognizedToken> {
        self.next_alt();
        Ok(data)
    }

    fn consume(&mut self, expected: &str) -> Result<(), UnrecognizedToken> {
        let mut unrecognized = UnrecognizedToken {
            line: self.line,
            column: self.column,
            text: String::new()
        };

        for ch in expected.chars() {
            if self.peek().is_none() {
                return Err(unrecognized);
            }
            unrecognized.text.push(self.peek().unwrap());

            if ch != self.peek().unwrap() {
                return Err(unrecognized);
            }

            self.next_alt();
        }

        Ok(())
    }

    fn word(&mut self) -> Result<TokenData, UnrecognizedToken> {
        assert!(self.peek().is_some());
        let ch = self.peek().unwrap();

        if ch == '\'' {
            self.generic()
        } else if ch.is_ascii_lowercase() || ch == '_' {
            self.variable_of_keyword()
        } else if ch.is_ascii_uppercase() {
            self.constructor()
        } else {
            Err(UnrecognizedToken {
                line: self.line, column: self.column,
                text: String::from(ch)
            })
        }
    }

    fn generic(&mut self) -> Result<TokenData, UnrecognizedToken> {
        assert!(self.peek().is_some());
        assert_eq!(self.peek().unwrap(), '\'');

        let mut unrecognized = UnrecognizedToken {
            line: self.line, column: self.column,
            text: String::new()
        };

        unrecognized.text.push('\'');
        self.next_alt();

        if self.peek().is_none() {
            return Err(unrecognized);
        }

        let ch = self.peek().unwrap();
        unrecognized.text.push(ch);
        if !(ch.is_ascii_lowercase() || ch == '_') {
            return Err(unrecognized)
        }
        self.next_alt();

        while self.peek()
            .filter(|&ch|
                ch.is_ascii_lowercase()
                || ch.is_ascii_digit()
                || ch == '_' )
            .is_some()
        {
            unrecognized.text.push(self.peek().unwrap());
        }

        Ok(TokenData::GenericIdent(unrecognized.text))
    }

    fn variable_of_keyword(&mut self) -> Result<TokenData, UnrecognizedToken> {
        assert!(self.peek().is_some());
        let ch = self.peek().unwrap();
        assert!(ch.is_ascii_lowercase() || ch == '_');

        let mut unrecognized = UnrecognizedToken {
            line: self.line, column: self.column,
            text: String::new()
        };

        unrecognized.text.push(ch);
        self.next_alt();

        while self.peek()
            .filter(|&ch|
                ch.is_ascii_lowercase()
                || ch.is_ascii_digit()
                || ch == '_'
                || ch == '\'')
            .is_some()
        {
            unrecognized.text.push(self.peek().unwrap());
            self.next_alt();
        }

        use TokenData::*;

        Ok(match unrecognized.text.as_str() {
            "let" => LET,
            "in" => IN,
            "fun" => FUN,
            "match" => MATCH,
            "with" => WITH,
            "type" => TYPE,
            "of" => OF,
            _ => SnakeIdent(unrecognized.text),
        })
    }

    fn constructor(&mut self) -> Result<TokenData, UnrecognizedToken> {
        assert!(self.peek().is_some());
        let ch = self.peek().unwrap();
        assert!(ch.is_ascii_uppercase());

        let mut unrecognized = UnrecognizedToken {
            line: self.line, column: self.column,
            text: String::new()
        };

        unrecognized.text.push(ch);
        self.next_alt();

        while self.peek()
            .filter(|ch| ch.is_ascii_alphanumeric())
            .is_some()
        {
            unrecognized.text.push(self.peek().unwrap());
            self.next_alt();
        }

        Ok(TokenData::PascalIdent(unrecognized.text))
    }

}

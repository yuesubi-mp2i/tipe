use crate::trace;
use crate::lang::*;

use super::{Linker, LinkResult, TraceNode};


use TraceNode::*;


impl<K: Kind> Linker<K> {
    pub fn statements(&mut self, raw: Vec<Statement<K>>)
        -> LinkResult<Vec<Statement<Linked>>>
    {
        raw.into_iter()
            .map(|s| self.statement(s))
            .collect()
    }

    fn statement(&mut self, raw: Statement<K>)
        -> LinkResult<Statement<Linked>>
    {
        let (var, val);
        trace!(self.trace, StatementVar, {
            var = self.variable(raw.var)?;
        });

        trace!(self.trace, StatementVal, {
            val = self.expr(raw.val)?;
        });

        Ok(Statement { var, val })
    }
}
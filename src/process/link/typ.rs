use crate::{id_scope, trace};
use crate::lang::*;

use super::*;


use TraceNode::*;


impl<K: Kind> Linker<K> {
    pub fn type_defs(&mut self, raw: Vec<TypeDef<K>>)
        -> LinkResult<Vec<TypeDef<Linked>>>
    {
        raw.into_iter()
            .map(|td| self.type_def(td))
            .collect()
    }

    fn type_def(&mut self, raw: TypeDef<K>) -> LinkResult<TypeDef<Linked>> {
        let (id, arg_ids, typ);

        trace!(self.trace, TypeDefId, {
            id = self.type_id_create(raw.id)?;
        });

        id_scope!(self.generic_ids, {
            trace!(self.trace, TypeDefArgIds, {
                arg_ids = raw.arg_ids
                    .into_iter()
                    .map(|raw_id| self.generic_id_create(raw_id))
                    .collect::<LinkResult<Vec<_>>>()?;
            });

            trace!(self.trace, TypeDefType, {
                use crate::lang::TypeDefType::*;
                typ = match raw.typ {
                    Type(raw_typ) => Type(self.typ(raw_typ)?),
                    TypeSum(raw_branches) =>
                        TypeSum(self.type_sum_branches(raw_branches)?)
                };
            });
        });

        Ok(TypeDef { id, arg_ids, typ })
    }

    fn type_sum_branches(&mut self, raw: Vec<TypeSumBranch<K>>)
        -> LinkResult<Vec<TypeSumBranch<Linked>>>
    {
        raw.into_iter()
            .map(|raw_branch| self.type_sum_branch(raw_branch))
            .collect()
    }

    fn type_sum_branch(&mut self, raw: TypeSumBranch<K>)
        -> LinkResult<TypeSumBranch<Linked>>
    {
        let (constructor_id, args);

        trace!(self.trace, TypeSumBranchConstructorId, {
            constructor_id = self.constructor_id_create(raw.constructor_id)?;
        });

        trace!(self.trace, TypeSumBranchArgs, {
            args = raw.args
                .into_iter()
                .map(|t| self.typ(t))
                .collect::<LinkResult<Vec<_>>>()?;
        });

        Ok(TypeSumBranch { constructor_id, args })
    }

    pub fn type_annotation(&mut self, raw: K::TypeAnnotation)
        -> LinkResult<<Linked as Kind>::TypeAnnotation>
    {
        K::link_annotation(raw, self)
    }

    pub fn typ(&mut self, raw: Type<K>) -> LinkResult<Type<Linked>> {
        use Type::*;

        Ok(match raw {
            Generic { id: raw_id } => {
                let id;
                trace!(self.trace, GenericId, {
                    id = self.generic_id_get_or_create(raw_id);
                });
                Generic { id }
            },

            Specialization { args: raw_args, typ: raw_type } => {
                let (args, typ);

                trace!(self.trace, SpecializationArgs, {
                    args = raw_args
                        .into_iter()
                        .map(|t| self.typ(t))
                        .collect::<LinkResult<Vec<Type<Linked>>>>()?;
                });

                trace!(self.trace, SpecializationType, {
                    typ = self.type_id_get(raw_type)?;
                });

                Specialization { args, typ }
            }

            Function { input: raw_input, output: raw_output } => {
                let (input, output);

                trace!(self.trace, TypeFunctionInput, {
                    input = Box::new(self.typ(*raw_input)?);
                });
                trace!(self.trace, TypeFunctionOutput, {
                    output = Box::new(self.typ(*raw_output)?);
                });

                Function { input, output }
            }
        })
    }
}


#[cfg(test)]
mod tests {
    use super::super::super::read;
    use super::*;


    #[test]
    fn test_type_def() {
        use read::type_def;

        let mut linker = Linker::new();
        
        assert!(linker.type_def(type_def(
            "type bool = False | True;;".chars()
        )).is_ok());

        assert!(linker.type_def(type_def(
            "type bool = False | True;;".chars()
        )).is_err());

        assert!(linker.type_def(type_def(
            "type alias = bool;;".chars()
        )).is_ok());

        assert!(linker.type_def(type_def(
            "type 'a option = None | Some of 'a;;".chars()
        )).is_ok());
        assert!(linker.type_def(type_def(
            "type 'a 'a wrong_option = 'a option;;".chars()
        )).is_err());
    }
}
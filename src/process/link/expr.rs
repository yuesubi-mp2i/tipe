use crate::{id_scope, lang::*, trace};

use super::*;


use TraceNode::*;


impl<K: Kind> Linker<K> {
    pub fn expr(&mut self, raw: Expr<K>) -> LinkResult<Expr<Linked>> {
        use ExprData::*;

        Ok(match raw.data {
            Binding { var: raw_var, val: raw_val, body: raw_body } => {
                let (var, val, body);
                trace!(self.trace, BindingVal, {
                    val = Box::new(self.expr(*raw_val)?);
                });

                id_scope!(self.var_ids, {
                    trace!(self.trace, BindingVar, {
                        var = self.variable(raw_var)?;
                    });

                    trace!(self.trace, BindingBody, {
                        body = Box::new(self.expr(*raw_body)?);
                    });
                });

                Binding { var, val, body }
            }

            Function { input: raw_input, out_type: raw_out_type, body: raw_body } => {
                let (input, out_type, body);

                id_scope!(self.var_ids, {
                    id_scope!(self.generic_ids, {
                        trace!(self.trace, FunctionInput, {
                            input = self.variable(raw_input)?;
                        });

                        trace!(self.trace, FunctionOutType, {
                            out_type = self.type_annotation(raw_out_type)?;
                        });

                        trace!(self.trace, FunctionBody, {
                            body = Box::new(self.expr(*raw_body)?);
                        });
                    });
                });

                Function { input, out_type, body }
            },

            Match { expr: raw_expr, cases: raw_cases } => {
                let (expr, cases);
                trace!(self.trace, MatchExpr, {
                    expr = Box::new(self.expr(*raw_expr)?);
                });

                trace!(self.trace, MatchCases, {
                    cases = raw_cases
                        .into_iter()
                        .map(|b| self.match_branch(b))
                        .collect::<LinkResult<Vec<_>>>()?;
                });

                Match { expr, cases }
            },

            Call { caller: raw_caller, arg: raw_arg } => {
                let (caller, arg);

                trace!(self.trace, CallCaller, {
                    caller = Box::new(self.expr(*raw_caller)?);
                });

                trace!(self.trace, CallArg, {
                    arg = Box::new(self.expr(*raw_arg)?);
                });

                Call { caller, arg }
            },

            LitVar { id: raw_id } => {
                let id;
                trace!(self.trace, LitVarId, {
                    id = self.var_id_get(raw_id)?;
                });
                LitVar { id }
            },

            LitConstructor { id: raw_id, args: raw_args } => {
                let (id, args);

                trace!(self.trace, LitConstructorId, {
                    id = self.constructor_id_get(raw_id)?;
                });

                trace!(self.trace, LitConstructorArgs, {
                    args = raw_args
                        .into_iter()
                        .map(|e| self.expr(e))
                        .collect::<LinkResult<Vec<_>>>()?;
                });

                LitConstructor { id, args }
            }
        }.into())
    }

    fn match_branch(&mut self, raw: MatchBranch<K>)
        -> LinkResult<MatchBranch<Linked>>
    {
        let (pattern, body);
        id_scope!(self.var_ids, {
            id_scope!(self.generic_ids, {
                trace!(self.trace, MatchBranchPattern, {
                    pattern = self.pattern(raw.pattern)?;
                });

                trace!(self.trace, MatchBranchBody, {
                    body = self.expr(raw.body)?;
                });
            });
        });

        Ok(MatchBranch { pattern, body })
    }
}
use crate::lang::*;

use crate::trace;
use super::*;


use TraceNode::*;


impl<K: Kind> Linker<K> {
    pub fn pattern(&mut self, raw: Pattern<K>) -> LinkResult<Pattern<Linked>> {
        use PatternData::*;

        Ok(match raw.data {
            Var(raw_var) => {
                let var;
                trace!(self.trace, PatternVar, {
                    var = self.variable(raw_var)?;
                });
                Var(var).into()
            },

            Constructor { id: raw_id, args: raw_args } => {
                let (id, args);
                trace!(self.trace, ConstructorId, {
                    id = self.constructor_id_get(raw_id)?;
                });

                trace!(self.trace, ConstructorArgs, {
                    args = raw_args
                        .into_iter()
                        .map(|p| self.pattern(p))
                        .collect::<LinkResult<Vec<Pattern<Linked>>>>()?;
                });

                Constructor { id, args }.into()
            }
        })
    }

    pub fn variable(&mut self, raw: Var<K>) -> LinkResult<Var<Linked>> {
        let (id, typ);

        trace!(self.trace, VarId, {
            id = self.var_id_create(raw.id)?;
        });

        trace!(self.trace, VarTyp, {
            typ = self.type_annotation(raw.typ)?;
        });

        Ok(Var { id, typ, meta: () })
    }
}
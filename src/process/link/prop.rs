use crate::trace;
use crate::lang::*;

use super::{Linker, LinkResult, TraceNode};

use TraceNode::*;


impl<K: Kind> Linker<K> {
    pub fn property(&mut self, raw: Property<K>) -> LinkResult<Property<Linked>> {
        let (vars, left, right);
        trace!(self.trace, PropertyVars, {
            vars = raw.vars
                .into_iter()
                .map(|var| self.variable(var))
                .collect::<LinkResult<_>>()?;
        });

        trace!(self.trace, PropertyLeft, {
            left = self.expr(raw.left)?;
        });
        trace!(self.trace, PropertyRight, {
            right = self.expr(raw.right)?;
        });

        Ok(Property { vars, left, right })
    }
}
use std::collections::HashMap;

use crate::lang::*;

use super::{
    IdGenerator,
    utils::*
};

mod ast;
mod expr;
mod pat;
mod prop;
mod stmt;
mod typ;


#[derive(Clone)]
pub struct Linker<K: Kind> {
    var_ids: ScopeStack<K::Id, usize>,
    generic_ids: ScopeStack<K::Id, usize>,
    constructor_ids: HashMap<K::Id, usize>,
    type_ids: HashMap<K::Id, usize>,

    var_counter: IdGenerator,
    generic_counter: IdGenerator,
    constructor_counter: IdGenerator,
    type_counter: IdGenerator,

    trace: Trace,
}


#[derive(Debug)]
pub enum LinkingErr {
    IdAlreadyInScope {
        id: String,
        trace: Trace,
    },
    UndefinedId {
        id: String,
        trace: Trace,
    }
}

type LinkResult<T> = Result<T, LinkingErr>;


impl<I: IntoIterator<Item = LId>> From<I> for Linker<Raw> {
    fn from(id_iter: I) -> Self {
        let ids: Vec<_> = id_iter.into_iter().collect();
        let max_usize = ids.iter()
            .map(|id| id.id)
            .max()
            .unwrap_or(0);

        let mut id_stack = ScopeStack::new();
        let frame = ids.iter()
            .map(|id| (id.name.clone(), id.id))
            .collect();
        id_stack.push_scope(frame);

        Linker {
            var_counter: IdGenerator::from(max_usize + 1),
            generic_counter: todo!(),
            constructor_counter: todo!(),
            type_counter: todo!(),
            trace: Trace::new(),
            var_ids: id_stack,
            constructor_ids: todo!(),
            generic_ids: todo!(),
            type_ids: todo!(),
        }
    }
}


#[macro_export]
macro_rules! id_scope {
    ($stack: expr, $code: block) => {
        {
            $stack.push_empty_scope();
            $code;
            $stack.pop_scope();
        }
    };
}

#[macro_export]
macro_rules! trace {
    ($tracer: expr, $node: ident, $code: block) => {
        {
            $tracer.enter($node);
            $code;
            $tracer.exit();
        }
    };
}


impl<K: Kind> Linker<K> {
    pub fn new() -> Linker<K> {
        let mut var_ids = ScopeStack::new();
        let mut generic_ids = ScopeStack::new();
        var_ids.push_empty_scope();
        generic_ids.push_empty_scope();

        let constructor_ids = HashMap::new();
        let type_ids = HashMap::new();

        Linker {
            var_ids,
            generic_ids,
            constructor_ids,
            type_ids,

            var_counter: IdGenerator::new(),
            generic_counter: IdGenerator::new(),
            constructor_counter: IdGenerator::new(),
            type_counter: IdGenerator::new(),

            trace: Trace::new(),
        }
    }

    fn var_id_get(&mut self, key: K::Id) -> LinkResult<LId> {
        match self.var_ids.get(&key) {
            Some(&val) => Ok(LId {
                id: val,
                name: key.to_string()
            }),
            None => self.var_id_create(key)
        }
    }

    fn var_id_create(&mut self, key: K::Id) -> LinkResult<LId> {
        match self.var_ids.get(&key) {
            None => {
                let val = self.var_counter.gen();
                self.var_ids.insert(key.clone(), val);
                Ok(LId { id: val, name: key.to_string() })
            }
            Some(_) => Err(LinkingErr::IdAlreadyInScope {
                id: key.to_string(),
                trace: self.trace.clone()
            })
        }
    }

    fn generic_id_get_or_create(&mut self, key: K::Id) -> LId {
        let id = match self.generic_ids.get(&key) {
            Some(&val) => val,
            None => {
                let val = self.generic_counter.gen();
                self.generic_ids.insert(key.clone(), val);
                val
            }
        };
        LId { id, name: key.to_string() }
    }

    fn generic_id_create(&mut self, key: K::Id) -> LinkResult<LId> {
        match self.generic_ids.get(&key) {
            None => {
                let val = self.generic_counter.gen();
                self.generic_ids.insert(key.clone(), val);
                Ok(LId { id: val, name: key.to_string() })
            }
            Some(_) => Err(LinkingErr::IdAlreadyInScope {
                id: key.to_string(),
                trace: self.trace.clone()
            })
        }
    }

    fn type_id_get(&self, key: K::Id) -> LinkResult<LId> {
        match self.type_ids.get(&key) {
            Some(&val) => Ok(LId {
                id: val,
                name: key.to_string()
            }),
            None => Err(LinkingErr::UndefinedId {
                id: key.to_string(),
                trace: self.trace.clone()
            })
        }
    }

    fn type_id_create(&mut self, key: K::Id) -> LinkResult<LId> {
        match self.type_ids.get(&key) {
            None => {
                let val = self.type_counter.gen();
                self.type_ids.insert(key.clone(), val);
                Ok(LId { id: val, name: key.to_string() })
            }
            Some(_) => Err(LinkingErr::IdAlreadyInScope {
                id: key.to_string(),
                trace: self.trace.clone()
            })
        }
    }

    fn constructor_id_get(&mut self, key: K::Id) -> LinkResult<LId> {
        match self.constructor_ids.get(&key) {
            Some(&val) => Ok(LId {
                id: val,
                name: key.to_string()
            }),
            None => self.constructor_id_create(key)
        }
    }

    fn constructor_id_create(&mut self, key: K::Id) -> LinkResult<LId> {
        match self.constructor_ids.get(&key) {
            None => {
                let val = self.constructor_counter.gen();
                self.constructor_ids.insert(key.clone(), val);
                Ok(LId { id: val, name: key.to_string() })
            }
            Some(_) => Err(LinkingErr::IdAlreadyInScope {
                id: key.to_string(),
                trace: self.trace.clone()
            })
        }
    }

    pub fn create_unassociated(&mut self) -> LId {
        LId::unnamed(self.var_counter.gen())
    }
    
    fn create_just_bellow(&mut self, key: K::Id) -> LinkResult<LId> {
        match self.var_ids.get(&key) {
            None => {
                let val = self.var_counter.gen();
                self.var_ids.insert_bellow(1, key.clone(), val);
                Ok(LId { id: val, name: key.to_string() })
            }
            Some(_) => Err(LinkingErr::IdAlreadyInScope {
                id: key.to_string(),
                trace: self.trace.clone()
            })
        }
    }
}


impl Linker<Linked> {
    pub fn reserve_id(&mut self, key: LId) -> LinkResult<()> {
        match self.var_ids.get(&key) {
            None => {
                // TODO: handle error
                self.var_counter.reserve(key.id).unwrap();
                self.var_ids.insert(key.clone(), key.id);
                Ok(())
            }
            Some(_) => Err(LinkingErr::IdAlreadyInScope {
                id: key.to_string(),
                trace: self.trace.clone()
            })
        }
    }
}
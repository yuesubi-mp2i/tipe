use crate::lang::*;

use super::{Linker, LinkResult};


impl<K: Kind> Linker<K> {
    pub fn ast(&mut self, raw: AST<K>) -> LinkResult<AST<Linked>> {
        let type_defs = self.type_defs(raw.type_defs)?;
        let statements = self.statements(raw.statements)?;

        Ok(AST {
            type_defs,
            statements,
        })
    }
}
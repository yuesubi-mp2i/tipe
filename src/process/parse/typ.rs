use super::*;


impl<I: Iterator<Item = Token>> Parser<I> {
    pub fn type_defs(&mut self) -> ParseResult<Vec<TypeDef<Raw>>> {
        let mut res = Vec::new();

        loop {
            match self.peek()?.data {
                TokenData::TYPE => res.push(self.type_def()?),
                _ => break
            }
        }

        Ok(res)
    }

    pub fn type_def(&mut self) -> ParseResult<TypeDef<Raw>> {
        self.consume(TokenData::TYPE)?;

        let mut argument_ids = Vec::new();

        // This is actually not the way OCaml parses type parameters if their
        // are more then two of them. But this way of doing it is easier to
        // parse.
        loop {
            match &self.peek()?.data {
                TokenData::GenericIdent(ident) => {
                    argument_ids.push(ident.clone());
                    self.next()?;
                },
                _ => break
            }
        }

        let id = match &self.peek()?.data {
            TokenData::SnakeIdent(ident) => ident.clone(),
            _ => return Err(UnexpectedToken {
                expected: String::from("type identifier"),
                got: self.next()?
            })
        };
        self.next()?;

        self.consume(TokenData::EQUAL)?;

        let typ = match self.peek()?.data {
            TokenData::PIPE | TokenData::PascalIdent(_)
                => TypeDefType::TypeSum(self.type_sum_branches()?),
            _ => TypeDefType::Type(self.typ()?)
        };

        self.consume(TokenData::DBLSEMICOL)?;

        return Ok(TypeDef { arg_ids: argument_ids, id, typ });
    }

    pub fn type_sum_branches(&mut self) -> ParseResult<Vec<TypeSumBranch<Raw>>> {
        let mut branches = Vec::new();

        loop {
            match self.peek()?.data {
                TokenData::PIPE | TokenData::PascalIdent(_) =>
                    branches.push(self.type_sum_branch()?),
                _ => break
            }
        }

        Ok(branches)
    }

    pub fn type_sum_branch(&mut self) -> ParseResult<TypeSumBranch<Raw>> {
        self.consume_if_exists(TokenData::PIPE);

        let id = match &self.peek()?.data {
            TokenData::PascalIdent(ident) => ident.clone(),
            _ => return Err(UnexpectedToken {
                expected: String::from("constructor identifier"),
                got: self.next()?
            })
        };
        self.next()?;

        let mut args = Vec::new();

        if self.peek()?.data ==  TokenData::OF {
            self.consume(TokenData::OF)?;

            args.push(self.short_type()?);

            loop {
                match self.peek()?.data {
                    TokenData::STAR => {
                        self.consume(TokenData::STAR)?;
                        args.push(self.short_type()?);
                    },
                    _ => break
                }
            }
        }

        Ok(TypeSumBranch {
            constructor_id: id,
            args
        })
    }

    pub fn typ(&mut self) -> ParseResult<Type<Raw>> {
        let t = self.short_type()?;

        if self.peek()?.data != TokenData::ARROW {
            return Ok(t);
        }
        self.consume(TokenData::ARROW)?;

        Ok(Type::Function {
            input: Box::new(t),
            output: Box::new(self.typ()?)
        })
    }

    pub fn short_type(&mut self) -> ParseResult<Type<Raw>> {
        let mut args = Vec::new();

        loop {
            match &self.peek()?.data {
                TokenData::GenericIdent(id) => {
                    args.push(Type::Generic { id: id.clone() });
                    self.next()?;
                },
                TokenData::SnakeIdent(id) => {
                    args.push(Type::Specialization {
                        args: Vec::new(),
                        typ: id.clone()
                    });
                    self.next()?;
                },
                TokenData::LPAREN => {
                    self.consume(TokenData::LPAREN)?;
                    args.push(self.typ()?);
                    self.consume(TokenData::RPAREN)?;
                }
                _ => break
            }
        }

        match args.pop() {
            Some(Type::Specialization {
                args: special_args,
                typ
            }) =>
                if special_args.is_empty() {
                    Ok(Type::Specialization { args, typ })
                } else {
                    Err(UnexpectedToken {
                        expected: String::from("type identifier"),
                        got: self.next()?
                    })
                },

            Some(not_specialization) if args.is_empty() =>
                Ok(not_specialization),

            _ => Err(UnexpectedToken {
                expected: String::from("generic, type identifier or parenthesized type"),
                got: self.next()?
            })
        }
    }
}


#[cfg(test)]
mod tests {
    use super::super::tests::str_to_parser;
    use super::*;

    use Type::*;


    #[test]
    fn test_type_def() -> ParseResult<()> {
        use TypeDefType::*;

        assert_eq!(
            str_to_parser("type bool = | True | False;;").type_def()?,
            TypeDef {
                id: String::from("bool"),
                arg_ids: vec![],
                typ: TypeSum(vec![
                    TypeSumBranch {
                        constructor_id: String::from("True"),
                        args: vec![]
                    },
                    TypeSumBranch {
                        constructor_id: String::from("False"),
                        args: vec![]
                    }
                ])
            }
        );

        assert_eq!(
            str_to_parser("type alt_bool = unit option;;").type_def()?,
            TypeDef {
                id: String::from("alt_bool"),
                arg_ids: vec![],
                typ: Type(Specialization {
                    args: vec![Specialization {
                        args: vec![],
                        typ: String::from("unit")
                    }],
                    typ: String::from("option")
                })
            }
        );

        assert_eq!(
            str_to_parser("type 'a list = Nil | Cons of 'a *'a list;;").type_def()?,
            TypeDef {
                id: String::from("list"),
                arg_ids: vec![String::from("'a")],
                typ: TypeSum(vec![
                    TypeSumBranch {
                        constructor_id: String::from("Nil"),
                        args: vec![]
                    },
                    TypeSumBranch {
                        constructor_id: String::from("Cons"),
                        args: vec![
                            Generic { id: String::from("'a") },
                            Specialization {
                                args: vec![Generic { id: String::from("'a") }],
                                typ: String::from("list")
                            }
                        ]
                    }
                ])
            }
        );

        Ok(())
    }

    #[test]
    fn test_type_sum_branch() -> ParseResult<()> {
        assert_eq!(
            str_to_parser("Zero").type_sum_branch()?,
            TypeSumBranch {
                constructor_id: String::from("Zero"),
                args: Vec::new()
            }
        );

        assert_eq!(
            str_to_parser("Fun of ('a -> 'b)").type_sum_branch()?,
            TypeSumBranch {
                constructor_id: String::from("Fun"),
                args: vec![Function {
                    input: Box::new(Generic { id: String::from("'a") }),
                    output: Box::new(Generic { id: String::from("'b") })
                }]
            }
        );

        assert_eq!(
            str_to_parser("Some of 'a").type_sum_branch()?,
            TypeSumBranch {
                constructor_id: String::from("Some"),
                args: vec![Generic { id: String::from("'a") }]
            }
        );

        assert_eq!(
            str_to_parser("Cons of 'a * 'a list").type_sum_branch()?,
            TypeSumBranch {
                constructor_id: String::from("Cons"),
                args: vec![
                    Generic { id: String::from("'a") },
                    Specialization {
                        args: vec![Generic { id: String::from("'a") }],
                        typ: String::from("list")
                    }
                ]
            }
        );

        Ok(())
    }

    #[test]
    fn test_typ() -> ParseResult<()> {
        assert_eq!(
            str_to_parser("('a -> 'b) -> ('b -> 'a)").typ()?,
            Function {
                input: Box::new(Function {
                    input: Box::new(Generic { id: String::from("'a") }),
                    output: Box::new(Generic { id: String::from("'b") })
                }),
                output: Box::new(Function {
                    input: Box::new(Generic { id: String::from("'b") }),
                    output: Box::new(Generic { id: String::from("'a") })
                })
            }
        );

        assert_eq!(
            str_to_parser("'a list -> (unit -> 'a) option").typ()?,
            Function {
                input: Box::new(Specialization {
                    args: vec![Generic { id: String::from("'a") }],
                    typ: String::from("list")
                }),
                output: Box::new(Specialization {
                    args: vec![Function {
                        input: Box::new(Specialization {
                            args: Vec::new(),
                            typ: String::from("unit")
                        }),
                        output: Box::new(Generic {
                            id: String::from("'a")
                        })
                    }],
                    typ: String::from("option")
                })
            }
        );

        Ok(())
    }

    #[test]
    fn test_short_type() -> ParseResult<()> {
        assert_eq!(
            str_to_parser("bool").short_type()?,
            Specialization { args: Vec::new(), typ: String::from("bool") }
        );
        assert_eq!(
            str_to_parser("nat -> nat").short_type()?,
            Specialization { args: Vec::new(), typ: String::from("nat") }
        );

        assert_eq!(
            str_to_parser("'a").short_type()?,
            Generic { id: String::from("'a") }
        );
        assert_eq!(
            str_to_parser("'a -> 'a").short_type()?,
            Generic { id: String::from("'a") }
        );

        assert_eq!(
            str_to_parser("('a -> 'a)").short_type()?,
            Function {
                input: Box::new(Generic { id: String::from("'a") }),
                output: Box::new(Generic { id: String::from("'a") })
            } 
        );

        Ok(())
    }
}
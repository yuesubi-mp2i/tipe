use super::*;


impl<I: Iterator<Item = Token>> Parser<I> {
    pub fn property(&mut self) -> ParseResult<Property<Raw>> {
        let mut vars = Vec::new();


        loop {
            match self.peek()?.data {
                TokenData::SnakeIdent { .. } => {
                    vars.push(self.variable()?);
                },
                _ => break
            }
        }

        self.consume(TokenData::DOT)?;
        let left = self.expr()?;
        self.consume(TokenData::EQUAL)?;
        let right = self.expr()?;

        Ok(Property { vars, left, right })
    }
}

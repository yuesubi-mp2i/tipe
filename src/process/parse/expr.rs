use super::*;


impl<I: Iterator<Item = Token>> Parser<I> {
    pub fn expr(&mut self) -> ParseResult<Expr<Raw>> {
        let e = match self.peek()?.data {
            TokenData::LET => self.let_in()?,
            TokenData::FUN => self.function()?,
            TokenData::MATCH => self.match_with()?,
            TokenData::LPAREN => self.scope()?,
            TokenData::PascalIdent(_) => self.literal_constructor()?,
            TokenData::SnakeIdent(_) => self.literal_variable()?,
            _ => return Err(UnexpectedToken {
                expected: String::from("expression"),
                got: self.next()?
            })
        };

        let mut args = Vec::new();

        loop {
            let arg = match &self.peek()?.data {
                TokenData::LPAREN => self.scope()?,
                TokenData::SnakeIdent(id) => {
                    let lit = ExprData::LitVar { id: id.clone() };
                    self.next()?;
                    lit.into()
                },
                TokenData::PascalIdent(id) => {
                    let lit = ExprData::LitConstructor {
                        id: id.clone(),
                        args: Vec::new()
                    };
                    self.next()?;
                    lit.into()
                },
                _ => break
            };

            args.push(arg);
        }

        Ok(args
            .into_iter()
            .fold(
                e,
                |caller, argument|
                ExprData::Call {
                    caller: Box::new(caller),
                    arg: Box::new(argument)
                }.into()
            )
        )
    }

    pub fn let_in(&mut self) -> ParseResult<Expr<Raw>> {
        self.consume(TokenData::LET)?;
        let variable = self.variable()?;
        self.consume(TokenData::EQUAL)?;
        let assignment = Box::new(self.expr()?);
        self.consume(TokenData::IN)?;
        let body = Box::new(self.expr()?);

        Ok(ExprData::Binding { var: variable, val: assignment, body }.into())
    }

    pub fn function(&mut self) -> ParseResult<Expr<Raw>> {
        self.consume(TokenData::FUN)?;

        let input = match &self.peek()?.data {
            TokenData::LPAREN => {
                self.consume(TokenData::LPAREN)?;
                let input = self.variable()?;
                self.consume(TokenData::RPAREN)?;
                input
            },
            _ => self.untyped_variable()?
        };

        let out_type = match &self.peek()?.data {
            TokenData::COLUMN => {
                self.consume(TokenData::COLUMN)?;
                Some(self.short_type()?)
            },
            _ => None
        };

        self.consume(TokenData::ARROW)?;
        let body = Box::new(self.expr()?);

        Ok(ExprData::Function { input, out_type, body }.into())
    }

    pub fn match_with(&mut self) -> ParseResult<Expr<Raw>> {
        self.consume(TokenData::MATCH)?;
        let expr = Box::new(self.expr()?);
        self.consume(TokenData::WITH)?;

        let mut cases = Vec::new();
        self.consume_if_exists(TokenData::PIPE);
        cases.push(self.match_branch()?);

        loop {
            if self.peek()?.data == TokenData::PIPE {
                self.consume(TokenData::PIPE)?;
                cases.push(self.match_branch()?);
            } else {
                break;
            }
        }

        Ok(ExprData::Match { expr, cases }.into())
    }

    pub fn match_branch(&mut self) -> ParseResult<MatchBranch<Raw>> {
        // TODO: allow multiple patterns
        let pattern = self.pattern()?;
        self.consume(TokenData::ARROW)?;
        let body = self.expr()?;
        Ok(MatchBranch { pattern, body })
    }

    pub fn scope(&mut self) -> ParseResult<Expr<Raw>> {
        self.consume(TokenData::LPAREN)?;
        let expr = self.expr()?;
        self.consume(TokenData::RPAREN)?;
        Ok(expr)
    }

    pub fn literal_constructor(&mut self) -> ParseResult<Expr<Raw>> {
        let id = match &self.peek()?.data {
            TokenData::PascalIdent(id) => id.clone(),
            _ => return Err(UnexpectedToken {
                expected: String::from("constructor identifier"),
                got: self.next()?
            })
        };
        self.next()?;

        if self.peek()?.data != TokenData::LPAREN {
            return Ok(ExprData::LitConstructor { id, args: Vec::new() }.into());
        }
        self.consume(TokenData::LPAREN)?;

        let mut argument = Vec::new();
        argument.push(self.expr()?);

        loop {
            match &self.peek()?.data {
                TokenData::RPAREN => break,
                _ => {
                    self.consume(TokenData::COMMA)?;
                    argument.push(self.expr()?);
                }
            }
        }

        self.consume(TokenData::RPAREN)?;
        Ok(ExprData::LitConstructor { id, args: argument }.into())
    }

    pub fn literal_variable(&mut self) -> ParseResult<Expr<Raw>> {
        let lit = match &self.peek()?.data {
            TokenData::SnakeIdent(id) =>
                ExprData::LitVar { id: id.clone() },
            _ => return Err(UnexpectedToken {
                expected: String::from("variable identifier"),
                got: self.next()?
            })
        };

        self.next()?;
        Ok(lit.into())
    }
}
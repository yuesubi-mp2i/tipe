use std::iter::{Iterator, Peekable};

use crate::lang::*;
use super::lex::token::{Token, TokenData};

mod expr;
mod pat;
mod prop;
mod stmt;
mod typ;


use TokenData::*;


#[derive(Debug)]
pub enum ParsingErr {
    UnexpectedToken {
        expected: String,
        got: Token
    },
}

use ParsingErr::*;
type ParseResult<T> = Result<T, ParsingErr>;


pub struct Parser<I: Iterator<Item = Token>>
{
    input: Peekable<I>,
}


impl<I: Iterator<Item = Token>> Parser<I> {
    pub fn new(iter: I) -> Self {
        Parser {
            input: iter.peekable(),
        }
    }

    fn peek(&mut self) -> ParseResult<&Token> {
        self.input.peek()
            .ok_or_else(|| unreachable!("At the very least EOF is expected"))
    }

    fn next(&mut self) -> ParseResult<Token> {
        self.input.next()
            .ok_or_else(|| unreachable!("At the very least EOF is expected"))
    }

    pub fn ast(&mut self) -> ParseResult<AST<Raw>> {
        let type_defs = self.type_defs()?;
        let statements = self.statements()?;
        Ok(AST { type_defs, statements })
    }

    fn consume(&mut self, token_data: TokenData) -> ParseResult<()> {
        let token = self.next()?;

        if token.data == token_data {
            Ok(())
        } else {
            Err(UnexpectedToken {
                // TODO: cleaner printing
                expected: String::from(format!("{:?}", token_data)),
                got: token
            })
        }
    }

    fn consume_if_exists(&mut self, token_data: TokenData) {
        let exists = self.peek()
            .map_or(
                false,
                |token| token.data == token_data
            );
        
        if exists {
            self.next().unwrap();
        }
    }
}


#[cfg(test)]
mod tests {
    use super::{Parser, Token};
    use crate::process::read;

    pub fn str_to_parser<'a>(text: &'a str) -> Parser<impl Iterator<Item = Token> + 'a> {
        Parser::new(read::lex_and_unwrap(text.chars()))
    }
}
use std::collections::HashMap;
use std::hash::Hash;


#[derive(Debug, Clone)]
pub struct ScopeStack<K, V> {
    stack: Vec<HashMap<K, V>>
}


impl<K: Eq + Hash, V: Clone> ScopeStack<K, V> {
    pub fn new() -> Self {
        ScopeStack {
            stack: Vec::new(),
        }
    }

    pub fn push_empty_scope(&mut self) {
        self.stack.push(HashMap::new());
    }

    pub fn push_scope(&mut self, frame: HashMap<K, V>) {
        self.stack.push(frame);
    }

    pub fn pop_scope(&mut self) -> Option<HashMap<K, V>> {
        self.stack.pop()
    }

    pub fn insert(&mut self, key: K, val: V) {
        self.stack.last_mut()
            .expect("stack should not be empty")
            .insert(key, val);
    }

    pub fn insert_bellow(&mut self, depth: usize, key: K, val: V) {
        self.stack.iter_mut()
            .rev()
            .nth(depth)
            .expect("stack should be high enough")
            .insert(key, val);
    }

    // TODO: find a solution to this horrible complexity, what even is the point
    // of the hashmap ?
    pub fn get(&self, key: &K) -> Option<&V> {
        for scope in self.stack.iter().rev() {
            if let Some(val) = scope.get(key) {
                return Some(val);
            }
        }

        None
    }
}
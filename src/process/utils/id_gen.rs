#[derive(Debug, Clone)]
pub struct IdGenerator {
    counter: usize
}

impl From<usize> for IdGenerator {
    fn from(counter_start: usize) -> Self {
        IdGenerator { counter: counter_start  }
    }
}

impl IdGenerator {
    pub fn new() -> IdGenerator {
        IdGenerator {
            counter: 0
        }
    }

    pub fn gen(&mut self) -> usize {
        let c = self.counter;
        self.counter += 1;
        c
    }

    pub fn reserve(&mut self, val: usize) -> Result<(), ()> {
        if val >= self.counter {
            self.counter = val + 1;
            Ok(())
        } else {
            Err(())
        }
    }
}
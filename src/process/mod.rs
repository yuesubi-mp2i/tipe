mod link;
pub mod read;
mod lex;
mod parse;
mod typ;
mod utils;

use utils::IdGenerator;
pub use link::{ Linker, LinkingErr };
pub use typ::Typer;


use crate::lang::*;


impl<K: Kind> AST<K> {
    pub fn link(self) -> AST<Linked> {
        Linker::new()
            .ast(self)
            .unwrap()
    }
}

impl AST<Raw> {
    // TODO: see if var id generator doesn't overlap with old values
    // not returned by free_vars
    pub fn link_with(self, free_vars: impl IntoIterator<Item = LId>) -> AST<Linked> {
        Linker::from(free_vars)
            .ast(self)
            .unwrap()
    }
}

impl<K: Kind> Expr<K> {
    pub fn link(self) -> Expr<Linked> {
        Linker::new()
            .expr(self)
            .unwrap()
    }
}

impl Expr<Raw> {
    pub fn link_with(self, free_vars: impl IntoIterator<Item = LId>) -> Expr<Linked> {
        Linker::from(free_vars)
            .expr(self)
            .unwrap()
    }
}

impl<K: Kind> Pattern<K> {
    pub fn link(self) -> Pattern<Linked> {
        Linker::new()
            .pattern(self)
            .unwrap()
    }
}

impl Pattern<Raw> {
    pub fn link_with(self, free_vars: impl IntoIterator<Item = LId>) -> Pattern<Linked> {
        Linker::from(free_vars)
            .pattern(self)
            .unwrap()
    }
}

impl<K: Kind> Property<K> {
    pub fn link(self) -> Property<Linked> {
        Linker::new()
            .property(self)
            .unwrap()
    }
}

impl Property<Raw> {
    pub fn link_with(self, free_vars: impl IntoIterator<Item = LId>) -> Property<Linked> {
        Linker::from(free_vars)
            .property(self)
            .unwrap()
    }
}
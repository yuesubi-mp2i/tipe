mod ast;
mod convert;
mod expr;
mod pat;
mod prop;
mod stmt;
mod typ;
mod unify;

use std::collections::HashMap;

use crate::lang::*;
use super::IdGenerator;
use unify::Unifier;


pub struct Typer {
    constructor_types: HashMap<LId, ConstructorTypes>,

    generic_counter: IdGenerator,
    unifier: Unifier,
}


#[derive(Clone)]
struct ConstructorTypes {
    typ: Type<LT>,
    arg_types: Vec<Type<LT>>
}

impl ConstructorTypes {
    pub fn beta_rename(self, counter: &mut IdGenerator) -> ConstructorTypes {
        let ids = self.typ.generics();
        let dictionary = ids.into_iter()
            .map(|id| (id, LId::unnamed(counter.gen())))
            .collect();

        let typ = Self::rename_generics_of_type(self.typ, &dictionary);
        let arg_types = self.arg_types.into_iter()
            .map(|arg_type| Self::rename_generics_of_type(arg_type, &dictionary))
            .collect();

        ConstructorTypes { typ, arg_types }
    }

    fn rename_generics_of_type(typ: Type<LT>, dictionary: &HashMap<LId, LId>) -> Type<LT> {
        use Type::*;

        match typ {
            Generic { id } =>
                Generic {
                    id: dictionary.get(&id)
                        .unwrap()
                        .clone()
                },

            Specialization { args, typ } =>
                Specialization {
                    args: args.into_iter()
                        .map(|arg|
                            Self::rename_generics_of_type(arg, dictionary)
                        )
                        .collect(),
                    typ
                },

            Function { input, output } =>
                Function {
                    input: Box::new(Self::rename_generics_of_type(*input, dictionary)),
                    output: Box::new(Self::rename_generics_of_type(*output, dictionary))
                }
        }
    }
}


#[derive(Debug)]
pub enum TypingErr {
    CantUnify {
        type_a: Type<LT>,
        type_b: Type<LT>
    }
}

type TypingResult<T> = Result<T, TypingErr>;


impl Typer {
    pub fn new() -> Typer {
        Typer {
            constructor_types: HashMap::new(),
            generic_counter: IdGenerator::new(),
            unifier: Unifier::new()
        }
    }

    fn instantiate_constructor_types(&mut self, id: &LId) -> ConstructorTypes {
        self.constructor_types.get(id)
            .expect("constructor should be registered")
            .clone()
            .beta_rename(&mut self.generic_counter)
    }

    fn new_type(&mut self) -> Type<LT> {
        Type::Generic {
            id: LId::unnamed(self.generic_counter.gen())
        }
    }

    fn instantiate(&mut self, annotation: <Linked as Kind>::TypeAnnotation)
        -> Type<LT>
    {
        match annotation {
            None => self.new_type(),
            Some(typ) => typ.into()
        }
    }

    fn unify(&mut self, type_a: &Type<LT>, type_b: &Type<LT>) -> TypingResult<()> {
        self.unifier.unify(type_a, type_b)
    }

    fn find_current_best_type(&mut self, typ: Type<LT>) -> Type<LT> {
        self.unifier.find(typ)
    }
}
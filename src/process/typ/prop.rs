use super::*;


impl Typer {
    pub fn type_property(&mut self, property: Property<Linked>)
        -> TypingResult<Property<LT>>
    {
        let property = self.register_property(property)?;
        Ok(self.unify_property(property))
    }

    pub fn register_property(&mut self, property: Property<Linked>)
        -> TypingResult<Property<LT>>
    {
        let vars = property.vars.into_iter()
            .map(|var| self.register_variable(var))
            .collect();
        let left = self.register_expr(property.left)?;
        let right = self.register_expr(property.right)?;
        self.unify(&left.meta.typ, &right.meta.typ)?;
        Ok(Property { vars, left, right })
    }

    pub fn unify_property(&mut self, property: Property<LT>) -> Property<LT> {
        let vars = property.vars.into_iter()
            .map(|var| self.unify_variable(var))
            .collect();
        let left = self.unify_expr(property.left);
        let right = self.unify_expr(property.right);
        Property { vars, left, right }
    }
}
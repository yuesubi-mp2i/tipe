use crate::lang::*;


impl From<TypeDef<Linked>> for TypeDef<LT> {
    fn from(linked: TypeDef<Linked>) -> Self {
        TypeDef {
            id: linked.id,
            arg_ids: linked.arg_ids,
            typ: linked.typ.into()
        }
    }
}

impl From<TypeDefType<Linked>> for TypeDefType<LT> {
    fn from(linked: TypeDefType<Linked>) -> Self {
        use TypeDefType::*;
        match linked {
            Type(typ) => Type(typ.into()),
            TypeSum(branches)  => TypeSum(
                branches.into_iter()
                    .map(|branch| branch.into())
                    .collect()
            )
        }
    }
}

impl From<TypeSumBranch<Linked>> for TypeSumBranch<LT> {
    fn from(linked: TypeSumBranch<Linked>) -> Self {
        TypeSumBranch {
            constructor_id: linked.constructor_id,
            args: linked.args.into_iter()
                .map(|arg| arg.into())
                .collect()
        }
    }
}

impl From<Type<Linked>> for Type<LT> {
    fn from(linked: Type<Linked>) -> Self {
        use Type::*;
        match linked {
            Generic { id } => Generic { id },
            Specialization { args, typ } =>
                Specialization {
                    args: args.into_iter()
                        .map(|arg| arg.into())
                        .collect(),
                    typ
                },
            Function { input, output } =>
                Function {
                    input: Box::new((*input).into()),
                    output: Box::new((*output).into())
                }
        }
    }
}
use super::*;


impl Typer {
    pub fn type_statement(&mut self, statement: Statement<Linked>)
        -> TypingResult<Statement<LT>>
    {
        let statement = self.register_statement(statement)?;
        Ok(self.unify_statement(statement))
    }

    pub fn register_statement(&mut self, statement: Statement<Linked>)
        -> TypingResult<Statement<LT>>
    {
        let var = self.register_variable(statement.var);
        let val = self.register_expr(statement.val)?;
        self.unify(&var.meta.typ, &val.meta.typ)?;
        Ok(Statement { var, val })
    }

    pub fn unify_statement(&mut self, statement: Statement<LT>) -> Statement<LT> {
        let var = statement.var;
        let val = self.unify_expr(statement.val);
        Statement { var, val }
    }
}
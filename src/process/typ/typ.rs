use super::*;


impl Typer {
    pub fn register_type_def(&mut self, type_def: TypeDef<Linked>) {
        let typ = Type::Specialization {
            args: type_def.arg_ids.into_iter()
                .map(|id| Type::Generic { id })
                .collect(),
            typ: type_def.id
        };

        use TypeDefType as TDF;
        match type_def.typ {
            TDF::Type(alias) => todo!(),
            TDF::TypeSum(branches) =>
                for TypeSumBranch { constructor_id, args } in branches {
                    self.constructor_types.insert(
                        constructor_id,
                        ConstructorTypes {
                            typ: typ.clone(),
                            arg_types: args.into_iter()
                                .map(|arg| arg.into())
                                .collect()
                        }
                    );
                }
        }
    }
}
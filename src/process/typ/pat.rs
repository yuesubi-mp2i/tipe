use super::*;


impl Typer {
    pub fn register_pattern(&mut self, pattern: Pattern<Linked>)
        -> TypingResult<Pattern<LT>>
    {
        use PatternData as PD;

        Ok(match pattern.data {
            PD::Var(var) => {
                let var = self.register_variable(var);
                Pattern {
                    data: PD::Var(var.clone()),
                    meta: var.meta.typ.into()
                }
            },

            PD::Constructor { id, args } => {
                let ConstructorTypes { typ, arg_types } =
                    self.instantiate_constructor_types(&id);

                let args = args.into_iter()
                    .map(|arg| self.register_pattern(arg))
                    .collect::<TypingResult<Vec<_>>>()?;

                args.iter()
                    .zip(arg_types.iter())
                    .map(|(arg, typ)|
                        self.unify(&arg.meta.typ, &typ)
                    )
                    .collect::<TypingResult<Vec<_>>>()?;

                Pattern {
                    data: PD::Constructor { id, args },
                    meta: typ.into()
                }
            }
        })
    }

    pub fn unify_pattern(&mut self, pattern: Pattern<LT>) -> Pattern<LT>
    {
        use PatternData as PD;

        let data = match pattern.data {
            PD::Var(var) =>
                PD::Var(var),

            PD::Constructor { id, args } => {
                let args = args.into_iter()
                    .map(|arg| self.unify_pattern(arg))
                    .collect();
                PD::Constructor { id, args }
            }
        };
        
        Pattern {
            data,
            meta: self.find_current_best_type(pattern.meta.typ).into()
        }
    }

    pub fn register_variable(&mut self, var: Var<Linked>) -> Var<LT> {
        let typ = self.instantiate(var.typ);
        Var { id: var.id, typ: (), meta: typ.into() }
    }

    pub fn unify_variable(&mut self, var: Var<LT>) -> Var<LT> {
        let typ = self.find_current_best_type(var.meta.typ);
        Var { id: var.id, typ: var.typ, meta: typ.into() }
    }
}
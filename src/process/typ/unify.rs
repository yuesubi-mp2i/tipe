use std::collections::HashMap;

use crate::lang::*;
use super::{TypingErr, TypingResult};


pub struct Unifier {
    nodes: HashMap<Type<LT>, Node>
}


#[derive(Debug)]
enum Node {
    Child {
        parent: Type<LT>
    },
    Root {
        typ: Type<LT>,
    }
}


impl Unifier {
    pub fn new() -> Unifier {
        Unifier {
            nodes: HashMap::new()
        }
    }

    fn add_if_unregistered(&mut self, typ: &Type<LT>) {
        if !self.nodes.contains_key(typ) {
            self.nodes.insert(
                typ.clone(),
                Node::Root {
                    typ: typ.clone(),
                }
            );
        }
    }

    pub fn unify(&mut self, type_a: &Type<LT>, type_b: &Type<LT>)
        -> TypingResult<()>
    {
        let type_a = self.find(type_a.clone());
        let type_b = self.find(type_b.clone());

        if type_a == type_b {
            return Ok(());
        }

        use Type::*;
        match (&type_a, &type_b) {
            (Specialization { args: args_a, typ: type_a },
                Specialization { args: args_b, typ: type_b })
            if type_a == type_b && args_a.len() == args_b.len() =>
            {
                args_a.into_iter()
                    .zip(args_b.into_iter())
                    .map(|(arg_a, arg_b)|
                        self.unify(arg_a, arg_b)
                    )
                    .collect::<TypingResult<Vec<_>>>()?;
                Ok(())
            }

            // TODO: handle cases where aliases are made with type defs

            (Function { input: input_a, output: output_a },
                Function { input: input_b, output: output_b }) =>
            {
                self.unify(input_a, input_b)?;
                self.unify(output_a, output_b)
            },

            (Generic { id }, other)
            | (other, Generic { id }) =>
            {
                self.nodes.insert(
                    Generic { id: id.clone() },
                    Node::Child { parent: other.clone() }
                );
                Ok(())
            }

            _ => Err(TypingErr::CantUnify {
                type_a: type_a.clone(),
                type_b: type_b.clone()
            })
        }
    }

    pub fn find(&mut self, typ: Type<LT>) -> Type<LT> {
        let mut typ  = typ;

        loop {
            use Node::*;

            match self.nodes.get(&typ) {
                Some(Child { parent }) =>
                    typ = parent.clone(),
                Some(Root { typ: root }) => {
                    typ = root.clone();
                    break;
                }, 
                _ => {
                    self.nodes.insert(
                        typ.clone(),
                        Node::Root { typ: typ.clone() }
                    );
                    break;
                }
            }
        }

        use Type::*;
        match typ {
            Generic { id } =>
                Generic { id },

            Function { input, output } =>
                Function {
                    input: Box::new(self.find(*input)),
                    output: Box::new(self.find(*output))
                },
            
            Specialization { args, typ } =>
                Specialization {
                    args: args.into_iter()
                        .map(|arg| self.find(arg))
                        .collect(),
                    typ
                }
        }
    }
}
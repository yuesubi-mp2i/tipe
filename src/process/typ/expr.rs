use super::*;


impl Typer {
    pub fn register_expr(&mut self, expr: Expr<Linked>) -> TypingResult<Expr<LT>> {
        use ExprData::*;

        Ok(match expr.data {
            Binding { var, val, body } => {
                let var = self.register_variable(var);
                let val = self.register_expr(*val)?;
                self.unify(&val.meta.typ, &var.meta.typ)?;

                let body = self.register_expr(*body)?;
                let body_type = body.meta.typ.clone();

                Expr {
                    data: Binding {
                        var,
                        val: Box::new(val),
                        body: Box::new(body)
                    },
                    meta: body_type.into()
                }
            }

            Function { input, out_type, body } => {
                let input = self.register_variable(input);
                let output_type = self.instantiate(out_type);

                let body = self.register_expr(*body)?;
                self.unify(&output_type, &body.meta.typ)?;

                Expr {
                    data: Function {
                        input: input.clone(),
                        out_type: (),
                        body: Box::new(body)
                    },
                    meta: Type::Function {
                        input: Box::new(input.meta.typ),
                        output: Box::new(output_type)
                    }.into()
                }
            },

            Match { expr, cases } => {
                let expr = self.register_expr(*expr)?;

                let cases = cases.into_iter()
                    .map(|branch| self.register_match_branch(branch))
                    .collect::<TypingResult<Vec<_>>>()?;

                let typ = cases.get(0)
                    .expect("A match should have at least one branch")
                    .body.meta.typ
                    .clone();

                cases.iter()
                    .map(|MatchBranch { pattern, body }| {
                        self.unify(&expr.meta.typ, &pattern.meta.typ)?;
                        self.unify(&typ, &body.meta.typ)
                    })
                    .collect::<TypingResult<Vec<_>>>()?;

                Expr {
                    data: Match {
                        expr: Box::new(expr),
                        cases
                    },
                    meta: typ.into()
                }
            },

            Call { caller, arg } => {
                let caller = self.register_expr(*caller)?;
                let arg = self.register_expr(*arg)?;

                let result_typ = self.new_type();
                self.unify(
                    &caller.meta.typ,
                    &Type::Function {
                        input: Box::new(arg.meta.typ.clone()),
                        output: Box::new(result_typ.clone())
                    }
                )?;

                Expr {
                    data: Call {
                        caller: Box::new(caller),
                        arg: Box::new(arg)
                    },
                    meta: result_typ.into()
                }
            },

            LitVar { id } => {
                Expr {
                    data: LitVar { id },
                    meta: self.new_type().into()
                }
            },

            LitConstructor { id, args } => {
                let ConstructorTypes { typ, arg_types } =
                    self.instantiate_constructor_types(&id);

                let args = args.into_iter()
                    .map(|arg| self.register_expr(arg))
                    .collect::<TypingResult<Vec<_>>>()?;

                args.iter()
                    .zip(arg_types.iter())
                    .map(|(arg, typ)|
                        self.unify(&arg.meta.typ, &typ)
                    )
                    .collect::<TypingResult<Vec<_>>>()?;

                Expr {
                    data: LitConstructor { id, args },
                    meta: typ.into()
                }
            }
        })
    }

    pub fn unify_expr(&mut self, expr: Expr<LT>) -> Expr<LT> {
        use ExprData::*;

        let data = match expr.data {
            Binding { var, val, body } => {
                let val = self.unify_expr(*val);
                let body = self.unify_expr(*body);
                Binding { var, val: Box::new(val), body: Box::new(body) }
            }

            Function { input, body, .. } => {
                let body = self.unify_expr(*body);
                Function { input, out_type: (), body: Box::new(body) }
            },

            Match { expr, cases } => {
                let expr = self.unify_expr(*expr);

                let cases = cases.into_iter()
                    .map(|branch| self.unify_match_branch(branch))
                    .collect();

                Match { expr: Box::new(expr), cases }
            },

            Call { caller, arg } => {
                let caller = self.unify_expr(*caller);
                let arg = self.unify_expr(*arg);
                Call {
                    caller: Box::new(caller),
                    arg: Box::new(arg)
                }
            },

            LitVar { id } =>
                LitVar { id },

            LitConstructor { id, args } => {
                let args = args.into_iter()
                    .map(|arg| self.unify_expr(arg))
                    .collect();
                LitConstructor { id, args }
            }
        };

        Expr {
            data,
            meta: self.find_current_best_type(expr.meta.typ).into()
        }
    }

    fn register_match_branch(&mut self, branch: MatchBranch<Linked>)
        -> TypingResult<MatchBranch<LT>>
    {
        Ok(MatchBranch {
            pattern: self.register_pattern(branch.pattern)?,
            body: self.register_expr(branch.body)?
        })
    }

    fn unify_match_branch(&mut self, branch: MatchBranch<LT>)
        -> MatchBranch<LT>
    {
        MatchBranch {
            pattern: self.unify_pattern(branch.pattern),
            body: self.unify_expr(branch.body)
        }
    }
}
use super::*;


impl Typer {
    pub fn type_ast(&mut self, ast: AST<Linked>) -> TypingResult<AST<LT>> {
        let ast = self.register_ast(ast)?;
        Ok(self.unify_ast(ast))
    }

    fn register_ast(&mut self, ast: AST<Linked>) -> TypingResult<AST<LT>> { 
        let type_defs = ast.type_defs.into_iter()
            .map(|type_def| {
                self.register_type_def(type_def.clone());
                type_def.into()
            })
            .collect();
        let statements = ast.statements.into_iter()
            .map(|statement| self.register_statement(statement))
            .collect::<TypingResult<_>>()?;

        Ok(AST { type_defs, statements })
    }

    fn unify_ast(&mut self, ast: AST<LT>) -> AST<LT> {
        let statements = ast.statements.into_iter()
            .map(|statement| self.unify_statement(statement))
            .collect();
        AST { type_defs: ast.type_defs, statements }
    }
}
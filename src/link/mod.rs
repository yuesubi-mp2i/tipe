mod id_gen;
mod link;
mod scope_stack;
mod vars;

pub use id_gen::IdGenerator;
pub use link::Linker;


use crate::lang::ast::*;


impl<K: Kind> AST<K> {
    pub fn link(self) -> AST<LU> {
        Linker::new()
            .ast(self)
            .unwrap()
    }
}

impl AST<Raw> {
    // TODO: see if var id generator doesn't overlap with old values
    // not returned by free_vars
    pub fn link_with(self, free_vars: impl IntoIterator<Item = LUId>) -> AST<LU> {
        Linker::from(free_vars)
            .ast(self)
            .unwrap()
    }
}

impl<K: Kind> Expr<K> {
    pub fn link(self) -> Expr<LU> {
        Linker::new()
            .expr(self)
            .unwrap()
    }
}

impl Expr<Raw> {
    pub fn link_with(self, free_vars: impl IntoIterator<Item = LUId>) -> Expr<LU> {
        Linker::from(free_vars)
            .expr(self)
            .unwrap()
    }
}

impl<K: Kind> Pattern<K> {
    pub fn link(self) -> Pattern<LU> {
        Linker::new()
            .pattern(self)
            .unwrap()
    }
}

impl Pattern<Raw> {
    pub fn link_with(self, free_vars: impl IntoIterator<Item = LUId>) -> Pattern<LU> {
        Linker::from(free_vars)
            .pattern(self)
            .unwrap()
    }
}

impl<K: Kind> Property<K> {
    pub fn link(self) -> Property<LU> {
        Linker::new()
            .property(self)
            .unwrap()
    }
}

impl Property<Raw> {
    pub fn link_with(self, free_vars: impl IntoIterator<Item = LUId>) -> Property<LU> {
        Linker::from(free_vars)
            .property(self)
            .unwrap()
    }
}
use std::collections::HashSet;

use crate::lang::ast::*;


impl<K: Kind> AST<K> {
    pub fn free_vars(&self) -> HashSet<K::Id> {
        self.type_defs
            .iter()
            .flat_map(|type_def| type_def.free_vars().into_iter())
            .chain(
                self.statements
                    .iter()
                    .flat_map(|stmt| stmt.free_vars().into_iter())
            )
            .collect()
    }
}


impl<K: Kind> Expr<K> {
    pub fn free_vars(&self) -> HashSet<K::Id> {
        use Expr::*;

        match self {
            Binding { var, val, body } =>
                body.free_vars()
                    .difference(&var.free_vars())
                    .cloned()
                    .collect::<HashSet<_>>()
                    .union(&val.free_vars())
                    .cloned()
                    .collect(),

            Function { input, body, .. } =>
                body.free_vars()
                    .difference(&input.free_vars())
                    .cloned()
                    .collect(),

            Call { caller, arg } =>
                (*caller)
                    .free_vars()
                    .union(&(*arg).free_vars())
                    .cloned()
                    .collect(),

            Match { expr, cases } =>
                (*expr)
                    .free_vars()
                    .into_iter()
                    .chain(
                        cases.into_iter()
                            .flat_map(|case|
                                case.free_vars().into_iter()
                            )
                    )
                    .collect(),

            LitVar { id } =>
                HashSet::from([id.clone()]),

            LitConstructor { id, args } =>
                args.into_iter()
                    .flat_map(|arg| arg.free_vars().into_iter())
                    .chain([id.clone()].into_iter())
                    .collect()
        }
    }
}


impl<K: Kind> MatchBranch<K> {
    pub fn free_vars(&self) -> HashSet<K::Id> {
        self.body.free_vars()
            .difference(&self.pattern.free_vars())
            .cloned()
            .collect()
    }
}


impl<K: Kind> Pattern<K> {
    pub fn free_vars(&self) -> HashSet<K::Id> {
        match self {
            Pattern::Var(var) =>
                var.free_vars(),
            Pattern::Constructor { id, args } =>
                args.into_iter()
                    .flat_map(|arg|
                        arg.free_vars().into_iter()
                    )
                    .chain([id.clone()].into_iter())
                    .collect()
        }
    }
}


impl<K: Kind> Var<K> {
    pub fn free_vars(&self) -> HashSet<K::Id> {
        HashSet::from([self.id.clone()])
    }
}


impl<K: Kind> Statement<K> {
    pub fn free_vars(&self) -> HashSet<K::Id> {
        self.val.free_vars()
            .into_iter()
            .chain(self.var.free_vars().into_iter())
            .collect()
    }
}


impl<K: Kind> TypeDef<K> {
    pub fn free_vars(&self) -> HashSet<K::Id> {
        HashSet::from([self.id.clone()])
            .into_iter()
            .chain(self.typ.free_vars().into_iter())
            .collect()
    }
}


impl<K: Kind> TypeDefType<K> {
    pub fn free_vars(&self) -> HashSet<K::Id> {
        use TypeDefType::*;

        match self {
            Type(_) => HashSet::new(),
            TypeSum(branches) =>
                branches.into_iter()
                    .flat_map(|branch|
                        branch.free_vars().into_iter()
                    )
                    .collect()
        }
    }
}


impl<K: Kind> TypeSumBranch<K> {
    pub fn free_vars(&self) -> HashSet<K::Id> {
        HashSet::from([self.constructor_id.clone()])
    }
}
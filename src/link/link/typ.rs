use crate::lang::{
    TraceNode,
    ast::*
};

use super::{Linker, LinkResult};


use TraceNode::*;


impl<K: Kind> Linker<K> {
    pub fn type_defs(&mut self, raw: Vec<TypeDef<K>>)
        -> LinkResult<Vec<TypeDef<LU>>>
    {
        raw.into_iter()
            .map(|td| self.type_def(td))
            .collect()
    }

    fn type_def(&mut self, raw: TypeDef<K>) -> LinkResult<TypeDef<LU>> {
        self.trace.enter(TypeDefId);
        let id = self.create(raw.id)?;
        self.trace.exit();

        self.push_empty_scope();

        self.trace.enter(TypeDefArgIds);
        let arg_ids = raw.arg_ids
                .into_iter()
                .map(|raw_id| self.create(raw_id))
                .collect::<LinkResult<Vec<_>>>()?;
        self.trace.exit();

        self.trace.enter(TypeDefType);
        use crate::lang::ast::typ::TypeDefType::*;
        let typ = match raw.typ {
            Type(raw_typ) => Type(self.typ(raw_typ)?),
            TypeSum(raw_branches) =>
                TypeSum(self.type_sum_branches(raw_branches)?)
        };
        self.trace.exit();

        self.pop_scope();

        Ok(TypeDef { id, arg_ids, typ })
    }

    fn type_sum_branches(&mut self, raw: Vec<TypeSumBranch<K>>)
        -> LinkResult<Vec<TypeSumBranch<LU>>>
    {
        raw.into_iter()
            .map(|raw_branch| self.type_sum_branch(raw_branch))
            .collect()
    }

    fn type_sum_branch(&mut self, raw: TypeSumBranch<K>)
        -> LinkResult<TypeSumBranch<LU>>
    {
        self.trace.enter(TypeSumBranchConstructorId);
        let constructor_id = self.create_just_bellow(raw.constructor_id)?;
        self.trace.exit();

        self.trace.enter(TypeSumBranchArgs);
        let args = raw.args
            .into_iter()
            .map(|t| self.typ(t))
            .collect::<LinkResult<Vec<_>>>()?;
        self.trace.exit();

        Ok(TypeSumBranch { constructor_id, args })
    }

    fn typ(&mut self, raw: Type<K>) -> LinkResult<Type<LU>> {
        use Type::*;

        Ok(match raw {
            Generic { id } => {
                self.trace.enter(GenericId);
                let id = self.get_or_create(id);
                self.trace.exit();
                Generic { id }
            },

            Specialization { args, typ } => {
                self.trace.enter(SpecializationArgs);
                let args = args
                    .into_iter()
                    .map(|t| self.typ(t))
                    .collect::<LinkResult<Vec<Type<LU>>>>()?;
                self.trace.exit();

                self.trace.enter(SpecializationType);
                let typ = self.get_or_create(typ);
                self.trace.exit();

                Specialization { args, typ }
            }

            Function { input, output } => {
                self.trace.enter(TypeFunctionInput);
                let input = Box::new(self.typ(*input)?);
                self.trace.exit();

                self.trace.enter(TypeFunctionOutput);
                let output = Box::new(self.typ(*output)?);
                self.trace.exit();

                Function { input, output }
            }
        })
    }
}


#[cfg(test)]
mod tests {
    use crate::lang::read;
    use super::*;


    #[test]
    fn test_type_def() {
        use read::type_def;

        let mut linker = Linker::new();
        
        assert!(linker.type_def(type_def(
            "type bool = False | True;;".chars()
        )).is_ok());

        assert!(linker.type_def(type_def(
            "type bool = False | True;;".chars()
        )).is_err());

        assert!(linker.type_def(type_def(
            "type alias = bool;;".chars()
        )).is_ok());

        assert!(linker.type_def(type_def(
            "type 'a option = None | Some of 'a;;".chars()
        )).is_ok());
        assert!(linker.type_def(type_def(
            "type 'a 'a wrong_option = 'a option;;".chars()
        )).is_err());
    }
}
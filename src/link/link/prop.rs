use crate::lang::{
    TraceNode,
    ast::*
};

use super::{Linker, LinkResult};

use TraceNode::*;


impl<K: Kind> Linker<K> {
    pub fn property(&mut self, raw: Property<K>) -> LinkResult<Property<LU>> {
        self.trace.enter(PropertyVars);
        let vars = raw.vars
            .into_iter()
            .map(|var| self.variable(var))
            .collect::<LinkResult<_>>()?;
        self.trace.exit();

        self.trace.enter(PropertyLeft);
        let left = self.expr(raw.left)?;
        self.trace.exit();
        self.trace.enter(PropertyRight);
        let right = self.expr(raw.right)?;
        self.trace.exit();

        Ok(Property { vars, left, right })
    }
}
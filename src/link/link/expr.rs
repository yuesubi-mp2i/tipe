use crate::lang::{
    TraceNode,
    ast::*
};

use super::{Linker, LinkResult};


use TraceNode::*;


impl<K: Kind> Linker<K> {
    pub fn expr(&mut self, raw: Expr<K>) -> LinkResult<Expr<LU>> {
        use Expr::*;

        Ok(match raw {
            Binding { var, val, body } => {
                self.push_empty_scope();
                self.trace.enter(BindingVar);
                let var = self.variable(var)?;
                self.trace.exit();

                self.trace.enter(BindingVal);
                let val = Box::new(self.expr(*val)?);
                self.trace.exit();

                self.trace.enter(BindingBody);
                let body = Box::new(self.expr(*body)?);
                self.trace.exit();
                self.pop_scope();

                Binding { var, val, body }
            }

            Function { input, body, .. } => {
                self.push_empty_scope();
                self.trace.enter(FunctionInput);
                let input = self.variable(input)?;
                self.trace.exit();

                self.trace.enter(FunctionBody);
                let body = Box::new(self.expr(*body)?);
                self.trace.exit();
                self.pop_scope();

                Function { input, out_type: (), body }
            },

            Match { expr, cases } => {
                self.trace.enter(MatchExpr);
                let expr = Box::new(self.expr(*expr)?);
                self.trace.exit();

                self.trace.enter(MatchCases);
                let cases = cases
                    .into_iter()
                    .map(|b| self.match_branch(b))
                    .collect::<LinkResult<Vec<_>>>()?;
                self.trace.exit();

                Match { expr, cases }
            },

            Call { caller, arg } => {
                self.trace.enter(CallCaller);
                let caller = Box::new(self.expr(*caller)?);
                self.trace.exit();

                self.trace.enter(CallArg);
                let arg = Box::new(self.expr(*arg)?);
                self.trace.exit();

                Call { caller, arg }
            },

            LitVar { id } => {
                self.trace.enter(LitVarId);
                let id = self.get_or_create(id);
                self.trace.exit();
                LitVar { id }
            },

            LitConstructor { id, args } => {
                self.trace.enter(LitConstructorId);
                let id = self.get_or_create(id);
                self.trace.exit();

                self.trace.enter(LitConstructorArgs);
                let args = args
                    .into_iter()
                    .map(|e| self.expr(e))
                    .collect::<LinkResult<Vec<_>>>()?;
                self.trace.exit();

                LitConstructor { id, args }
            }
        })
    }

    fn match_branch(&mut self, raw: MatchBranch<K>)
        -> LinkResult<MatchBranch<LU>>
    {
        self.push_empty_scope();
        self.trace.enter(MatchBranchPattern);
        let pattern = self.pattern(raw.pattern)?;
        self.trace.exit();

        self.trace.enter(MatchBranchBody);
        let body = self.expr(raw.body)?;
        self.trace.exit();
        self.pop_scope();

        Ok(MatchBranch { pattern, body })
    }
}
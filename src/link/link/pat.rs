use crate::lang::{
    TraceNode,
    ast::*
};

use super::{Linker, LinkResult};


use TraceNode::*;


impl<K: Kind> Linker<K> {
    pub fn pattern(&mut self, raw: Pattern<K>) -> LinkResult<Pattern<LU>> {
        use Pattern::*;

        Ok(match raw {
            Var(var) => {
                self.trace.enter(PatternVar);
                let var = self.variable(var)?;
                self.trace.exit();
                Var(var)
            },

            Constructor { id, args } => {
                self.trace.enter(ConstructorId);
                let id = self.get_or_create(id);
                self.trace.exit();

                self.trace.enter(ConstructorArgs);
                let args = args
                    .into_iter()
                    .map(|p| self.pattern(p))
                    .collect::<LinkResult<Vec<Pattern<LU>>>>()?;
                self.trace.exit();

                Constructor { id, args }
            }
        })
    }

    pub fn variable(&mut self, raw: Var<K>) -> LinkResult<Var<LU>> {
        self.trace.enter(VarId);
        let id = self.create(raw.id)?;
        self.trace.exit();
        Ok(Var { id, typ: () })
    }
}
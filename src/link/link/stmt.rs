use crate::lang::{
    TraceNode,
    ast::*
};

use super::{Linker, LinkResult};


use TraceNode::*;


impl<K: Kind> Linker<K> {
    pub fn statements(&mut self, raw: Vec<Statement<K>>)
        -> LinkResult<Vec<Statement<LU>>>
    {
        raw.into_iter()
            .map(|s| self.statement(s))
            .collect()
    }

    fn statement(&mut self, raw: Statement<K>)
        -> LinkResult<Statement<LU>>
    {
        self.trace.enter(StatementVar);
        let var = self.variable(raw.var)?;
        self.trace.exit();

        self.trace.enter(StatementVal);
        let val = self.expr(raw.val)?;
        self.trace.exit();

        Ok(Statement { var, val })
    }
}
use crate::lang::{
    Trace,
    ast::*
};

use super::{
    IdGenerator,
    scope_stack::ScopeStack
};

mod ast;
mod expr;
mod pat;
mod prop;
mod stmt;
mod typ;


#[derive(Clone)]
pub struct Linker<K: Kind> {
    counter: IdGenerator,
    trace: Trace,

    id_stack: ScopeStack<K::Id, usize>,
}


#[derive(Debug)]
pub enum LinkingErr {
    IdAlreadyInScope {
        id: String,
        trace: Trace,
    }
}

type LinkResult<T> = Result<T, LinkingErr>;


impl<I: IntoIterator<Item = LUId>> From<I> for Linker<Raw> {
    fn from(id_iter: I) -> Self {
        let ids: Vec<_> = id_iter.into_iter().collect();
        let max_usize = ids.iter()
            .map(|id| id.id)
            .max()
            .unwrap_or(0);

        let mut id_stack = ScopeStack::new();
        let frame = ids.iter()
            .map(|id| (id.name.clone(), id.id))
            .collect();
        id_stack.push_scope(frame);

        Linker {
            counter: IdGenerator::from(max_usize + 1),
            trace: Trace::new(),
            id_stack
        }
    }
}


impl<K: Kind> Linker<K> {
    pub fn new() -> Linker<K> {
        let mut id_stack = ScopeStack::new();
        id_stack.push_empty_scope();

        Linker {
            counter: IdGenerator::new(),
            trace: Trace::new(),
            id_stack,
        }
    }

    fn get_or_create(&mut self, key: K::Id) -> LUId {
        let id = match self.id_stack.get(&key) {
            Some(&val) => val,
            None => {
                let val = self.counter.gen();
                self.id_stack.insert(key.clone(), val);
                val
            }
        };
        LUId { id, name: key.to_string() }
    }

    fn create(&mut self, key: K::Id) -> LinkResult<LUId> {
        match self.id_stack.get(&key) {
            None => {
                let val = self.counter.gen();
                self.id_stack.insert(key.clone(), val);
                Ok(LUId { id: val, name: key.to_string() })
            }
            Some(_) => Err(LinkingErr::IdAlreadyInScope {
                id: key.to_string(),
                trace: self.trace.clone()
            })
        }
    }

    pub fn create_unassociated(&mut self) -> LUId {
        LUId::unnamed(self.counter.gen())
    }
    
    fn create_just_bellow(&mut self, key: K::Id) -> LinkResult<LUId> {
        match self.id_stack.get(&key) {
            None => {
                let val = self.counter.gen();
                self.id_stack.insert_bellow(1, key.clone(), val);
                Ok(LUId { id: val, name: key.to_string() })
            }
            Some(_) => Err(LinkingErr::IdAlreadyInScope {
                id: key.to_string(),
                trace: self.trace.clone()
            })
        }
    }

    fn push_empty_scope(&mut self) {
        self.id_stack.push_empty_scope();
    }

    fn pop_scope(&mut self) {
        self.id_stack.pop_scope();
    }
}


impl Linker<LU> {
    pub fn reserve_id(&mut self, key: LUId) -> LinkResult<()> {
        match self.id_stack.get(&key) {
            None => {
                // TODO: handle error
                self.counter.reserve(key.id).unwrap();
                self.id_stack.insert(key.clone(), key.id);
                Ok(())
            }
            Some(_) => Err(LinkingErr::IdAlreadyInScope {
                id: key.to_string(),
                trace: self.trace.clone()
            })
        }
    }
}
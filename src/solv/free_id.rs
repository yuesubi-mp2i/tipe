use std::hash::Hash;

use crate::{
    lang::ast::*,
    link::Linker
};


#[derive(Eq, Clone)]
pub struct FreeId {
    pub id: LUId,
    pub assumption_depth: usize
}

impl From<LUId> for FreeId {
    fn from(id: LUId) -> Self {
        FreeId { id, assumption_depth: 0 }
    }
}

impl Hash for FreeId {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}

impl PartialEq for FreeId {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl FreeId {
    pub fn gen_sub_free_id<K: Kind>(&self, linker: &mut Linker<K>) -> FreeId {
        FreeId {
            id: linker.create_unassociated(),
            assumption_depth: self.assumption_depth + 1
        }
    }

    pub fn make_same_depth(&self, id: LUId) -> FreeId {
        FreeId {
            id: id,
            assumption_depth: self.assumption_depth
        }
    }
}
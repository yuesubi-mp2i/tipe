use crate::{
    lang::ast::*,
    link::Linker
};

use Expr::*;


pub fn aliased<K: Kind>(expr: Expr<LU>, linker: &mut Linker<K>) -> Expr<LU> {
    match expr {
        LitVar { id } =>
            LitVar { id },

        LitConstructor { id, args } =>
            LitConstructor {
                id: id,
                args: args
                    .into_iter()
                    .map(|arg| alias_and_expand(arg, linker))
                    .collect()
            },
        
        Call { caller, arg } =>
            Call {
                caller: Box::new(alias_and_expand(*caller, linker)),
                arg: Box::new(alias_and_expand(*arg, linker))
            },

        Function { input, body, .. } =>
            Function {
                input: input,
                out_type: (),
                body: Box::new(alias_and_expand(*body, linker))
            },

        Match { expr, cases } =>
            Match {
                expr: Box::new(alias_and_expand(*expr, linker)),
                cases: cases
                    .into_iter()
                    .map(|MatchBranch { pattern, body }|
                        MatchBranch {
                            pattern,
                            body: alias_and_expand(body, linker)
                        }
                    )
                    .collect()
            },

        Binding { var, val, body } =>
            Binding {
                var: var,
                val: Box::new(aliased(*val, linker)),
                body: Box::new(aliased(*body, linker))
            }
    }
}


fn alias_and_expand<K: Kind>(expr: Expr<LU>, linker: &mut Linker<K>) -> Expr<LU> {
    match expr {
        LitVar { id } => LitVar { id },

        Binding { var, val, body }
        if matches!(*body, LitVar { .. }) =>
            Binding {
                var,
                val: Box::new(aliased(*val, linker)),
                body
            },

        complex => {
            let alias = linker.create_unassociated();
            Binding {
                var: Var { id: alias.clone(), typ: () },
                val: Box::new(aliased(complex, linker)),
                body: Box::new(LitVar { id: alias })
            }
        }
    }
}


#[cfg(test)]
mod tests {
    use crate::lang::read::expr;

    use super::*;


    fn assert_aliased_eq(original: &str, normal: &str) {
        let mut linker = Linker::new();

        assert_eq!(
            aliased(
                linker.expr(expr(original.chars())).unwrap(),
                &mut linker
            ).link(),
            expr(normal.chars()).link()
        );
    }
    

    #[test]
    fn test_aliased() {
        assert_aliased_eq(
            "Succ (Succ (Zero))",
            "Succ (let x = Succ (let y = Zero in y) in x)"
        );

        assert_aliased_eq(
            "fun (x) -> x",
            "fun (x) -> x"
        );

        assert_aliased_eq(
            "let id = fun (x) -> x in id y",
            "let id = fun (x) -> x in id y"
        );
    }
}
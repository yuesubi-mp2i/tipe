use std::collections::HashSet;

use crate::lang::ast::*;

use super::super::utils::*;

use Expr::*;


pub fn factorized(expr: Expr<LU>) -> Expr<LU> {
    match expr {
        LitVar { id } =>
            LitVar { id },

        LitConstructor { id, args } =>
            lit_constructor(id, args),
        
        Call { caller, arg } =>
            call(*caller, *arg),

        Function { input, body, .. } =>
            function(input, *body),

        Match { expr, cases } =>
            match_with(*expr, cases),

        Binding { var, val, body } =>
            binding(var, *val, *body),
    }
}


fn lit_constructor(id: LUId, args: Vec<Expr<LU>>) -> Expr<LU> {
    let (bindings_vec, new_args) : (Vec<_>, Vec<_>) =
        args
        .into_iter()
        .map(|e| factorized(e))
        .map(|e|
            extract::free_bindings(HashSet::new(), e)
        )
        .unzip();

    misc::fold_bindings(
        bindings_vec
            .into_iter()
            .map(|bindings| bindings.into_iter())
            .flatten(),
        LitConstructor {
            id,
            args: new_args
        }
    )
}


fn call(caller: Expr<LU>, arg: Expr<LU>) -> Expr<LU> {
    let (caller_binds, caller) =
        extract::free_bindings(HashSet::new(), factorized(caller));

    let (arg_binds, arg) =
        extract::free_bindings(HashSet::new(), factorized(arg));

    misc::fold_bindings(
        caller_binds
            .into_iter()
            .chain(arg_binds.into_iter()),
        Call { caller: Box::new(caller), arg: Box::new(arg) }
    )
}


fn function(input: Var<LU>, body: Expr<LU>) -> Expr<LU> {
    let bound_ids = HashSet::from([input.clone().id]);
    let (bindings, body) =
        extract::free_bindings(bound_ids, factorized(body));
    
    misc::fold_bindings(
        bindings.into_iter(),
        Function {
            input,
            out_type: (),
            body: Box::new(body)
        }
    )
}

fn match_with(expr: Expr<LU>, cases: Vec<MatchBranch<LU>>)
    -> Expr<LU>
{
    let (expr_bindings, expr) =
        extract::free_bindings(HashSet::new(), factorized(expr));

    let (bindings_vec, cases): (Vec<_>, Vec<_>) =
        cases
        .into_iter()
        .map(|MatchBranch { pattern, body }| {
            let (bindings, body) =
                extract::free_bindings(
                    find::pattern_ids(&pattern),
                    body
                );
            (bindings, MatchBranch { pattern, body })
        })
        .unzip();

    misc::fold_bindings(
        expr_bindings
            .into_iter()
            .chain(
                bindings_vec
                    .into_iter()
                    .map(|b| b.into_iter())
                    .flatten()
            ),
        Match { expr: Box::new(expr), cases: cases }
    )
}


fn binding(var: Var<LU>, val: Expr<LU>, body: Expr<LU>) -> Expr<LU> {
    let (val_bindings, val) =
        extract::free_bindings(HashSet::new(), factorized(val));

    let body = factorized(body);
    
    misc::fold_bindings(
        val_bindings
            .into_iter(),
        Binding {
            var: var,
            val: Box::new(val),
            body: Box::new(body)
        }
    )
}


#[cfg(test)]
mod tests {
    use crate::lang::read::expr;

    use super::*;


    fn assert_factorized_eq(original: &str, normal: &str) {
        assert_eq!(
            factorized(expr(original.chars()).link()).link(),
            expr(normal.chars()).link()
        );
    }
    

    #[test]
    fn test_lit_constructor() {
        assert_factorized_eq("Zero", "Zero");

        assert_factorized_eq("Succ Zero", "Succ Zero");

        assert_factorized_eq(
            "Tuple (
                let y = z in
                let x = y in
                x,
                let b = a in
                b
            )",
            "
            let y = z in
            let x = y in
            let b = a in
            Tuple (x, b)
            "
        );
    }


    #[test]
    fn test_function() {
        assert_factorized_eq(
            "fun (b) -> let c = False in b",
            "let c = False in fun (b) -> b"
        );

        assert_factorized_eq(
            "fun (b) -> let c = b in b",
            "fun (b) -> let c = b in b"
        );

        assert_factorized_eq(
            "fun (b) -> let c = b in c",
            "fun (b) -> let c = b in c"
        );
    }
}
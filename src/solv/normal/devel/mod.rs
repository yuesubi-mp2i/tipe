use crate::{lang::ast::*, link::Linker};

mod alias;
mod factor;

use alias::aliased;
use factor::factorized;


pub fn developed<K: Kind>(expr: Expr<LU>, linker: &mut Linker<K>) -> Expr<LU> {
    factorized(aliased(expr, linker))
}
use crate::lang::ast::*;

use Expr::*;


pub fn fold_bindings<I>(bindings: I, tail: Expr<LU>) -> Expr<LU>
    where I: DoubleEndedIterator<Item = (Var<LU>, Expr<LU>)>
{
    bindings
        .rev()
        .fold(
            tail,
            |expr, (var, val)|
                Binding {
                    var: var,
                    val: Box::new(val),
                    body: Box::new(expr)
                }
        )
}
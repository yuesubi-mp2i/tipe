use std::collections::HashSet;
use crate::lang::ast::*;

use super::find;
use super::misc::fold_bindings;

use Expr::*;


type FreeBindings = Vec<(Var<LU>, Expr<LU>)>;
type BoundExpr = Expr<LU>;

type BindingList = Vec<(Var<LU>, Expr<LU>)>;


pub fn free_bindings(mut bound_ids: HashSet<LUId>, expr: Expr<LU>)
    -> (FreeBindings, BoundExpr)
{
    let (bindings, res_expr) =
        top_bindings(expr);

    for (var, body) in &bindings {
        if !(find::expr_free_var_ids(body).is_disjoint(&bound_ids)) {
            bound_ids.insert(var.id.clone());
        }
    }

    let mut bound_bindings = Vec::new();
    let mut free_bindings = Vec::new();

    for (var, val) in bindings {
        if bound_ids.contains(&var.id) {
            bound_bindings.push((var, val));
        } else {
            free_bindings.push((var, val));
        }
    }

    let bound_expr = fold_bindings(
        bound_bindings.into_iter(),
        res_expr
    );

    (free_bindings, bound_expr)
}


fn top_bindings(expr: Expr<LU>) -> (BindingList, Expr<LU>)
{
    let mut bindings = Vec::new();
    let mut res_expr = expr;

    while let Binding { var, val, body } = res_expr {
        bindings.push((var, *val));
        res_expr = *body;
    };

    (bindings, res_expr)
}


#[cfg(test)]
mod tests {
    use crate::lang::read::expr;

    use super::*;


    // #[test]
    // fn test_free_bindings() {
    //     todo!()
    // }


    #[test]
    fn test_top_bindings() {
        let id = expr("fun (x) -> x".chars()).link();
        assert_eq!(
            top_bindings(id.clone()),
            (Vec::new(), id)
        );

        let dbl_alias = expr("let y = z in let x = y in x".chars()).link();
        assert_eq!(
            top_bindings(dbl_alias),
            (
                vec![
                    (Var { id: LUId::from((0, "y")), typ: () },
                        LitVar { id: LUId::from((1, "z")) }),
                    (Var { id: LUId::from((2, "x")), typ: () },
                        LitVar { id: LUId::from((0, "y")) })
                ],
                LitVar { id: LUId::from((2, "x")) }
            )
        );
    }
}
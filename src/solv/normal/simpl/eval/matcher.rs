use std::collections::HashMap;

use crate::lang::ast::*;
use super::pat;


#[derive(Debug, PartialEq, Eq)]
pub struct MatchSolution {
    pub variables: HashMap<LUId, Expr<LU>>,
    pub chosen: Expr<LU>
}


pub fn resolve_explicit(
    bindings: &HashMap<LUId, Expr<LU>>,
    branches: &Vec<MatchBranch<LU>>,
    expr: &Expr<LU>
) -> Option<MatchSolution>
{
    for MatchBranch { pattern, body } in branches {
        match pat::fit(bindings, pattern, expr) {
            None => {}
            Some(variables) =>
                return Some(MatchSolution { variables, chosen: body.clone() })
        }
    }

    None
}
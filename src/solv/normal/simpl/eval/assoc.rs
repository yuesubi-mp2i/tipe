use std::collections::HashMap;

use crate::lang::ast::*;
use Expr::*;


pub fn get(bindings: &HashMap<LUId, Expr<LU>>, expr: &Expr<LU>) -> Expr<LU> {
    match expr {
        LitVar { id } =>
            match bindings.get(&id) {
                None => LitVar { id: id.clone() },

                Some(Binding { var, val, body }) => {
                    let mut new_bindings = bindings.clone();
                    new_bindings.insert(var.id.clone(), *val.clone());
                    get(&new_bindings, body)
                },

                Some(expr) => get(bindings, expr)
            },
        
        other => other.clone()
    }
}
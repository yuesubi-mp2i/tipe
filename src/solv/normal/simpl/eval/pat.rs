use std::collections::HashMap;
use crate::lang::ast::*;

use super::assoc;

use Expr::*;


pub fn fit(bindings: &HashMap<LUId, Expr<LU>>, pat: &Pattern<LU>, expr: &Expr<LU>)
    -> Option<HashMap<LUId, Expr<LU>>>
{
    match pat {
        Pattern::Var(Var { id, .. }) =>
            Some(HashMap::from([(id.clone(), expr.clone())])),

        Pattern::Constructor { id: pat_id, args: pat_args } =>
            match assoc::get(&bindings, expr) {
                LitConstructor { id: expr_id, args: expr_args }
                if pat_id == &expr_id =>
                {
                    let mut variables = HashMap::new();
                    let pats_exprs = pat_args.iter()
                        .zip(expr_args.iter());

                    for (pat, expr) in pats_exprs {
                        match fit(bindings, pat, expr) {
                            None => return None,
                            Some(vars) =>
                                variables.extend(vars),
                        }
                    }

                    Some(variables)
                },

                _ => None
            },

        _ => None
    }
}
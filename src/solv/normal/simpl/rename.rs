use crate::lang::ast::*;

use Expr::*;


pub fn rename_var(old: &LUId, new: &LUId, expr: Expr<LU>) -> Expr<LU> {
    match expr {
        LitVar { id } =>
            if &id == old {
                LitVar { id: new.clone() }
            } else {
                LitVar { id }
            }

        LitConstructor { id, args } =>
            LitConstructor {
                id,
                args: args
                    .into_iter()
                    .map(|expr| rename_var(old, new, expr))
                    .collect()
            },
        
        Binding { var, val, body } =>
            Binding {
                var,
                val: Box::new(rename_var(old, new, *val)),
                body: Box::new(rename_var(old, new, *body))
            },
        
        Function { input, out_type, body } =>
            Function {
                input,
                out_type,
                body: Box::new(rename_var(old, new, *body))
            },
        
        Call { caller, arg } =>
            Call {
                caller: Box::new(rename_var(old, new, *caller)),
                arg: Box::new(rename_var(old, new, *arg))
            },
        
        Match { expr, cases } =>
            Match {
                expr: Box::new(rename_var(old, new, *expr)),
                cases: cases
                    .into_iter()
                    .map(|MatchBranch { pattern, body }|
                        MatchBranch {
                            pattern,
                            body: rename_var(old, new, body)
                        }
                    )
                    .collect()
            }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    use crate::lang::read::expr;


    #[test]
    fn test_rename_var() {
        assert_eq!(
            rename_var(
                &LUId::from((7, "x")), &LUId::from((42, "y")),
                expr("Succ (n)".chars()).link()
            ),
            expr("Succ (n)".chars()).link()
        );

        assert_eq!(
            rename_var(
                &LUId::from((1, "x")), &LUId::from((1,"y")),
                expr("Tuple (x, x)".chars()).link()
            ),
            expr("Tuple (y, y)".chars()).link()
        );
    }
}
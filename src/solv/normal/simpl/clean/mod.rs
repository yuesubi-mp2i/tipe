use crate::lang::ast::*;

mod unalias;
mod used;

use unalias::unaliased;
use used::used;


pub fn cleaned(expr: Expr<LU>) -> Expr<LU> {
    // - remove useless aliases by renaming
    // - remove unused lets
    // - TODO: remove duplicate lets
    used(unaliased(expr))
}
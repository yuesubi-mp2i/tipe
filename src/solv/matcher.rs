use std::collections::HashMap;

use crate::lang::ast::*;

use Expr::*;


pub struct FoundMatch {
    pub bindings: HashMap<LUId, Expr<LU>>,
    pub expr: Expr<LU>,
    pub cases: Vec<MatchBranch<LU>>
}


pub fn find_match(expr: Expr<LU>) -> Option<FoundMatch> {
    let bindings = HashMap::new();

    find_match_aux(bindings, expr)
}

fn find_match_aux(mut bindings: HashMap<LUId, Expr<LU>>, expr: Expr<LU>)
    -> Option<FoundMatch>
{
    match expr {
        Binding { var, val, body } =>
            find_match_aux(bindings.clone(), *val.clone())
                .or_else(|| {
                    bindings.insert(var.id, *val);
                    find_match_aux(bindings, *body)
                }),

        Function { body, .. } =>
            find_match_aux(bindings, *body),

        Call { caller, arg } =>
            find_match_aux(bindings.clone(), *caller)
                .or_else(|| find_match_aux(bindings, *arg)),

        Match { expr, cases } =>
            Some(FoundMatch { bindings, expr: *expr, cases }),

        LitVar { .. } => None,

        LitConstructor { args, .. } =>
            args.into_iter()
                .fold(
                    None,
                    |opt_res, e|
                        opt_res.or_else(|| find_match_aux(
                            bindings.clone(),
                            e
                        ))
                )
    }
}
use std::collections::{HashMap, HashSet};
use crate::lang::ast::*;

use super::free_id::FreeId;


pub struct Assumption {
    pub assumptions: HashMap<FreeId, Expr<LU>>,
    pub new_free_ids: HashSet<FreeId>
}

impl Assumption {
    pub fn empty() -> Self {
        Assumption {
            assumptions: HashMap::new(),
            new_free_ids: HashSet::new()
        }
    }

    pub fn used_free_ids(&self) -> HashSet<FreeId> {
        self.assumptions.keys()
            .cloned()
            .collect()
    }

    pub fn refine(mut self, other: Assumption) -> Assumption {
        self.assumptions.extend(other.assumptions);
        self.new_free_ids.extend(other.new_free_ids);
        self
    }
}


pub fn make_into(
    pat: &Pattern<LU>,
    expr: &Expr<LU>,
    free_ids: &HashSet<FreeId>,
) -> Option<Assumption>
{
    match (pat, expr) {
        (Pattern::Var(Var { .. }), _) =>
            Some(Assumption::empty()),
        
        (constructor_pat, Expr::LitVar { id: var_id })
        if free_ids.contains(&FreeId::from(var_id.clone())) =>
        {
            let free_id = free_ids
                .get(&FreeId::from(var_id.clone()))
                .unwrap()
                .clone();

            let Fitting { expr, new_free_ids } =
                make_fitting(constructor_pat, free_id.assumption_depth);

            Some(Assumption {
                assumptions: HashMap::from([(free_id, expr)]),
                new_free_ids
            })
        },

        (Pattern::Constructor { id: pat_id, args: pat_args },
            Expr::LitConstructor { id: lit_id, args: lit_args  })
        if pat_id == lit_id =>
        {
            pat_args
                .iter()
                .zip(lit_args.iter())
                .fold(
                    Some(Assumption::empty()),
                    |opt_assumption, (pat, expr)| {
                        let assumption = opt_assumption?;
                        let free_ids = free_ids
                            .difference(&assumption.used_free_ids())
                            .cloned()
                            .collect();

                        let new_assumption = make_into(pat, expr, &free_ids)?;
                        Some(assumption.refine(new_assumption))
                    }
                )
        }

        _ => None
    }
}


struct Fitting {
    pub expr: Expr<LU>,
    pub new_free_ids: HashSet<FreeId>
}

fn make_fitting(
    pat: &Pattern<LU>,
    assumption_depth: usize
) -> Fitting
{
    use Expr::*;

    match pat {
        Pattern::Var(Var { id, .. }) =>
            Fitting {
                expr: LitVar { id: id.clone() },
                new_free_ids: HashSet::from([
                    FreeId { id: id.clone(), assumption_depth }
                ])
            },

        Pattern::Constructor { id, args } => {
            let (args, new_free_idss): (Vec<_>, Vec<_>) = args.iter()
                .map(|arg| make_fitting(
                    arg, assumption_depth + 1
                ))
                .map(|Fitting { expr, new_free_ids }|
                    (expr, new_free_ids)
                )
                .unzip();

            Fitting {
                expr: LitConstructor { id: id.clone(), args },
                new_free_ids: new_free_idss.into_iter()
                    .flatten()
                    .collect()
            }
        }
    }
}


/*
#[cfg(test)]
mod tests {
    use crate::lang::read;
    use super::*;

    #[test]
    fn test_fit() {
        let any = read::pattern("_".chars());
        let id = read::expr("fun (x) -> x".chars());

        let dbl_succ = read::pattern("Succ (Succ (n'))".chars());
        let one = read::expr("Succ (Zero)".chars());
        let tree = read::expr("Succ (Succ (Succ (Zero)))".chars());

        // assert_eq!(
        //     fit(&any, &id),
        //     Some(HashMap::from([
        //         (String::from("_"), id.clone())
        //     ]))
        // );
        // assert_eq!(
        //     fit(&any, &one),
        //     Some(HashMap::from([
        //         (String::from("_"), one.clone())
        //     ]))
        // );
        // assert_eq!(
        //     fit(&any, &tree),
        //     Some(HashMap::from([
        //         (String::from("_"), tree.clone())
        //     ]))
        // );
        // 
        // assert_eq!(fit(&dbl_succ, &id), None);
        // assert_eq!(fit(&dbl_succ, &one), None);
        // assert_eq!(
        //     fit(&dbl_succ, &tree),
        //     Some(HashMap::from([
        //         (String::from("n'"), one.clone())
        //     ]))
        // );
    }


    #[test]
    fn test_make_into() {
        let succ = read::pattern("Succ (n')".chars());
        let id = read::expr("fun (x) -> x".chars());
        // let x = read::expr("x".chars());
        // let succ_x = read::expr("Succ (x)".chars());

        //assert_eq!(make_into(&succ, &id), None);
        // TODO: test when structural equality works
        // assert_eq!(make_into(&succ, &x), None);
        // assert_eq!(make_into(&succ, &succ_x), None);
    }
}*/
use std::collections::{HashMap, HashSet};

use crate::lang::{Expr, ExprData, MatchBranch, Linked};

use super::free_id::FreeId;



pub fn expressions_match(
    pattern: Expr<Linked>, patternee: Expr<Linked>,
    free_ids: &HashSet<FreeId>
) -> Option<HashMap<FreeId, Expr<Linked>>>
{
    use ExprData::*;
    // TODO: fix to handle matches on Tuple (x, x) properly, and more generally,
    // any pattern that uses a same variable multiple times.
    // This problem arises whenever expression_match is called on multiple
    // sub-expression one after the other (i.e. when chain is used).

    match (pattern.data, patternee.data) {
        (LitVar { id: pat_id }, expr)
        if free_ids.contains(&FreeId::from(pat_id.clone())) =>
        {
            let free_id = free_ids.get(&FreeId::from(pat_id))
                .unwrap()
                .clone();
            Some(HashMap::from([(free_id, expr.into())]))
        },

        (LitConstructor { id: pat_id, args: pat_args },
            LitConstructor { id, args })
        if pat_id == id =>
            pat_args.into_iter()
                .zip(args.into_iter())
                .map(|(pat, expr)|
                    expressions_match(pat, expr, free_ids)
                )
                .fold(
                    Some(HashMap::new()),
                    |opta, optb| Some(
                        opta?.into_iter()
                            .chain(optb?.into_iter())
                            .collect()
                    )
                ),
        
        (Function { input: pat_input, body: pat_body, .. },
            Function { input, body, .. })
        if pat_input.id == input.id =>
            expressions_match(*pat_body, *body, free_ids),
        
        (Call { caller: pat_caller, arg: pat_arg },
            Call { caller, arg }) =>
            Some(
                expressions_match(*pat_caller, *caller, free_ids)?.into_iter()
                    .chain(expressions_match(*pat_arg, *arg, free_ids)?.into_iter())
                    .collect()
            ),
        
        (Binding { var: pat_var, val: pat_val, body: pat_body },
            Binding { var, val, body })
        if pat_var.id == var.id =>
            Some(
                expressions_match(*pat_val, *val, free_ids)?.into_iter()
                    .chain(expressions_match(*pat_body, *body, free_ids)?.into_iter())
                    .collect()
            ),
        
        (Match { expr: pat_expr, cases: pat_cases },
            Match { expr, cases })
        if pat_cases.len() == cases.len() =>
        {
            // TODO: check if match patterns are equal
            pat_cases.into_iter()
                .map(|MatchBranch { body, .. }| body)
                .zip(
                    cases.into_iter()
                        .map(|MatchBranch { body, .. }| body)
                )
                .chain([(*pat_expr, *expr)].into_iter())
                .map(|(pat, expr)|
                    expressions_match(pat, expr, free_ids)
                )
                .fold(
                    Some(HashMap::new()),
                    |opta, optb| Some(
                        opta?.into_iter()
                            .chain(optb?.into_iter())
                            .collect()
                    )
                )
        }

        _ => None
    }
}
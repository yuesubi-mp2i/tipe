use crate::lang::*;

mod clean;
mod eval;
mod rename;

use clean::cleaned;
use eval::evaluated;


pub fn simplified(expr: Expr<Linked>) -> Expr<Linked> {
    cleaned(evaluated(expr))
}


#[cfg(test)]
mod tests {
    use crate::process::read::expr;
    use super::*;


    fn assert_simplified_eq(original: &str, normal: &str) {
        assert_eq!(
            simplified(expr(original.chars()).link()).link(),
            expr(normal.chars()).link()
        );
    }


    #[test]
    fn test_evaluated() {
        assert_simplified_eq(
            "(fun (x) -> x) y",
            "y",
        );
    }
}
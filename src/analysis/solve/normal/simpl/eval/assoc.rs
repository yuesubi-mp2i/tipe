use std::collections::HashMap;

use crate::lang::*;
use ExprData::*;


pub fn get(bindings: &HashMap<LId, Expr<Linked>>, expr: &Expr<Linked>) -> Expr<Linked> {
    match &expr.data {
        LitVar { id } =>
            match bindings.get(&id).map(|val| &val.data) {
                None => LitVar { id: id.clone() }.into(),

                Some(Binding { var, val, body }) => {
                    let mut new_bindings = bindings.clone();
                    new_bindings.insert(var.id.clone(), *val.clone());
                    get(&new_bindings, &body)
                },

                Some(expr) => get(bindings, &expr.clone().into())
            },
        
        other => other.clone().into()
    }
}
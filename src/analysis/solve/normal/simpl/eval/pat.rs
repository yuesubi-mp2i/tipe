use std::collections::HashMap;
use crate::lang::*;

use super::assoc;

use ExprData::*;


pub fn fit(bindings: &HashMap<LId, Expr<Linked>>, pat: &Pattern<Linked>, expr: &Expr<Linked>)
    -> Option<HashMap<LId, Expr<Linked>>>
{
    use PatternData as PD;

    match &pat.data {
        PD::Var(Var { id, .. }) =>
            Some(HashMap::from([(id.clone(), expr.clone())])),

        PD::Constructor { id: pat_id, args: pat_args } =>
            match assoc::get(&bindings, expr).data {
                LitConstructor { id: expr_id, args: expr_args }
                if pat_id == &expr_id =>
                {
                    let mut variables = HashMap::new();
                    let pats_exprs = pat_args.iter()
                        .zip(expr_args.iter());

                    for (pat, expr) in pats_exprs {
                        match fit(bindings, pat, expr) {
                            None => return None,
                            Some(vars) =>
                                variables.extend(vars),
                        }
                    }

                    Some(variables)
                },

                _ => None
            },

        _ => None
    }
}
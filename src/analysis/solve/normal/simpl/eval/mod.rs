use crate::lang::*;

mod assoc;
mod call;
mod choice;
mod matcher;
mod pat;
mod tail;

use call::called;
use choice::chosen;
use tail::tailed;


pub fn evaluated(expr: Expr<Linked>) -> Expr<Linked> {
    let mut res = expr;

    loop {
        let last = res.clone();
        res = chosen(called(res));

        if res == last {
            break;
        }
    }

    tailed(res)
}


#[cfg(test)]
mod tests {
    use crate::process::read::expr;
    use super::*;


    fn assert_evaluated_eq(original: &str, normal: &str) {
        assert_eq!(
            evaluated(expr(original.chars()).link()).link(),
            expr(normal.chars()).link()
        );
    }


    #[test]
    fn test_evaluated() {
        assert_evaluated_eq(
            "(fun (x) -> x) y",
            "let x = y in y",
        );
    }
}
use std::collections::HashMap;

use crate::lang::*;
use super::pat;


#[derive(Debug, PartialEq, Eq)]
pub struct MatchSolution {
    pub variables: HashMap<LId, Expr<Linked>>,
    pub chosen: Expr<Linked>
}


pub fn resolve_explicit(
    bindings: &HashMap<LId, Expr<Linked>>,
    branches: &Vec<MatchBranch<Linked>>,
    expr: &Expr<Linked>
) -> Option<MatchSolution>
{
    for MatchBranch { pattern, body } in branches {
        match pat::fit(bindings, pattern, expr) {
            None => {}
            Some(variables) =>
                return Some(MatchSolution { variables, chosen: body.clone() })
        }
    }

    None
}
use std::collections::HashMap;

use crate::lang::*;

use super::super::super::utils::misc;
use super::matcher;

use ExprData::*;


pub fn chosen(expr: Expr<Linked>) -> Expr<Linked> {
    let mut bindings = HashMap::new();
    chosen_aux(&mut bindings, expr)
}


fn chosen_aux(bindings: &mut HashMap<LId, Expr<Linked>>, expr: Expr<Linked>) -> Expr<Linked> {
    match expr.data {
        Match { expr, cases } => {
            let simpler_expr = chosen_aux(bindings, *expr);

            match matcher::resolve_explicit(bindings, &cases, &simpler_expr) {
                None => Match {
                    expr: Box::new(simpler_expr),
                    cases: cases
                        .into_iter()
                        .map(|MatchBranch { pattern, body }|
                            MatchBranch {
                                pattern,
                                body: chosen_aux(bindings, body)
                            }
                        )
                        .collect()
                },

                Some(matcher::MatchSolution {
                    variables,
                    chosen: choice
                }) => {
                    let mut new_bindings = bindings.clone();
                    new_bindings.extend(
                        variables
                            .clone()
                            .into_iter()
                    );

                    let let_ins: Vec<_> = variables
                        .into_iter()
                        .map(|(id, val)|
                            (Var { id, typ: None, meta: () }, val)
                        )
                        .collect();

                    misc::fold_bindings(
                        let_ins.into_iter(),
                        chosen_aux(&mut new_bindings, choice)
                    ).data
                }
            }
        },

        LitVar { id } =>
            LitVar { id },
        
        LitConstructor { id, args } => LitConstructor {
            id,
            args: args
                .into_iter()
                .map(|arg| chosen_aux(bindings, arg))
                .collect()
        },

        Call { caller, arg } =>
            Call {
                caller: Box::new(*caller),
                arg: Box::new(*arg)
            },

        Function { input, out_type, body } =>
            Function {
                input,
                out_type,
                body: Box::new(chosen_aux(bindings, *body))
            },

        Binding { var, val, body } => {
            let val = chosen_aux(bindings, *val);

            let mut new_bindings = bindings.clone();
            new_bindings.insert(var.id.clone(), val.clone());
            let body = chosen_aux(&mut new_bindings, *body);

            Binding { var, val: Box::new(val), body: Box::new(body) }.into()
        }
    }.into()
}


#[cfg(test)]
mod tests {
    use crate::process::read::expr;
    use super::*;


    fn assert_chosen_eq(original: &str, normal: &str) {
        assert_eq!(
            chosen(expr(original.chars()).link()).link(),
            expr(normal.chars()).link()
        );
    }


    #[test]
    fn test_chosen() {
        assert_chosen_eq(
            "match Succ (Zero) with Succ n -> n",
            "let n = Zero in n",
        );

        assert_chosen_eq(
            "match Succ (Zero) with Zero -> x",
            "match Succ (Zero) with Zero -> x",
        );
    }
}
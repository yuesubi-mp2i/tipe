use std::collections::HashMap;

use crate::lang::*;
use ExprData::*;

use super::assoc;


pub fn tailed(expr: Expr<Linked>) -> Expr<Linked> {
    let mut bindings = HashMap::new();
    tailed_aux(&mut bindings, expr)
}


fn tailed_aux(bindings: &mut HashMap<LId, Expr<Linked>>, expr: Expr<Linked>) -> Expr<Linked> {
    match expr.data {
        Binding { var, val, body } => {
            let deref_val = tailed_aux(bindings, *val);

            let mut new_bindings = bindings.clone();
            new_bindings.insert(var.id.clone(), deref_val.clone());

            // TODO: fix this horrendous complexity
            let tail = assoc::get(&new_bindings, &*body);
            let deref_body = tailed_aux(&mut new_bindings, tail);

            Binding { var, val: Box::new(deref_val), body: Box::new(deref_body) }
        },

        LitVar { id } =>
            LitVar { id },
        
        LitConstructor { id, args } => LitConstructor {
            id,
            args: args
                .into_iter()
                .map(|arg| tailed_aux(bindings, arg))
                .collect()
        },

        Call { caller, arg } =>
            Call {
                caller: Box::new(tailed_aux(bindings, *caller)),
                arg: Box::new(tailed_aux(bindings, *arg))
            },

        Function { input, out_type, body } =>
            Function {
                input,
                out_type,
                body: Box::new(tailed_aux(bindings, *body))
            },

        Match { expr, cases } =>
            Match {
                expr: Box::new(tailed_aux(bindings, *expr)),
                cases: cases
                    .into_iter()
                    .map(|MatchBranch { pattern, body }|
                        MatchBranch {
                            pattern,
                            body: tailed_aux(bindings, body)
                        }
                    )
                    .collect()
            }
    }.into()
}


#[cfg(test)]
mod tests {
    use crate::process::read::expr;
    use super::*;


    fn assert_tailed_eq(original: &str, normal: &str) {
        assert_eq!(
            tailed(expr(original.chars()).link()).link(),
            expr(normal.chars()).link()
        );
    }


    #[test]
    fn test_tailed() {
        assert_tailed_eq(
            "let id = (fun (x) -> x) in id",
            "let id = (fun (x) -> x) in fun (x) -> x",
        );
    }
}
use std::collections::HashMap;

use crate::lang::*;
use ExprData::*;

use super::assoc;


pub fn called(expr: Expr<Linked>) -> Expr<Linked> {
    let mut bindings = HashMap::new();
    called_aux(&mut bindings, expr)
}


fn called_aux(bindings: &mut HashMap<LId, Expr<Linked>>, expr: Expr<Linked>) -> Expr<Linked> {
    match expr.data {
        Call { caller, arg } => {
            let caller = called_aux(bindings, *caller);
            let arg = called_aux(bindings, *arg);

            match assoc::get(bindings, &caller).data {
                Function { input, body, .. } => {
                    let mut new_bindings = bindings.clone();
                    new_bindings.insert(input.id.clone(), arg.clone());

                    Binding {
                        var: input,
                        val: Box::new(arg),
                        body: Box::new(called_aux(&mut new_bindings,
                             *body
                        ))
                    }
                },

                _ => Call {
                    caller: Box::new(caller),
                    arg: Box::new(arg)
                }
            } 
        }

        LitVar { id } =>
            LitVar { id },
        
        LitConstructor { id, args } => LitConstructor {
            id,
            args: args
                .into_iter()
                .map(|arg| called_aux(bindings, arg))
                .collect()
        },

        Function { input, out_type, body } =>
            Function {
                input,
                out_type,
                body: Box::new(called_aux(bindings, *body))
            },

        Match { expr, cases } =>
            Match {
                expr: Box::new(called_aux(bindings, *expr)),
                cases: cases
                    .into_iter()
                    .map(|MatchBranch { pattern, body }|
                        MatchBranch {
                            pattern,
                            body: called_aux(bindings, body)
                        }
                    )
                    .collect()
            },

        Binding { var, val, body } => {
            let val = called_aux(bindings, *val);

            let mut new_bindings = bindings.clone();
            new_bindings.insert(var.id.clone(), val.clone());
            let body = called_aux(&mut new_bindings, *body);

            Binding { var, val: Box::new(val), body: Box::new(body) }
        }
    }.into()
}


#[cfg(test)]
mod tests {
    use crate::process::read::expr;
    use super::*;


    fn assert_called_eq(original: &str, normal: &str) {
        assert_eq!(
            called(expr(original.chars()).link()).link(),
            expr(normal.chars()).link()
        );
    }


    #[test]
    fn test_called() {
        assert_called_eq(
            "(fun (x) -> x) y",
            "let x = y in x",
        );

        assert_called_eq(
            "let f = fun (x) -> x in f y",
            "let f = fun (x) -> x in let x = y in x",
        );
    }
}
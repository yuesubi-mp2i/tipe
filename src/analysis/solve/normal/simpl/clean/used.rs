use std::collections::HashSet;
use crate::lang::*;

use ExprData::*;


pub fn used(expr: Expr<Linked>) -> Expr<Linked> {
    let (_, expr) = used_aux(expr);
    expr
}


type UsedVarIds = HashSet<LId>;


fn used_aux(expr: Expr<Linked>) -> (UsedVarIds, Expr<Linked>) {
    match expr.data {
        Binding { var, val, body } => {
            let (mut used_ids, deref_body) = used_aux(*body);

            if used_ids.contains(&var.id) {
                let (val_used_ids, deref_val) = used_aux(*val);

                used_ids.insert(var.id.clone());
                used_ids.extend(val_used_ids);

                let expr = Binding {
                    var,
                    val: Box::new(deref_val),
                    body: Box::new(deref_body)
                };

                (used_ids, expr.into())
            } else {
                (used_ids, deref_body)
            }

        },

        LitVar { id } =>
            (HashSet::from([id.clone()]), LitVar { id }.into()),
        
        LitConstructor { id, args } => {
            let (nested_ids, args): (Vec<_>, _) =
                args
                .into_iter()
                .map(|arg| used_aux(arg))
                .unzip();

            (nested_ids.into_iter().flatten().collect(),
                LitConstructor { id, args }.into())
        },

        Call { caller, arg } => {
            let (caller_ids, deref_caller) = used_aux(*caller);
            let (arg_ids, deref_arg) = used_aux(*arg);

            let ids = caller_ids
                .into_iter()
                .chain(arg_ids.into_iter())
                .collect();

            let expr = Call {
                caller: Box::new(deref_caller),
                arg: Box::new(deref_arg)
            };

            (ids, expr.into())
        },

        Function { input, out_type, body } => {
            let (ids, deref_body) = used_aux(*body);

            let expr = Function {
                input,
                out_type,
                body: Box::new(deref_body)
            };

            (ids, expr.into())
        },

        Match { expr, cases } => {
            let (expr_ids, deref_expr) = used_aux(*expr);

            let (nested_ids, cases): (Vec<_>, _) = cases
                .into_iter()
                .map(|MatchBranch { pattern, body }| {
                    let (ids, body) = used_aux(body);
                    (ids, MatchBranch { pattern, body })
                })
                .unzip();

            let ids = expr_ids
                .into_iter()
                .chain(
                    nested_ids
                        .into_iter()
                        .flatten()
                )
                .collect();

            (ids, Match { expr: Box::new(deref_expr), cases }.into())
        }
    }
}


#[cfg(test)]
mod tests {
    use crate::process::read::expr;
    use super::*;


    fn assert_used_eq(original: &str, normal: &str) {
        assert_eq!(
            used(expr(original.chars()).link()).link(),
            expr(normal.chars()).link()
        );
    }


    #[test]
    fn test_used() {
        assert_used_eq(
            "let _ = x in y",
            "y",
        );
    }
}
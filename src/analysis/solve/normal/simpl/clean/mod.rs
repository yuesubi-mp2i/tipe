use crate::lang::*;

mod unalias;
mod used;

use unalias::unaliased;
use used::used;


pub fn cleaned(expr: Expr<Linked>) -> Expr<Linked> {
    // - remove useless aliases by renaming
    // - remove unused lets
    // - TODO: remove duplicate lets
    used(unaliased(expr))
}
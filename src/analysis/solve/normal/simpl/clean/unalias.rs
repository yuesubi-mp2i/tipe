use crate::lang::*;
use super::super::rename::rename_var;

use ExprData::*;


pub fn unaliased(expr: Expr<Linked>) -> Expr<Linked> {
    match expr.data {
        Binding { var, val, body } =>
            match unaliased(*val).data {
                LitVar { id } =>
                    unaliased(rename_var(&var.id, &id, *body)).data,

                other_val =>
                    Binding {
                        var,
                        val: Box::new(other_val.into()),
                        body: Box::new(unaliased(*body))
                    }
            },

        LitVar { id } =>
            LitVar { id },
        
        LitConstructor { id, args } => LitConstructor {
            id,
            args: args
                .into_iter()
                .map(|arg| unaliased(arg))
                .collect()
        },

        Call { caller, arg } =>
            Call {
                caller: Box::new(unaliased(*caller)),
                arg: Box::new(unaliased(*arg))
            },

        Function { input, out_type, body } =>
            Function {
                input,
                out_type,
                body: Box::new(unaliased(*body))
            },

        Match { expr, cases } =>
            Match {
                expr: Box::new(unaliased(*expr)),
                cases: cases
                    .into_iter()
                    .map(|MatchBranch { pattern, body }|
                        MatchBranch {
                            pattern,
                            body: unaliased(body)
                        }
                    )
                    .collect()
            }
    }.into()
}


#[cfg(test)]
mod tests {
    use crate::process::read::expr;
    use super::*;


    fn assert_unalised_eq(original: &str, normal: &str) {
        assert_eq!(
            unaliased(expr(original.chars()).link()).link(),
            expr(normal.chars()).link()
        );
    }


    #[test]
    fn test_unaliased() {
        assert_unalised_eq(
            "let y = x in let z = y in z",
            "x",
        );
    }
}
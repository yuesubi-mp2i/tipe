use crate::lang::*;

use ExprData::*;


pub fn fold_bindings<I>(bindings: I, tail: Expr<Linked>) -> Expr<Linked>
    where I: DoubleEndedIterator<Item = (Var<Linked>, Expr<Linked>)>
{
    bindings
        .rev()
        .fold(
            tail,
            |expr, (var, val)|
                Binding {
                    var: var,
                    val: Box::new(val),
                    body: Box::new(expr)
                }.into()
        )
}
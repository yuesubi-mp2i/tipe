use std::collections::HashSet;
use crate::lang::*;

use ExprData::*;


pub fn expr_free_var_ids(expr: &Expr<Linked>) -> HashSet<LId> {
    match &expr.data {
        LitVar { id } =>
            HashSet::from([id.clone()]),
        
        LitConstructor { args, .. } =>
            args
            .iter()
            .map(expr_free_var_ids)
            .flat_map(HashSet::into_iter)
            .collect(),
        
        Call { caller, arg } =>
            expr_free_var_ids(caller)
                .union(&expr_free_var_ids(arg))
                .cloned()
                .collect(),

        Function { input, body, .. } =>
            expr_free_var_ids(body)
                .difference(&HashSet::from([input.id.clone()]))
                .cloned()
                .collect(),

        Match { expr, cases } => {
            let cases_free_vars = cases
                .into_iter()
                .flat_map(|MatchBranch { pattern, body }|
                    expr_free_var_ids(body)
                        .difference(&pattern_ids(pattern))
                        .cloned()
                        .collect::<Vec<_>>()
                )
                .collect();

            expr_free_var_ids(expr)
                .union(&cases_free_vars)
                .cloned()
                .collect()
        },

        Binding { var, val, body } => {
            let body_free_vars =
                expr_free_var_ids(body)
                .difference(&HashSet::from([var.id.clone()]))
                .cloned()
                .collect();

            expr_free_var_ids(val)
                .union(&body_free_vars)
                .cloned()
                .collect()
        }
    }
}


pub fn pattern_ids(pattern: &Pattern<Linked>) -> HashSet<LId> {
    use PatternData as PD;
    match &pattern.data {
        PD::Var(Var { id, .. }) =>
            HashSet::from([id.clone()]),
        
        PD::Constructor { args, .. } =>
            args
            .iter()
            .map(pattern_ids)
            .map(IntoIterator::into_iter)
            .flatten()
            .collect()
    }
}


#[cfg(test)]
mod tests {
    use crate::process::read;

    use super::*;


    fn expr_free_var_ids_from_str(src: &str) -> HashSet<LId> {
        expr_free_var_ids(&read::expr(src.chars()).link())
    }

    #[test]
    fn test_expr_free_var_ids() {
        assert_eq!(
            expr_free_var_ids_from_str("x"),
            HashSet::from([LId::from((0, "x"))])
        );

        assert_eq!(
            expr_free_var_ids_from_str("Some x"),
            HashSet::from([LId::from((0, "x"))])
        );

        assert_eq!(
            expr_free_var_ids_from_str("f x"),
            HashSet::from([
                LId::from((0, "f")),
                LId::from((1, "x"))
            ])
        );

        assert_eq!(
            expr_free_var_ids_from_str("fun (x) -> f x"),
            HashSet::from([LId::from((1, "f"))])
        );

        assert_eq!(
            expr_free_var_ids_from_str("match a with x -> f x"),
            HashSet::from([
                LId::from((0, "a")),
                LId::from((2, "f"))
            ])
        );

        assert_eq!(
            expr_free_var_ids_from_str("let x = a in f x"),
            HashSet::from([
                LId::from((0, "a")),
                LId::from((2, "f"))
            ])
        );
    }


    fn pattern_ids_from_str(src: &str) -> HashSet<LId> {
        pattern_ids(&read::pattern(src.chars()).link())
    }

    #[test]
    fn test_pattern_ids() {
        assert_eq!(
            pattern_ids_from_str("x"),
            HashSet::from([LId::from((0, "x"))])
        );

        assert_eq!(
            pattern_ids_from_str("Zero"),
            HashSet::new()
        );

        assert_eq!(
            pattern_ids_from_str("Tuple (n, Tuple (m, p))"),
            HashSet::from([
                LId::from((0, "n")),
                LId::from((1, "m")),
                LId::from((2, "p"))
            ])
        );
    }
}
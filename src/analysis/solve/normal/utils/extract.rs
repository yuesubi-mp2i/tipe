use std::collections::HashSet;
use crate::lang::*;

use super::find;
use super::misc::fold_bindings;

use ExprData::*;


type FreeBindings = Vec<(Var<Linked>, Expr<Linked>)>;
type BoundExpr = Expr<Linked>;

type BindingList = Vec<(Var<Linked>, Expr<Linked>)>;


pub fn free_bindings(mut bound_ids: HashSet<LId>, expr: Expr<Linked>)
    -> (FreeBindings, BoundExpr)
{
    let (bindings, res_expr) =
        top_bindings(expr);

    for (var, body) in &bindings {
        if !(find::expr_free_var_ids(body).is_disjoint(&bound_ids)) {
            bound_ids.insert(var.id.clone());
        }
    }

    let mut bound_bindings = Vec::new();
    let mut free_bindings = Vec::new();

    for (var, val) in bindings {
        if bound_ids.contains(&var.id) {
            bound_bindings.push((var, val));
        } else {
            free_bindings.push((var, val));
        }
    }

    let bound_expr = fold_bindings(
        bound_bindings.into_iter(),
        res_expr
    );

    (free_bindings, bound_expr)
}


fn top_bindings(expr: Expr<Linked>) -> (BindingList, Expr<Linked>)
{
    let mut bindings = Vec::new();
    let mut res_expr = expr;

    while let Binding { var, val, body } = res_expr.data {
        bindings.push((var, *val));
        res_expr = *body;
    };

    (bindings, res_expr)
}


#[cfg(test)]
mod tests {
    use crate::process::read::expr;

    use super::*;


    // #[test]
    // fn test_free_bindings() {
    //     todo!()
    // }


    #[test]
    fn test_top_bindings() {
        let id = expr("fun (x) -> x".chars()).link();
        assert_eq!(
            top_bindings(id.clone()),
            (Vec::new(), id)
        );

        let dbl_alias = expr("let y = z in let x = y in x".chars()).link();
        assert_eq!(
            top_bindings(dbl_alias),
            (
                vec![
                    (Var { id: LId::from((1, "y")), typ: None, meta: () },
                        LitVar { id: LId::from((0, "z")) }.into()),
                    (Var { id: LId::from((2, "x")), typ: None, meta: () },
                        LitVar { id: LId::from((1, "y")) }.into())
                ],
                LitVar { id: LId::from((2, "x")) }.into()
            )
        );
    }
}
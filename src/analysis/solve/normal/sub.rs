use std::collections::HashMap;
use crate::lang::*;

use ExprData::*;


pub fn substituted(
    expr: Expr<Linked>,
    to_substitute: &HashMap<LId, Expr<Linked>>
) -> Expr<Linked>
{
    match expr.data {
        LitVar { id } =>
            match to_substitute.get(&id) {
                None => LitVar { id },
                Some(val) => val.data.clone()
            },

        LitConstructor { id, args } =>
            LitConstructor {
                id,
                args: args
                    .into_iter()
                    .map(|expr| substituted(expr, to_substitute))
                    .collect()
            },
        
        Binding { var, val, body } =>
            Binding {
                var,
                val: Box::new(substituted(*val, to_substitute)),
                body: Box::new(substituted(*body, to_substitute))
            },
        
        Function { input, out_type, body } =>
            Function {
                input,
                out_type,
                body: Box::new(substituted(*body, to_substitute))
            },
        
        Call { caller, arg } =>
            Call {
                caller: Box::new(substituted(*caller, to_substitute)),
                arg: Box::new(substituted(*arg, to_substitute))
            },
        
        Match { expr, cases } =>
            Match {
                expr: Box::new(substituted(*expr, to_substitute)),
                cases: cases
                    .into_iter()
                    .map(|MatchBranch { pattern, body }|
                        MatchBranch {
                            pattern,
                            body: substituted(body, to_substitute)
                        }
                    )
                    .collect()
            }
    }.into()
}


#[cfg(test)]
mod tests {
    use super::*;

    use crate::process::read::expr;


    // #[test]
    fn test_substituted() {
    }
}
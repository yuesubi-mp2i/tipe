use crate::{lang::*, process::Linker};

mod sub;
mod devel;
mod simpl;
mod sort;
mod utils;

pub use sub::substituted;
use devel::developed;
use simpl::simplified;
use sort::sorted;


pub fn normalized<K: Kind>(
    expr: Expr<Linked>,
    linker: &mut Linker<K>
) -> Expr<Linked>
{
    let mut expr = expr;
    
    // This loop is a duck tape solution
    loop {
        let old_expr = expr.clone();

        expr = developed(expr, linker);
        expr = simplified(expr);
        expr = sorted(expr);

        if expr == old_expr {
            break;
        }
    }

    expr
}


#[cfg(test)]
mod tests {
    use crate::process::read::expr;

    use super::*;


    fn assert_normalized_eq(original: &str, normal: &str) {
        let mut linker = Linker::new();

        assert_eq!(
            normalized(
                linker.expr(expr(original.chars())).unwrap(),
                &mut linker
            ).link(),
            expr(normal.chars()).link()
        );
    }


    #[test]
    fn test_normalizing() {
        assert_normalized_eq(
            "(fun (a) -> fun (b) -> b) x y",
            "y"
        );

        assert_normalized_eq(
            "fun (b) -> let c = False in b",
            "fun (b) -> b"
        );

        assert_normalized_eq(
            "(fun (a) -> a) x",
            "x"
        );

        assert_normalized_eq(
            "Tuple (
                let y = z in
                    let x = y in
                    x,
                let b = a in
                    b
            )",
            "
            Tuple (z, a)
            "
        );
    }
}
use crate::{lang::*, process::Linker};

mod alias;
mod factor;

use alias::aliased;
use factor::factorized;


pub fn developed<K: Kind>(expr: Expr<Linked>, linker: &mut Linker<K>) -> Expr<Linked> {
    factorized(aliased(expr, linker))
}
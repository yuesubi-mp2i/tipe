use std::collections::{HashMap, HashSet};

use clap::error::Result;

use crate::lang::*;
use crate::process::{Linker, Typer};

use super::{
    free_id::FreeId,
    proof::*
};

use super::{matcher, pat};


pub fn prove(raw_prop: &Property<Raw>, raw_ast: &AST<Raw>) -> Result<Proof, CounterEx> {
    let mut linker = Linker::new();
    // TODO: maybe handle those errors
    let ast = linker.ast(raw_ast.clone()).unwrap();
    let prop = linker.property(raw_prop.clone()).unwrap();

    let free_ids = prop.vars
        .iter()
        .map(|var| FreeId::from(var.id.clone()))
        .collect();

    let bindings = ast
        .clone()
        .statements
        .into_iter()
        .map(|Statement { var, val }|
            (var.id.clone(), val.clone())
        )
        .collect();

    let prover = Prover {
        ast_vars: ast.free_vars(),
        linker,
        bindings,
        free_ids,
        assumptions: HashMap::new(),
        left: prop.left.clone(),
        right: prop.right.clone()
    };

    prover.prove()
}


#[derive(Clone)]
struct Prover {
    ast_vars: HashSet<LId>,
    linker: Linker<Raw>,
    bindings: HashMap<LId, Expr<Linked>>,
    free_ids: HashSet<FreeId>,
    assumptions: HashMap<LId, Expr<Linked>>,
    left: Expr<Linked>,
    right: Expr<Linked>
}


impl Prover {
    pub fn prove(mut self) -> Result<Proof, CounterEx> {
        use super::normal::{normalized, substituted};

        // TODO: figure out when to stop substituting or how many times to do it
        self.left = substituted(self.left.clone(), &self.bindings);
        self.right = substituted(self.right.clone(), &self.bindings);

        self.left = normalized(self.left.clone(), &mut self.linker);
        self.right = normalized(self.right.clone(), &mut self.linker);

        match matcher::find_match(self.left.clone())
            .or_else(|| matcher::find_match(self.right.clone()))
        {
            Some(matcher::FoundMatch {
                expr: match_expr,
                cases,
                ..
            }) => {
                let mut choices = Vec::new();

                for case in cases {
                    // TODO : check if it is not a sub pattern of above cases
                    match self.clone().try_make_choice(&match_expr, &case) {
                        None => (),
                        Some(choice_res) =>
                            choices.push(choice_res?)
                    }
                }
                
                if choices.len() > 0 {
                    return Ok(Proof {
                        tree: ChoiceTree::Choice(choices)
                    })
                } else {
                    unreachable!("Expected an exhaustive match")
                }
            },

            _ => {}
        }

        // TODO next time :
        // - collect properties that can be used
        // - link using the free variables of the property & the Prover
        // - if matching expressions works
        //   - check whether the property uses one of the free variable that is smaller
        //   - alias using the linker to prevent any id conflict
        //   - add hypotheses to the proof tree
        //     - add indications of the type using x = ..., y = ... in the printing

        let final_left = self.expr_to_standard_naming(self.left.clone());
        let final_right= self.expr_to_standard_naming(self.right.clone());

        if final_left == final_right {
            Ok(Proof {
                tree: ChoiceTree::Leaf {
                    left: final_left,
                    right: final_right 
                }
            })
        } else {
            Err(CounterEx {
                vals: self.assumptions,
                left: final_left,
                right: final_right
            })
        }
    }


    fn try_make_choice(
        mut self,
        match_expr: &Expr<Linked>,
        case: &MatchBranch<Linked>,
    )
        -> Option<Result<Choice, CounterEx>>
    {
        match pat::make_into(&case.pattern, match_expr, &self.free_ids) {
            None => None,

            Some(pat::Assumption { assumptions, new_free_ids}) => {
                let used_ids: HashSet<_> = assumptions.keys().cloned().collect();
                self.free_ids = self.free_ids
                        .difference(&used_ids)
                        .cloned()
                        .collect::<HashSet<_>>()
                        .union(&new_free_ids)
                        .cloned()
                        .collect();
                
                let id_assumptions: HashMap<_, _> = assumptions.into_iter()
                    .map(|(free_id, expr)| (free_id.id, expr))
                    .collect();
                self.assumptions.extend(id_assumptions.clone());   

                self.left = id_assumptions.iter()
                    .fold(
                        self.left,
                        |tail, (id, val)| ExprData::Binding {
                            var: Var { id: id.clone(), typ: None, meta: () },
                            val: Box::new(val.clone()),
                            body: Box::new(tail)
                        }.into()
                    );
                self.right = id_assumptions.iter()
                    .fold(
                        self.right,
                        |tail, (id, val)| ExprData::Binding {
                            var: Var { id: id.clone(), typ: None, meta: () },
                            val: Box::new(val.clone()),
                            body: Box::new(tail)
                        }.into()
                    );

                Some(match self.prove() {
                    Ok(proof) => Ok(Choice {
                        assumptions: id_assumptions,
                        next: proof.tree
                    }),

                    Err(counter_ex) => Err(counter_ex)
                })
            }
        }
    }

    fn expr_to_standard_naming(&mut self, expr: Expr<Linked>) -> Expr<Linked> {
        let mut linker = Linker::new();

        // Maybe make this less horrible :
        // Ids currently need to be inserted in order because the
        // id is reserved my making a big jump in id counting.
        let mut ids_to_reserve: Vec<LId> = self.free_ids
            .iter()
            .map(|free_id| free_id.id.clone())
            .collect();
        ids_to_reserve.extend(self.assumptions.keys().cloned());
        //ids_to_reserve.extend(context.bindings.keys().cloned());
        ids_to_reserve.extend(self.ast_vars.clone());
        ids_to_reserve.sort_by(|ida, idb| ida.id.cmp(&idb.id));

        for id in ids_to_reserve {
            linker.reserve_id(id).unwrap()
        }

        linker.clone()
            .expr(expr)
            .unwrap()
    }
}

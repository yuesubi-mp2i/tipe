use std::collections::HashMap;

use crate::lang::ast::*;
use crate::utils::ScopeStack;


pub fn eval(
    bindings: &HashMap<usize, Expr<Untyped>>,
    expr: Expr<Untyped>
) -> Expr<Untyped>
{
    let mut scopes = ScopeStack::new();
    scopes.push_empty_scope();

    for (k, v) in bindings.iter() {
        scopes.insert(k.clone(), v.clone());
    }

    let mut evaluator = Evaluator {
        bindings: scopes 
    };

    evaluator.expr(expr)
}


struct Evaluator {
    bindings: ScopeStack<usize, Expr<Untyped>>
}


impl Evaluator {
    fn expr(&mut self, expr: Expr<Untyped>) -> Expr<Untyped> {
        use Expr::*;

        match expr {
            Binding { var, val, body }
                => self.assignment(var, *val, *body),
            Function { input, out_type, body }
                => Function { input, out_type, body: Box::new(self.expr(*body)) },
            Call { caller, arg }
                => self.call(*caller, *arg),
            Match { expr, cases }
                => self.match_with(*expr, cases),
            LitVar { id } =>
                self.literal_variable(id),
            LitConstructor { id, args: argument } =>
                self.literal_constructor(id, argument)
        }
    }

    fn assignment(&mut self, var: Var<Untyped>, val: Expr<Untyped>, body: Expr<Untyped>) -> Expr<Untyped> {
        self.bindings.push_empty_scope();
        self.bindings.insert(var.id, val);
        let res = self.expr(body);
        self.bindings.pop_scope();
        res
    }

    fn call(&mut self, caller: Expr, arg: Expr) -> Expr {
        use Expr::*;

        let caller = self.expr(caller);
        let arg = self.expr(arg);

        match caller {
            Function { input, body, .. } => {
                self.bindings.push_empty_scope();
                self.bindings.insert(input.id, arg);
                let res = self.expr(*body);
                self.bindings.pop_scope();
                res
            },
            caller => Call {
                caller: Box::new(caller),
                arg: Box::new(arg)
            }
        }
    }

    fn match_with(&mut self, expr: Expr<Untyped>, cases: Vec<MatchBranch<Untyped>>) -> Expr<Untyped> {
        let simpler_expr = self.expr(expr);

        use super::matcher;
        match matcher::resolve_explicit(&cases, &simpler_expr) {
            None => Expr::Match {
                expr: Box::new(simpler_expr),
                cases
            },

            Some(matcher::MatchSolution {
                variables,
                chosen
            }) => {
                self.bindings.push_scope(variables);
                chosen
            }
        }
    }

    fn literal_variable(&self, id: usize) -> Expr<Untyped> {
        match self.bindings.get_copy(&id) {
            None => Expr::LitVar { id },
            Some(expr) => expr
        }
    }

    fn literal_constructor(&mut self, id: usize, argument: Vec<Expr<Untyped>>) -> Expr<Untyped> {
        Expr::LitConstructor {
            id,
            args: argument.iter()
                .map(|arg| self.expr(arg.clone()))
                .collect()
        }
    }
}
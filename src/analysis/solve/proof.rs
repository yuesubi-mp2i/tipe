use std::{collections::HashMap, fmt::Display};

use crate::lang::*;


#[derive(Debug)]
pub struct Choice {
    pub assumptions: HashMap<LId, Expr<Linked>>,
    pub next: ChoiceTree
}

#[derive(Debug)]
pub enum ChoiceTree {
    Leaf {
        left: Expr<Linked>,
        right: Expr<Linked>
    },
    Hypothesis(Box<ChoiceTree>),
    Choice(Vec<Choice>)
}


#[derive(Debug)]
pub struct Proof {
    pub tree: ChoiceTree
}


#[derive(Debug)]
pub struct CounterEx {
    pub vals: HashMap<LId, Expr<Linked>>,
    pub left: Expr<Linked>,
    pub right: Expr<Linked>,
}


impl Display for Proof {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        print_tree(f, &self.tree, 0)
    }
}

fn print_tree(f: &mut std::fmt::Formatter<'_>, tree: &ChoiceTree, depth: usize)
    -> std::fmt::Result
{
    use ChoiceTree::*;

    match tree {
        Leaf { left, right } => {
            print_tab(f, depth)?;
            writeln!(f, "□ {:?} = {:?}", left, right)
        },

        Hypothesis(next) => {
            print_tab(f, depth)?;
            writeln!(f, "┝╸ ...")?;
            print_tree(f, next, depth)
        },

        Choice(choices) => {
            for choice in choices {
                print_tab(f, depth + 1)?;
                write!(f, "\n")?;

                print_tab(f, depth)?;
                write!(f, "├─┬╴")?;
                f.debug_map()
                    .entries(choice.assumptions.iter())
                    .finish()?;
                write!(f, "\n")?;

                print_tree(f, &choice.next, depth + 1)?;
            }

            Ok(())
        }
    }
}

fn print_tab(f: &mut std::fmt::Formatter<'_>, size: usize) -> std::fmt::Result {
    for _ in 0..size {
        write!(f, "│ ")?
    }
    Ok(())
}


impl Display for CounterEx {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_map()
            .entries(self.vals.iter())
            .finish()?;
        write!(f, "\n");

        writeln!(f, "{:?} = {:?}", self.left, self.right)
    }
}
use crate::lang::ast::*;


pub struct DependsOn {
    pub id: usize
}


impl DependsOn {

    pub fn expr(&mut self, expr: &Expr<usize>) -> bool {
        use Expr::*;

        match expr {
            Assignment {
                variable,
                assignment,
                body
            } =>
                self.variable(variable)
                    || self.expr(assignment)
                    || self.expr(body),
            Function {
                input,
                out_type,
                body
            } =>
                self.variable(input)
                    || self.expr(body),
            Call {
                caller,
                argument
            } =>
                self.expr(caller)
                    || self.expr(argument),
            Match {
                expr,
                cases
            } =>
                self.expr(expr)
                    || cases.iter().any(|b| self.match_branch(b)),
            LiteralVariable {
                id
            } =>
                *id == self.id,
            LiteralConstructor {
                id,
                argument
            } =>
                *id == self.id
                    || argument.iter().any(|e| self.expr(e))
        }
    }

    pub fn variable(&mut self, variable: &Variable<usize>) -> bool {
        variable.id == self.id
    }

    pub fn match_branch(&mut self, branch: &MatchBranch<usize>) -> bool {
        let MatchBranch { pattern, body } = branch;
        self.pattern(pattern) || self.expr(body)
    }

    pub fn pattern(&mut self, pattern: &Pattern<usize>) -> bool {
        use Pattern::*;

        match pattern {
            Variable(variable) => self.variable(variable),
            Constructor { id, arguments } =>
                *id == self.id
                    || arguments.iter().any(|p| self.pattern(p))
        }
    }
}
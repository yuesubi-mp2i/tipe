use std::collections::HashMap;

use crate::lang::*;

use ExprData::*;


pub struct FoundMatch {
    pub bindings: HashMap<LId, Expr<Linked>>,
    pub expr: Expr<Linked>,
    pub cases: Vec<MatchBranch<Linked>>
}


pub fn find_match(expr: Expr<Linked>) -> Option<FoundMatch> {
    let bindings = HashMap::new();

    find_match_aux(bindings, expr)
}

fn find_match_aux(mut bindings: HashMap<LId, Expr<Linked>>, expr: Expr<Linked>)
    -> Option<FoundMatch>
{
    match expr.data {
        Binding { var, val, body } =>
            find_match_aux(bindings.clone(), *val.clone())
                .or_else(|| {
                    bindings.insert(var.id, *val);
                    find_match_aux(bindings, *body)
                }),

        Function { body, .. } =>
            find_match_aux(bindings, *body),

        Call { caller, arg } =>
            find_match_aux(bindings.clone(), *caller)
                .or_else(|| find_match_aux(bindings, *arg)),

        Match { expr, cases } =>
            Some(FoundMatch { bindings, expr: *expr, cases }),

        LitVar { .. } => None,

        LitConstructor { args, .. } =>
            args.into_iter()
                .fold(
                    None,
                    |opt_res, e|
                        opt_res.or_else(|| find_match_aux(
                            bindings.clone(),
                            e
                        ))
                )
    }
}
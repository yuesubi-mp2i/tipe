use std::{collections::HashSet};

use crate::lang::ast::*;


enum Var {
    Static(String),
    Unifier(String)
}

enum Constraint  {
    Specialization {
        args_a: Vec<Var>,
        typ_a: Var,
        args_b: Vec<Var>,
        typ_b: Var,
    },
    Function {
        input_a: Var,
        output_a: Var,
        input_b: Var,
        output_b: Var,
    }
} 


pub fn typ(raw_ast: &AST<Raw>) -> Result<AST<Typed>, ()> {
    // TODO:
    // Use algo J with union-find to unify.
    // To do this, link types together
    
    let constraints = find_constraints();
    let type_values = unify(constraints);
    set_types(raw_ast)
}


fn find_constraints() -> Vec<Constraint> {
    todo!()
}

fn unify(constraints: Vec<Constraint>) -> HashSet<String, Type<Typed>> {
    todo!()
}

fn set_types(raw_ast: &AST<Raw>) -> Result<AST<Typed>, ()> {
    todo!()
}